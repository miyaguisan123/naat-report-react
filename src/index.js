import React from 'react'
import ReactDOM from 'react-dom'

import NaatEditor from './components/naatEditor'

import 'antd/lib/date-picker/style/css'
import 'antd/dist/antd.css'
import './css/editor.css'

const app = document.getElementById('app')
/*
const idForm = parseInt(app.getAttribute('data-idForm'), 10)
const idPatient = parseInt(app.getAttribute('data-idPatient'), 10)
const idProduct = parseInt(app.getAttribute('data-idProduct'), 10)
*/

const idForm = 1
const idPatient = 1
const idProduct = 1

const editor = <NaatEditor idForm={idForm} idPatient={idPatient} idProduct={idProduct}/>
ReactDOM.render(editor, app)
