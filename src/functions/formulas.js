import {getElementWith} from './functions'

function formulaIsValid(formula) {
	var evaluatedFormula = formula + ""
	
	if (evaluatedFormula.includes('script>') || evaluatedFormula.includes('document')) {
		return false
	}
	
	evaluatedFormula = evaluatedFormula.replace(new RegExp('\\[hoy]', 'g'), '')
	evaluatedFormula = evaluatedFormula.replace(new RegExp('\\[ahora]', 'g'), '')
	evaluatedFormula = evaluatedFormula.replace(new RegExp('\\[today]', 'g'), '')
	evaluatedFormula = evaluatedFormula.replace(new RegExp('\\[now]', 'g'), '')
	
	var allowedChars = '.0123456789 ()[]<>?=!:*/%+-abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ_@'
	for (var index = 0; index < evaluatedFormula.length; index++) {
		var currentChar = evaluatedFormula.charAt(index)
		if (allowedChars.indexOf(currentChar) < 0) {
			return false
		}
	}
	
	return true
}

function replaceTimeExpressions(formula) {
	var hoursRegex = new RegExp('\\(.*?h\\)', 'g')
	var minutesRegex = new RegExp('\\(.*?m\\)', 'g')
	var secondsRegex = new RegExp('\\(.*?s\\)', 'g')
	
	var newFormula = formula
	
	var hoursMatches = formula.match(hoursRegex)
	if (hoursMatches) {
		hoursMatches.forEach(match => {
			var hours = parseFloat(match.slice(1,match.length - 2)) * (3600000)
			newFormula = newFormula.replace(match, hours)
		})
	}
	
	var minutesMatches = formula.match(minutesRegex)
	if (minutesMatches) {
		minutesMatches.forEach(match => {
			var minutes = parseFloat(match.slice(1,match.length - 2)) * (60000)
			newFormula = newFormula.replace(match, minutes)
		})
	}
	
	var secondsMatches = formula.match(secondsRegex)
	if (secondsMatches) {
		secondsMatches.forEach(match => {
			var seconds = parseFloat(match.slice(1,match.length - 2)) * 1000
			newFormula = newFormula.replace(match, seconds)
		})
	}
	
	return newFormula
}

function evaluateFormulaFunction(formula,precision) {
	try {
		var expressionFunction = new Function('', 'return (' + formula + ');')
		return expressionFunction()
	}
	catch(e) {
		//error... report?
		//user defined a bad formula
	}
	
	return ''
}

export function getFormulaType(form, formula) {
	var type = 'number'
	
	if (formula.indexOf('h)') !== -1 || formula.indexOf('m)') !== -1 || formula.indexOf('s)') !== -1) {
		type = 'milliseconds'
	}
	else {
		var searchExpression = new RegExp(/[^[\]]+(?=])/g)
		var elements = formula.match(searchExpression)
		var types = []
		
		if (elements !== null && typeof elements !== typeof undefined && elements.length > 0) {
			elements.forEach(idElement => {
				if (idElement === 'hoy' || idElement === 'today') {
					types.push('date')
				}
				else if (idElement === 'ahora' || idElement === 'now') {
					types.push('time')
				}
				else {
					var element = getElementWith(form, idElement, 'idElement')
					var elementType = 'number'
					
					if (element.type === 'date') {
						elementType = 'date'
					}
					else if (element.type === 'time') {
						elementType = 'time'
					}
					else if (element.type === 'calc') {
						elementType = getFormulaType(form, element.formula)
					}
					
					types.push(elementType)
				}
			})
			
			types.forEach(internalType => {
				if (internalType === 'date') {
					type = 'days'
				}
				else if (internalType === 'time') {
					type = 'milliseconds'
				}
			})
		}
	}
	
	return type
}

//string format needs to be used to avoid Unix EPOCH limit
//form is not needed
function evaluateComplexFormula(form, formula, precision, fieldDictionary) {
	var result = 0
	
	for(var key in fieldDictionary) {
		var item = fieldDictionary[key]
		var type = item.type
		var value = item.value
		var newValue = value
		
		if (type === 'date') {
			var date = new Date(value)
			date.setHours(0,0,0,0)
			newValue = 'new Date("'+value+'").getTime()'
		}
		else if (type === 'time') {
			var time = new Date(value)
			var placeHolder = new Date()
			placeHolder.setHours(time.getHours())
			placeHolder.setMinutes(time.getMinutes())
			placeHolder.setSeconds(time.getSeconds())
			newValue = 'new Date("'+value+'").getTime()'
		}
		else if (type === 'calc') {
			var testDate = new Date(value)
			if (isNaN(testDate.getTime()) === false) {
				newValue = 'new Date("'+value+'").getTime()'
			}
		}
		
		var regex = new RegExp('\\['+key+']', 'g')
		formula = formula.replace(regex, newValue)
	}
	
	result = evaluateFormulaFunction(formula, precision)
	
	return result
}

export function resultFromFormula(form, formula, precision) {
	if (formulaIsValid(formula) === false) {
		return ''
	}
	
	var fieldDictionary = {}
	var searchExpression = new RegExp(/[^[\]]+(?=])/g)
	var elements = formula.match(searchExpression)
	
	if (elements !== null && elements.length > 0) {
		var emptyElements = 0
		var formulaContainsObjects = false
		
		elements.forEach(idElement => {
			if (idElement === 'hoy' || idElement === 'today') {
				fieldDictionary[idElement] = {
					value: new Date(),
					type: 'date'
				}
				
				formulaContainsObjects = true
			}
			else if (idElement === 'ahora' || idElement === 'now') {
				fieldDictionary[idElement] = {
					value: new Date(),
					type: 'time'
				}
				
				formulaContainsObjects = true
			}
			else {
				var element = getElementWith(form, idElement, 'idElement')
				var currentValue = element.value
				
				if (element.type === 'calc') {
					var elementFormula = element.formula
					var elementPrecision = element.precision
					currentValue = resultFromFormula(form, elementFormula, elementPrecision)
				}
				
				if ((currentValue + "").length > 0) {
					if ((element.type === 'date' || element.type === 'time') && currentValue.length > 0) {
						currentValue = new Date(currentValue)
						formulaContainsObjects = true
					}
					else if (element.type === 'calc') {
						var test = new Date(currentValue)
						if (isNaN(test) === false) {
							currentValue = new Date(currentValue)
							formulaContainsObjects = true
						}
					}
					else if (element.type === 'checkbox') {
						var total = 0
						element.value.forEach(val => {
							total += parseInt(val, 10)
						})
						
						currentValue = total
					}
					
					fieldDictionary[idElement] = {
						value: currentValue,
						type: element.type
					}
				}
				else {
					emptyElements += 1
				}
			}
		})
		
		if (emptyElements > 0) {
			return ''
		}
		
		if (formulaContainsObjects === true) {
			return evaluateComplexFormula(form, replaceTimeExpressions(formula), precision, fieldDictionary)
		}
		else {
			for(var key in fieldDictionary) {
				var regex = new RegExp('\\['+key+']', 'g')
				var value = fieldDictionary[key].value
				
				formula = formula.replace(regex, value)
			}
			
			return evaluateFormulaFunction(formula, precision)
		}
	}
	else {
		return evaluateFormulaFunction(formula, precision)
	}
}