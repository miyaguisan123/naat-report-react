import {updateCriteriasRelatedTo, updateDependenciesRelatedTo, updateFormulasRelatedTo} from './elementFunctions'
import {generateRange} from './functions'

var totalSeparators = 0
const alphabetic = generateRange("A", "Z")

export function updateElementId(form, element, idNew) {
	form.pages.forEach(page => {
		page.elements.forEach(internalElement => {
			updateCriteriasRelatedTo(element, internalElement, idNew)
			updateDependenciesRelatedTo(element, internalElement, idNew)
			updateFormulasRelatedTo(element, internalElement, idNew)
		})
	})
}

function reOrderItemIds(form, item, parentIndex, clean, ignoreUserDefinedIds) {
	var index = 1
	
	item.elements.forEach(element => {
		if (element.type === 'separator') {
			var idNewSeparator = 'sep_' + totalSeparators
			element.fieldID = idNewSeparator
		}
		else {
			var idNewElement = (parentIndex+''+index)
			index++;
			
			if (ignoreUserDefinedIds === true || element.fieldID.length === 0 || (element.fieldID.indexOf('@') !== -1)) {
				if (clean === false) {
					 idNewElement += '_rename'
				}
				
				updateElementId(form, element, idNewElement)
				element.fieldID = idNewElement
				
				if (element.type === 'table') {
					var headers = element.fields
					
					for (var headerIndex=0;headerIndex<headers.length;headerIndex++) {
						var header = headers[headerIndex]
						var items = header.items
						
						for (var itemIndex=0;itemIndex<items.length;itemIndex++) {
							var tableItem = items[itemIndex]
							
							if (tableItem.type === 'calc') {
								var newFormula = tableItem.formula
								if (newFormula.includes('UPDATE[') && newFormula.includes('rename') === false) {
									var matches = newFormula.match(/(UPDATE)\[(\S+)_(\S+)]/g)
									for (var matchIndex=0;matchIndex<matches.length;matchIndex++) {
										var match = matches[matchIndex]
										var parts = match.split('_')
										var replacement = '['+idNewElement+'_'+parts[1]
										newFormula = newFormula.replace(match, replacement)
									}
									
									tableItem.formula = newFormula
								}
							}
							
							var letter = alphabetic[headerIndex]
							var idElementTable = letter + (itemIndex + 1)
							var newElementID = idNewElement + '_' + idElementTable
							
							updateElementId(form, tableItem, newElementID)
							tableItem.fieldID = newElementID
						}
					}
				}
			}
			
			reOrderItemIds(form, element, (idNewElement+'.'), clean, ignoreUserDefinedIds)
		}
	})
}

export function reOrderFormIds(form) {
	var ignoreUserDefinedIds = false
	totalSeparators = 0
	
	form.pages.forEach((page, index) => {
		reOrderItemIds(form, page, (index + 1) + '@', false, ignoreUserDefinedIds)
	})
	
	form.pages.forEach((page, index) => {
		reOrderItemIds(form, page, (index + 1) + '@', true, ignoreUserDefinedIds)
	})
	
	return form
}

export function removeIDsFrom(container) {
	if (typeof container.fieldID !== typeof undefined) {
		container.fieldID = ''
		
		if (container.type === 'table') {
			var headers = container.fields
			for (var headerIndex=0;headerIndex<headers.length;headerIndex++) {
				var header = headers[headerIndex]
				header.items.forEach(item => {
					removeIDsFrom(item)
				})
			}
		}
	}
	
	if (typeof container.elements !== typeof undefined) {
		container.elements.forEach(internalElement => {
			removeIDsFrom(internalElement)
		})
	}
}