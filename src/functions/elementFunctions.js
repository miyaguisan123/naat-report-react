//** - ELEMENT DELETE - **//
function removeCriteriasRelatedTo(currentElement, idElementDeleted) {
	var criteriasToRemove = []
	
	currentElement.criteria.forEach((criteria, index) => {
		if (criteria.field === idElementDeleted) {
			criteriasToRemove.push(index)
		}
	})
	
	if (criteriasToRemove.length > 0) {
		for (var indexToRemove = criteriasToRemove.length -1; indexToRemove >= 0; indexToRemove--) {
			currentElement.criteria.splice(criteriasToRemove[indexToRemove],1)
		}
	}
	
	if (currentElement.type === 'table') {
		var fields = currentElement.fields
		if (typeof fields !== typeof undefined && fields.length > 0) {
			var totalFields = fields.length
			for (var i=0;i<totalFields;i++) {
				var field = fields[i]
				var items = field.items
				
				if (typeof items !== typeof undefined && items.length > 0) {
					var totalItems = items.length
					for (var j=0;j<totalItems;j++) {
						var item = items[j]
						removeCriteriasRelatedTo(item, idElementDeleted)
					}
				}
			}
		}
	}
	
	if (typeof currentElement.elements !== typeof undefined) {
		currentElement.elements.forEach(internalElement => {
			removeCriteriasRelatedTo(internalElement, idElementDeleted)
		})
	}
}

function removeDependenciesRelatedTo(currentElement, idElementDeleted) {
	if (currentElement.type === 'dataSource') {
		var config = currentElement.dataSourceConfig
		var dependency = config.dependencies
		
		if (typeof dependency !== typeof undefined && dependency.editorID === idElementDeleted) {
			dependency.editorID = ''
			dependency.tableField = ''
			currentElement.value = ''
			currentElement.dataSourceTemporal = ''
		}
	}
	else if (currentElement.type === 'table') {
		var fields = currentElement.fields
		if (typeof fields !== typeof undefined && fields.length > 0) {
			var totalFields = fields.length
			for (var i=0;i<totalFields;i++) {
				var field = fields[i]
				var items = field.items
				
				if (typeof items !== typeof undefined && items.length > 0) {
					var totalItems = items.length
					for (var j=0;j<totalItems;j++) {
						var item = items[j]
						removeDependenciesRelatedTo(item, idElementDeleted)
					}
				}
			}
		}
	}
	
	if (typeof currentElement.elements !== typeof undefined) {
		currentElement.elements.forEach(internalElement => {
			removeDependenciesRelatedTo(internalElement, idElementDeleted)
		})
	}
}

function removeFormulasRelatedTo(currentElement, idElementDeleted) {
	if (currentElement.type === 'calc' && currentElement.formula.includes('['+idElementDeleted+']')) {
		currentElement.formula = ''
		currentElement.value = ''
		currentElement.formulaResult = ''
	}
	else if (currentElement.type === 'table') {
		var fields = currentElement.fields
		if (typeof fields !== typeof undefined && fields.length > 0) {
			var totalFields = fields.length
			for (var i=0;i<totalFields;i++) {
				var field = fields[i]
				var items = field.items
				
				if (typeof items !== typeof undefined && items.length > 0) {
					var totalItems = items.length
					for (var j=0;j<totalItems;j++) {
						var item = items[j]
						removeFormulasRelatedTo(item, idElementDeleted)
					}
				}
			}
		}
	}
	
	if (typeof currentElement.elements !== typeof undefined) {
		currentElement.elements.forEach(internalElement => {
			removeFormulasRelatedTo(internalElement, idElementDeleted)
		})
	}
}

export function deleteContentRelatedTo(form, idElementDeleted) {
	form.pages.forEach(page => {
		page.elements.forEach(element => {
			removeCriteriasRelatedTo(element, idElementDeleted)
			removeDependenciesRelatedTo(element, idElementDeleted)
			removeFormulasRelatedTo(element, idElementDeleted)
		})
	})
}

//** - ELEMENT UPDATE - **//
export function updateCriteriasRelatedTo(sourceElement, currentElement, idNew) {
	currentElement.criteria.forEach(criteria => {
		if (criteria.field === sourceElement.fieldID) {
			criteria.field = idNew
		}
	})
	
	if (currentElement.type === 'table') {
		var fields = currentElement.fields
		if (typeof fields !== typeof undefined && fields.length > 0) {
			var totalFields = fields.length
			for (var i=0;i<totalFields;i++) {
				var field = fields[i]
				var items = field.items
				
				if (typeof items !== typeof undefined && items.length > 0) {
					var totalItems = items.length
					for (var j=0;j<totalItems;j++) {
						var item = items[j]
						updateCriteriasRelatedTo(sourceElement, item, idNew)
					}
				}
			}
		}
	}
	
	if (typeof currentElement.elements !== typeof undefined) {
		currentElement.elements.forEach(internalElement => {
			updateCriteriasRelatedTo(sourceElement, internalElement, idNew)
		})
	}
}

export function updateDependenciesRelatedTo(sourceElement, currentElement, idNew) {
	if (sourceElement.type === 'dataSource' && currentElement.type === 'dataSource') {
		var config = currentElement.dataSourceConfig
		var dependency = config.dependencies
		
		if (dependency !== null && typeof dependency !== typeof undefined) {
			if (dependency.editorID === sourceElement.fieldID) {
				dependency.editorID = idNew
			}
		}
	}
	else if (currentElement.type === 'table') {
		var fields = currentElement.fields
		if (typeof fields !== typeof undefined && fields.length > 0) {
			var totalFields = fields.length
			for (var i=0;i<totalFields;i++) {
				var field = fields[i]
				var items = field.items
				
				if (typeof items !== typeof undefined && items.length > 0) {
					var totalItems = items.length
					for (var j=0;j<totalItems;j++) {
						var item = items[j]
						updateDependenciesRelatedTo(sourceElement, item, idNew)
					}
				}
			}
		}
	}
	
	if (typeof currentElement.elements !== typeof undefined) {
		currentElement.elements.forEach(internalElement => {
			updateDependenciesRelatedTo(sourceElement, internalElement, idNew)
		})
	}
}

export function updateFormulasRelatedTo(sourceElement, currentElement, idNew) {
	if (currentElement.type === 'calc' && currentElement.formula.includes('['+sourceElement.fieldID+']')) {
		var regex = new RegExp('\\['+sourceElement.fieldID+']', 'g')
		var newFormula = currentElement.formula.replace(regex, '['+idNew+']')
		currentElement.formula = newFormula
	}
	else if (currentElement.type === 'table') {
		var fields = currentElement.fields
		if (typeof fields !== typeof undefined && fields.length > 0) {
			var totalFields = fields.length
			for (var i=0;i<totalFields;i++) {
				var field = fields[i]
				var items = field.items
				
				if (typeof items !== typeof undefined && items.length > 0) {
					var totalItems = items.length
					for (var j=0;j<totalItems;j++) {
						var item = items[j]
						updateFormulasRelatedTo(sourceElement, item, idNew)
					}
				}
			}
		}
	}
	
	if (typeof currentElement.elements !== typeof undefined) {
		currentElement.elements.forEach(internalElement => {
			updateFormulasRelatedTo(sourceElement, internalElement, idNew)
		})
	}
}

function cleanDependency(idElementDependency, currentElement) {
	if (currentElement.type === 'dataSource') {
		var config = currentElement.dataSourceConfig
		var dependency = config.dependencies
		
		if (typeof dependency !== typeof undefined && dependency.editorID === idElementDependency) {
			currentElement.dataSourceTemporal = ''
			currentElement.value = ''
		}
	}
	else if (currentElement.type === 'table') {
		var fields = currentElement.fields
		if (typeof fields !== typeof undefined && fields.length > 0) {
			var totalFields = fields.length
			for (var i=0;i<totalFields;i++) {
				var field = fields[i]
				var items = field.items
				
				if (typeof items !== typeof undefined && items.length > 0) {
					var totalItems = items.length
					for (var j=0;j<totalItems;j++) {
						var item = items[j]
						cleanDependency(idElementDependency, item)
					}
				}
			}
		}
	}
	
	if (typeof currentElement.elements !== typeof undefined) {
		currentElement.elements.forEach(internalElement => {
			cleanDependency(idElementDependency, internalElement)
		})
	}
}

export function cleanDependenciesRelatedTo(form, idElement) {
	form.pages.forEach(page => {
		page.elements.forEach(internalElement => {
			cleanDependency(idElement, internalElement)
		})
	})
}