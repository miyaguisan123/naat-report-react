import {getElementWith} from './functions'

export function elementFulfillsCriteria(form, element) {
	var criteriaMet = true
	var criteria = element.criteria
	var totalCriteriaElements = criteria.length
	
	if (totalCriteriaElements > 0) {
		var expression = ''
		
		criteria.forEach(criteriaObject => {
			var fieldID = criteriaObject.field
			var expectedValue = criteriaObject.value
			var isOptional = criteriaObject.optional
			var criteriaEquality = criteriaObject.equality
			var criteriaElement = getElementWith(form, fieldID, 'idElement')
			
			if (criteriaElement !== null) {
				var currentValue = criteriaElement.value
				var numericCurrentValue = parseFloat(currentValue)
				
				if (isNaN(numericCurrentValue) === false) {
					expectedValue = parseFloat(expectedValue)
					if (isOptional) {
						expression = expression + ('||'+currentValue+criteriaEquality+expectedValue)
					}
					else {
						expression = ('&&'+currentValue+criteriaEquality+expectedValue) + expression
					}
				}
				else {
					if (isOptional) {
						expression = expression + ('||"'+currentValue+'"'+criteriaEquality+'"'+expectedValue+'"')
					}
					else {
						expression = ('&&"'+currentValue+'"'+criteriaEquality+'"'+expectedValue+'"') + expression
					}
				}
			}
		})
		
		if (expression.length > 0) {
			expression = expression.substring(2)
			var expressionFunction = new Function('return (' + expression + ');')
			criteriaMet = expressionFunction()
		}
	}
	else {
		criteriaMet = false
	}
	
	return criteriaMet
}

export function minMaxAreValid(minToTest, maxToTest, elementType) {
	var minString = minToTest + ''
	var maxString = maxToTest + ''
	var valid = true
	
	if (minString.length > 0 && maxString.length > 0) {
		if (elementType === 'date') {
			var dateMin = new Date(minToTest)
			var dateMax = new Date(maxToTest)
			dateMin.setHours(0,0,0,0)
			dateMax.setHours(0,0,0,0)
			
			valid = (dateMin.getTime() <= dateMax.getTime())
		}
		else if (elementType === 'time') {
			var currentPlaceholderMinDate = new Date()
			var currentPlaceholderMaxDate = new Date()
			var timeMin = new Date(minToTest)
			var timeMax = new Date(maxToTest)
			
			currentPlaceholderMinDate.setHours(timeMin.getHours())
			currentPlaceholderMinDate.setMinutes(timeMin.getMinutes())
			currentPlaceholderMinDate.setSeconds(timeMin.getSeconds())
			
			currentPlaceholderMaxDate.setHours(timeMax.getHours())
			currentPlaceholderMaxDate.setMinutes(timeMax.getMinutes())
			currentPlaceholderMaxDate.setSeconds(timeMax.getSeconds())
			
			valid = (currentPlaceholderMinDate.getTime() <= currentPlaceholderMaxDate.getTime())
		}
		else {
			var min = parseFloat(minToTest)
			var max = parseFloat(maxToTest)
			
			valid = (min <= max)
		}
	}
	
	return valid
}