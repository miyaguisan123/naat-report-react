import axios from 'axios'
import message from 'antd/lib/message'
import {translate} from './translations'
import {elementFulfillsCriteria} from './criteriaValidation'

export const flagStatus = {1:'#ff4500',2:'#00008b',5:'#643687'}

//export const base = 'http://localhost:80/ne200/'
//export const base = 'http://localhost:8084/naatreportDemo/'
export const base = 'http://localhost:8084/naatcentinela/'
//export const base = ''

export var dataSourceWSURL = base + 'editorDataSourcesWS.htm'
export var dataSourceHelperWSURL = dataSourceWSURL
export var filesWSURL = base + 'editorFilesWS.htm'
export var formWSURL = base + 'editorFormWS.htm'
export var notesWSURL = base + 'editorNotesWS.htm'
export var pdfWSURL = base + 'editorPDFWS.htm'
export var xmlWSURL = base + 'editorXMLWS.htm'

export function loadForm(editor) {
	var config = {
		method: 'post',
		headers: {
			'Accept':'application/json, text/javascript, */*; q=0.01'
		},
		json: true,
		data: {
			action: 'readForm',
			idForm: editor.props.idForm,
			idPatient: editor.props.idPatient,
			idProduct: editor.props.idProduct
		},
		url: formWSURL
	}
	
	axios.request(config).then(response => {
		var jsonResponse = response.request.response
		var jsonObject = JSON.parse(jsonResponse)
		
		if (jsonObject.error === false) {
			message.success(translate('_system-error-format-form', editor.state.currentLang))
		}
		else {
			if (typeof jsonObject.form !== typeof undefined) {
				var newForm = editor.updatedForm(fixForm(JSON.parse(jsonObject.form)))
				newForm.status = parseInt(jsonObject.status, 10)
				
				var formVersion = editor.updatedForm(newForm)
				var newVersion = JSON.stringify(formVersion)
				editor.formVersions = [newVersion]
				
				var state = {
					path: jsonObject.path,
					form: newForm,
					currentOverlay: null,
					userIsAdmin: false,
					editModeEnabled: false
				}
				
				canSaveChanges = true
				editor.setState(state)
			}
		}
	}).catch(error => {
		editor.closeCurrentPanel()
		message.error(translate('_system-error-loading-form', editor.state.currentLang), 5.0)
		
		editor.setState({
			currentOverlay: null,
			form: createNewForm('Error connecting to Server')
		})
	})
}

export function saveFormChanges(editor, complete, onComplete) {
	if (canSaveChanges !== true) {
		message.success(translate('_system-unable-to-save', editor.state.currentLang), 5.0)
		return
	}
	
	editor.closeCurrentPanel()
	var formJSON = JSON.stringify(editor.state.form)
	if(formID === typeof undefined)
        var formID = 0
    if(idParticularForm === typeof undefined)
        var idParticularForm = 0
	
	var config = {
		method: 'post',
		data: {
			action: 'writeForm',
			idForm: editor.props.idForm,
			idPatient: editor.props.idPatient,
			idProduct: editor.props.idProduct,
			json: formJSON,
			complete: complete
		},
		url: formWSURL
	}
	
	axios.request(config).then(response => {
		message.success(translate('_system-success-save-form', editor.state.currentLang), 5.0)
		onComplete()
	}).catch(error => {
		console.log(error)
		message.error(translate('_system-error-save-form', editor.state.currentLang), 5.0)
	})
}

export function twoDigitsFrom(number) {
   return ('0' + number).slice(-2);
}

export function decodeHtml(html) {
	if (typeof html === typeof undefined || html.length === 0) {
		return ""
	}
	
	var txt = document.createElement('textarea')
	txt.innerHTML = html
	return txt.value
}

var separatorId = 0
var canSaveChanges = false

export function moveArrayElement(array, old_index, new_index) {
	if (new_index >= array.length) {
			var k = new_index - array.length
			while ((k--) + 1) {
					array.push(undefined)
			}
	}
	
	array.splice(new_index, 0, array.splice(old_index, 1)[0])
	
	return array
}

export function addFormulaFixIndicator(elementJSON, idElement) {
	var idToFind = '['+idElement+'_'
	var regex = new RegExp('\\'+idToFind, 'g')
	return elementJSON.replace(regex, 'UPDATE'+idToFind)
}

function fixElementCriterias(element) {
	var allCriterias = element.criteria
	if (typeof allCriterias === typeof undefined || allCriterias === null) {
		element.criteria = []
		return
	}
	
	var totalCriterias = allCriterias.length
	for (var i=0;i<totalCriterias;i++) {
		var criteria = allCriterias[i]
		
		if (typeof criteria.optional !== 'boolean') {
			criteria.optional = (criteria.optional === 'true')
		}
		
		if (typeof criteria.equality === typeof undefined) {
			criteria.equality = "==="
		}
	}
}

function fixElement(element) {
	if (element.type === 'separator') {
		element.fieldID = 'sep_' + separatorId
		
		if (typeof element.background === typeof undefined || element.background.length === 0) {
			element.background = '#E4F1FE'
		}
		
		separatorId += 1
	}
	else {
		if ((element.type === 'file' || element.type === 'firm') && typeof element.maxSize === typeof undefined) {
			element.maxSize = (1024 * 1024 * 5)
		}
		
		if (element.type === 'checkbox' && typeof element.value === 'string') {
			element.value = []
		}
		
		if (element.type === 'dataSource' && typeof element.dataSourceConfig.connection === typeof undefined) {
			element.dataSourceConfig.connection = -1
		}
		
		if (element.type === 'dataSource' && typeof element.dataSourceConfig.sourceExtra === typeof undefined) {
			element.dataSourceConfig.sourceExtra = ''
		}
		
		if (element.type === 'table') {
			if (typeof element.chart === typeof undefined) {
				element.chart = ''
			}
			
			if (typeof element.addedByUser === typeof undefined || typeof element.addedByUser !== 'number') {
				element.addedByUser = 0
			}
			
			if (typeof element.allowsNewElements === typeof undefined) {
				element.allowsNewElements = true
			}
			
			var fields = element.fields
			if (typeof fields !== typeof undefined && fields.length > 0) {
				var totalFields = fields.length
				for (var i=0;i<totalFields;i++) {
					var field = fields[i]
					var items = field.items
					
					if (typeof items !== typeof undefined && items.length > 0) {
						var totalItems = items.length
						for (var j=0;j<totalItems;j++) {
							var item = items[j]
							fixElement(item)
						}
					}
				}
			}
		}
	}
	
	if (typeof element.visible === 'string') {
		element.required = (element.required === 'true')
		element.visible = (element.visible === 'true')
		element.lock = (element.lock === 'true')
	}
	
	fixElementCriterias(element)
	
	if (typeof element.elements === typeof undefined) {
		element.elements = []
	}
	else {
		element.elements.forEach(element => {
			fixElement(element)
		})
	}
}

function fixPage(page) {
	if (typeof page.elements === typeof undefined) {
		page.elements = []
	}
	else {
		page.elements.forEach(element => {
			fixElement(element)
		})
	}
}

export function fixForm(form) {
	separatorId = 0
	
	if (typeof form.editor === typeof undefined) {
		form.editor = '2.0.0'
	}
	
	form.pages.forEach((page, index) => {
		fixPage(page)
	})
	
	return form
}

export function createNewElement(type, lang) {
	var newElement = undefined
	if (type === 'separator') {
		newElement = {}
		newElement.type = 'separator'
		newElement.visible = true
		newElement.criteria = []
		newElement.xmlID = ''
		newElement.fieldID = ''
		newElement.title = translate('Separator', lang)
	}
	else {
		newElement = {}
		newElement.criteria = []
		newElement.flagStatus =  0
		newElement.lock = false
		newElement.required = true
		newElement.value = ''
		newElement.visible = true
		newElement.xmlID = ''
		newElement.fieldID = ''
		newElement.type = type
		newElement.leftText = translate('Left Text', lang)
		newElement.rightText = translate('Right Text', lang)
		
		if (type === 'checkbox') {
			newElement.value = []
			newElement.orientation = 'horizontal'
			newElement.order = 'right'
			newElement.checkboxs = [{value:0,text:translate('First Option', lang),dict:''}]
		}
		else if (type === 'dataSource') {
			newElement.dataSourceConfig = {sourceDB:'',sourceTable:'',sourceValue:'',sourceDescription:'',dependencies:{editorID:'',tableField:''}}
			newElement.dataSourceTemporal = ''
		}
		else if (type === 'date') {
			newElement.rightText = '(dd/MMM/aaaa)'
			newElement.minimum = ''
			newElement.maximum = ''
		}
		else if (type === 'dropdown') {
			newElement.default = -1
			newElement.options = [{value:-1,text:'',dict:''}]
		}
		else if (type === 'external_file') {
			newElement.height = 100
			newElement.width = 100
			newElement.lock = true
		}
		else if (type === 'number' || type === 'calc') {
			newElement.minimum = ''
			newElement.maximum = ''
			newElement.precision = 0
			
			if (type === 'calc') {
				newElement.formula = ''
				newElement.formulaResult = ''
			}
		}
		else if (type === 'radio') {
			newElement.orientation = 'horizontal'
			newElement.order = 'right'
			newElement.radios = [{value:0,text:translate('First Option', lang),dict:''}]
		}
		else if (type === 'slider') {
			newElement.minimum = 0
			newElement.maximum = 100
			newElement.step = 1
		}
		else if (type === 'table') {
			newElement.addedByUser = 0
			newElement.chart = ''
			newElement.orientation = 'horizontal'
			newElement.title = translate('Title', lang)
			newElement.fields = [{
				header:translate(translate('Header', lang), lang),
				items:[createNewElement('', lang)]
			}]
		}
		else if (type === 'test') {
			newElement.questions = ['¿'+translate('Question', lang)+'?']
			newElement.answers = [translate('Answer', lang)]
			newElement.correctAnswers = [1]
		}
		else if (type === 'text' || type === 'textarea') {
			newElement.maxLength = 0
			
			if (type === 'text') {
				newElement.expression = ''
			}
		}
		else if (type === 'time') {
			newElement.rightText = '(hh:mm:ss)'
			newElement.minimum = ''
			newElement.maximum = ''
		}
		
		newElement.elements = []
	}
	
	return JSON.parse(JSON.stringify(newElement))
}

export function connectToDB(panel, config) {
	axios.post(dataSourceWSURL, {
		action: 'readDataSource',
		token: config.token,
		connection: config.connection,
    database: config.sourceDB,
		table: config.sourceTable,
		id: config.sourceValue,
		description: config.sourceDescription,
		extra: config.sourceExtra,
		filter: config.filter,
		page: config.page,
		maxResults: 20,
		scope: config.scope
  })
  .then(function (response) {
		var jsonResponse = response.request.response
		var jsonObject = JSON.parse(jsonResponse)
		
		var data = []
		if (typeof jsonObject.content !== typeof undefined) {
			var content = jsonObject.content
			var totalContent = content.length
			
			for (var entryIndex=0;entryIndex<totalContent;entryIndex++) {
				var entry = content[entryIndex]
				var dataID = decodeHtml(entry[config.sourceValue])
				var dataDescription = decodeHtml(entry[config.sourceDescription])
				var dataExtra = decodeHtml(entry[config.sourceExtra])
				
				data.push({key: entryIndex, id: dataID, description: dataDescription, extra: dataExtra })
			}
		}
		
		panel.totalPages = Math.ceil(jsonObject.total / 20)
    panel.setState({
			data: data,
			loading: false
		})
  })
  .catch(function (error) {
    panel.props.closePanel()
  })
}

export function newPage(lang) {
	var newPage = {
		elements: [],
		instructions: '',
		name: translate('New Page', lang),
		subtitle: ''
	}
	
	return newPage
}

export function createNewForm(name, lang) {
	var pageName = translate('New Page', lang)
	
	return JSON.parse('{"name":"'+name+'","version":"1.0.0","description":"","editor":"2.0.0","pages":[{"name":"'+pageName+'","subtitle":"","instructions":"", "elements":[]}]}')
}

export function removeAt(sourceArray, index) {
	return [
		...sourceArray.slice(0, index),
		...sourceArray.slice(index + 1)
	]
}

export function scrollToBottom(DOMElementID) {
		var domElement = document.getElementById(DOMElementID)
		if (typeof domElement !== typeof undefined) {
			domElement.scrollTop = domElement.scrollHeight
		}
}

export function scrollToTop(DOMElementID) {
		var domElement = document.getElementById(DOMElementID)
		if (typeof domElement !== typeof undefined) {
			domElement.scrollTop = 0
		}
}

function searchInTable(table, idDesired, property) {
	var fields = table.fields
	
	if (typeof fields !== typeof undefined && fields.length > 0) {
		var totalFields = fields.length
		for (var i=0;i<totalFields;i++) {
			var field = fields[i]
			var items = field.items
			
			if (typeof items !== typeof undefined && items.length > 0) {
				var totalItems = items.length
				for (var j=0;j<totalItems;j++) {
					var item = items[j]
					
					if ((property === 'idElement' && item.fieldID === idDesired) || (property === 'idXML' && item.xmlID === idDesired)) {
						return item
					}
				}
			}
		}
	}
	
	return null
}

function searchIn(parent, idDesired, property) {
	if (typeof parent.elements === typeof undefined) {
		return null
	}
	
	var limit = parent.elements.length
	var desiredElement = null
	
	for (var i=0;i<limit;i++) {
		var element = parent.elements[i]
		
		if ((property === 'idElement' && element.fieldID === idDesired) || (property === 'idXML' && element.xmlID === idDesired)) {
			desiredElement = element
			break
		}
		
		if (desiredElement === null && element.type === 'table') {
			desiredElement = searchInTable(element, idDesired, property)
		}
		
		if (desiredElement === null) {
			desiredElement = searchIn(element, idDesired, property)
		}
	}
	
	return desiredElement
}

export function getElementWith(form, idDesired, property) {
	if (idDesired.length === 0) {
		return null
	}
	
	let pages = form.pages
	var limit = pages.length
	var element = null
	
	for (var i=0;i<limit;i++) {
		var page = pages[i]
		element = searchIn(page, idDesired, property)
		
		if (element !== null) {
			break
		}
	}
	
	return element
}

export function uuid(length = 32) {
  var uuid = "", i, random
	
  for (i = 0; i < length; i++) {
    random = Math.random() * 16 | 0
    if (i === 8 || i === 12 || i === 16 || i === 20) {
      uuid += "-"
    }
		
    uuid += (i === 12 ? 4 : (i === 16 ? (random & (3 | 8)) : random)).toString(16)
  }
	
  return uuid
}

export function arrayContentsToString(sourceArray) {
	var newArray = []
	
	for (var i=0;i<sourceArray.length;i++) {
		newArray.push(sourceArray[i] + '')
	}
	
	return newArray
}

export function generateRange(start, end, step) {
    var range = []
    var typeofStart = typeof start
    var typeofEnd = typeof end

    if (step === 0) {
        throw TypeError("Step cannot be zero.");
    }

    if (typeofStart === "undefined" || typeofEnd === "undefined") {
        throw TypeError("Must pass start and end arguments.")
    } else if (typeofStart !== typeofEnd) {
        throw TypeError("Start and end arguments must be of same type.")
    }

    typeof step === "undefined" && (step = 1)

    if (end < start) {
        step = -step
    }

    if (typeofStart === "number") {
        while (step > 0 ? end >= start : end <= start) {
            range.push(start)
            start += step
        }

    }
		else if (typeofStart === "string") {
        if (start.length !== 1 || end.length !== 1) {
            throw TypeError("Only strings with one character are supported.")
        }
				
        start = start.charCodeAt(0)
        end = end.charCodeAt(0)
				
        while (step > 0 ? end >= start : end <= start) {
            range.push(String.fromCharCode(start))
            start += step
        }

    }

    return range;
}

export function getTableElementMaxElements(headers) {
	var max = 0
	
	for (var headerIndex=0; headerIndex < headers.length; headerIndex++) {
		var header = headers[headerIndex]
		var items = header.items
		
		if (items.length > max) {
			max = items.length
		}
	}
	
	return max
}

export function cleanElement(element) {
	if (element.type === 'separator') {
		return
	}
	
	if (element.lock === false) {
		element.value = ''
	}
	
	if (element.type === 'table') {
		var headers = element.fields
		for (var headerIndex=0;headerIndex<headers.length;headerIndex++) {
			var header = headers[headerIndex]
			header.items.forEach(item => {
				cleanElement(item)
			})
		}
	}
	
	if (typeof element.elements !== typeof undefined) {
		element.elements.forEach(internalElement => {
			cleanElement(internalElement)
		})
	}
}

export function cleanForm(form) {
	form.pages.forEach(page => {
		page.elements.forEach(element => {
			cleanElement(element)
		})
	})
}

function getVisibleElementsFrom(form, element, putInArray) {
	if (typeof element.type !== typeof undefined) {
		var visible = false
		var criteria = element.criteria
		
		if ((typeof criteria === typeof undefined || criteria.length === 0) && (putInArray.includes(element.fieldID) === false)) {
			visible = true
		}
		else if (elementFulfillsCriteria(form, element) && (putInArray.includes(element.fieldID) === false)) {
			visible = true
		}
		
		if (visible) {
			putInArray.push(element.fieldID)
			
			if (element.type === 'table') {
				var fields = element.fields
				if (typeof fields !== typeof undefined && fields.length > 0) {
					var totalFields = fields.length
					for (var i=0;i<totalFields;i++) {
						var field = fields[i]
						var items = field.items
						
						if (typeof items !== typeof undefined && items.length > 0) {
							var totalItems = items.length
							for (var j=0;j<totalItems;j++) {
								var item = items[j]
								getVisibleElementsFrom(form, item, putInArray)
							}
						}
					}
				}
			}
		}
	}

	var innerElements = element.elements
	if (typeof innerElements !== typeof undefined && Array.isArray(innerElements) && innerElements.length > 0) {
		innerElements.forEach(innerElement => {
			getVisibleElementsFrom(form, innerElement, putInArray)
		})
	}
}

function getUncompleteElementsFrom(form, element, putInArray) {
	if (typeof element.type !== typeof undefined) {
		var visible = false
		var criteria = element.criteria
		
		if ((typeof criteria === typeof undefined || criteria.length === 0) && (putInArray.includes(element.fieldID) === false)) {
			visible = true
		}
		else if (elementFulfillsCriteria(form, element) && (putInArray.includes(element.fieldID) === false)) {
			visible = true
		}
		
		if (visible) {
			if (element.type !== 'table' && element.required === true && (element.value + '').length === 0 && (putInArray.includes(element.fieldID) === false)) {
				putInArray.push(element.fieldID)
			}
			else if (element.type === 'table') {
				var fields = element.fields
				if (typeof fields !== typeof undefined && fields.length > 0) {
					var totalFields = fields.length
					for (var i=0;i<totalFields;i++) {
						var field = fields[i]
						var items = field.items
						
						if (typeof items !== typeof undefined && items.length > 0) {
							var totalItems = items.length
							for (var j=0;j<totalItems;j++) {
								var item = items[j]
								getUncompleteElementsFrom(form, item, putInArray)
							}
						}
					}
				}
			}
		}
	}
	
	var innerElements = element.elements
	if (typeof innerElements !== typeof undefined && Array.isArray(innerElements) && innerElements.length > 0) {
		innerElements.forEach(innerElement => {
			getUncompleteElementsFrom(form, innerElement, putInArray)
		})
	}
}

export function getVisibleElements(form) {
	var elements = []
	form.pages.forEach(page => {
		getVisibleElementsFrom(form, page, elements)
	})
	return elements
}

export function getUncompleteElements(form) {
	var elements = []
	form.pages.forEach(page => {
		getUncompleteElementsFrom(form, page, elements)
	})
	return elements
}