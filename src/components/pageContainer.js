import React, { Component } from 'react'
import PropTypes from 'prop-types'
import FontAwesome from 'react-fontawesome'
import Sortable from 'react-sortablejs'
import Page from './page'

import Tooltip from 'antd/lib/tooltip'
import {translate} from '../functions/translations'

import {newPage, removeAt} from '../functions/functions'
import {removeIDsFrom} from '../functions/reordering'

const sortableOptions = {
	animation: 50,
	delay: 0,
	dragClass: "sortable",
	filter: '.not-sortable',
	ghostClass: 'ghost',
	group: 'page-container',
	handle: '.handle',
	preventOnFilter: true
}

export default class PageContainer extends Component {
	constructor(props) {
		super(props)
		this.state = {
			configPageIndex: -1
		}
	}
	addNewPage = (e) => {
		e.stopPropagation()
		var page = newPage(this.props.editor.state.currentLang)
		
		var form = this.props.editor.state.form
		var pages = form.pages.concat([page])
		var newIndex =form.pages.length
		form.pages = pages
		
		this.props.editor.addVersion(form, false)
		this.props.editor.setState({
			currentPage: newIndex
		})
	}
	clonePage = (index) => {
		if (index < 0 || index >= this.props.editor.state.form.pages.length) {
			return
		}
		
		var currentPage = this.props.editor.state.currentPage
		var form = this.props.editor.state.form
		var lang = this.props.editor.state.currentLang
		var pageToClone = form.pages[index]
		var clone = JSON.parse(JSON.stringify(pageToClone))
		clone.name = clone.name +" ("+translate("copy", lang)+")"
		removeIDsFrom(clone)
		
		var pages = form.pages
		pages.splice(index + 1, 0, clone)
		form.pages = pages
		
		if (currentPage === index) {
			currentPage = (index + 1)
		}
		else if (currentPage > index) {
			currentPage = currentPage + 1
		}
		
		this.props.editor.addVersion(form, false)
		this.props.editor.setState({
			currentPage: currentPage
		})
	}
	deletePage = (index) => {
		var form = this.props.editor.state.form
		var pages = form.pages
		
		if ((pages.length - 1) > 0) {
			var currentPage = this.props.editor.state.currentPage
			pages = removeAt(pages, index)
			form.pages = pages
			
			if (index < currentPage) {
				currentPage = (currentPage - 1)
			}
			else if (index >= pages.length) {
				currentPage = (pages.length - 1)
			}
			
			this.props.editor.addVersion(form, false)
			this.props.editor.setState({
				currentPage: currentPage
			})
		}
		else {
			var lang = this.props.editor.state.currentLang
			var newPageElement = newPage(lang)
			var newPageArray = []
			newPageArray.push(newPageElement)
			
			form.pages.length = 0
			form.pages = newPageArray
			
			this.props.editor.addVersion(form, false)
			this.props.editor.setState({
				currentPage: 0
			})
		}
	}
	selectPage = (index) => {
		if (index === this.props.editor.state.currentPage) {
			return
		}
		
		this.props.editor.setState({
			currentPage: index
		})
	}
	updatePageOrder = (order, sortable, evt) => {
		var currentPage = this.props.editor.state.currentPage
		var newCurrentPage = currentPage
		var form = this.props.editor.state.form
		var pages = form.pages
		var newPages = []
		
		var i=0
		order.forEach(function(element, newIndex) {
			if (element !== 'ignore') {
				var index = parseInt(element, 10)
				var page = pages[index]
				newPages = newPages.concat([page])
				
				if (currentPage === index) {
					newCurrentPage = i
				}
				
				i += 1
			}
		})
		
		form.pages = newPages
		
		this.props.editor.addVersion(form, false)
		this.props.editor.setState({
			currentPage: newCurrentPage
		})
	}
	setConfigPanelIndex = (index) => {
		this.setState({
			configPageIndex: index
		})
	}
	renderPages = () => {
		var currentPage = this.props.editor.state.currentPage
		var editModeEnabled = this.props.editor.state.editModeEnabled
		var form = this.props.editor.state.form
		var lang = this.props.editor.state.currentLang
		
		return form.pages.map((page, index) => {
			return <Page
				key={'page_'+index}
				className={(currentPage === index ? 'active sortable':'sortable')}
				clonePage={this.clonePage}
				configIndex={this.state.configPageIndex}
				deletePage={this.deletePage}
				editModeEnabled={editModeEnabled}
				index={index}
				lang={lang}
				page={page}
				selectPage={this.selectPage}
				setConfigPanelIndex={this.setConfigPanelIndex}/>
		})
	}
	render() {
		var editModeEnabled = this.props.editor.state.editModeEnabled
		var form = this.props.editor.state.form
		var lang = this.props.editor.state.currentLang
		
		if (form === null) {
			return null
		}
		
		return <div style={{minWidth:800}}>
			<Sortable tag="ul" className='page-container' onChange={this.updatePageOrder} style={{display:'flex', flexWrap:'wrap', justifyContent:'flex-start'}} options={sortableOptions}>
				{this.renderPages()}
				{editModeEnabled && <li className="not-sortable" data-id="ignore" style={{cursor:'pointer', display:'flex', alignItems:'center'}}>
					<Tooltip title={translate("New Page", lang)} arrowPointAtCenter={false}><FontAwesome name='plus' style={{color:'#2c6ba8', padding:'0px 15px'}} onClick={this.addNewPage}/></Tooltip>
				</li>}
			</Sortable>
		</div>
	}
}

PageContainer.propTypes = {
	editor: PropTypes.object.isRequired
}