import React, {Component} from 'react'
import Sortable from 'react-sortablejs'
import PropTypes from 'prop-types'

import {translate} from '../functions/translations'

const sortableOptions = {
	ghostClass: 'tool-ghost',
	group:{
		name:'element-container',
		pull:'clone',
		put:false
	},
	sort:false
}

class ToolItem extends Component {
	addItem = (e) => {
		e.stopPropagation()
		this.props.add(this.props.type)
	}
	render() {
		return (
			<div data-id={'tool_'+this.props.type} style={{alignItems:'center', cursor:'pointer', display:'flex', height:40, justifyContent:'center', minWidth:40}} onClick={this.addItem}>
				<img title={translate('_system-tool-name-'+this.props.type, this.props.lang)} src={'./img/tool_'+this.props.type+'.png'} width='40px' height='40px' alt='tool' style={{display:'block'}}/>
			</div>
		);
	}
}

const availableElements = ['text', 'number', 'calc', 'textarea', 'dropdown', 'separator', 'date', 'time', 'radio', 'checkbox', 'table', 'dataSource', 'slider', 'test', 'file','external_file', 'firm']
export default class ToolPanel extends Component {
	renderChildren = () => {
		return availableElements.map((type, index) => {
			return <ToolItem lang={this.props.lang} type={type} key={'Tool'+index} add={this.props.addElement}/>
		})
	}
	render() {
		return <Sortable options={sortableOptions} className='palette-theme' style={{flex:'none',height:'100%',listStyleType:'none',margin:0,padding:0,width:40,display:'inline-block'}}>
			{this.renderChildren()}
		</Sortable>
	}
}

ToolPanel.propTypes = {
	addElement: PropTypes.func.isRequired
}