import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Checkbox from 'antd/lib/checkbox'
import {decodeHtml} from '../functions/functions'

const CheckboxGroup = Checkbox.Group

export default class ElementItemsChecklist extends Component {
	constructor(props) {
		super(props)
		
		this.state = {
			defaultValue: []
		}
	}
	saveValue = (newValue) => {
		var eme = this.props.editor.state.editModeEnabled
		
		if (eme === false) {
			this.props.element.value = newValue.sort((a, b) => a - b)
			var updatedForm = this.props.editor.state.form
			
			this.props.editor.addVersion(updatedForm, true)
		}
		else {
			this.setState({
				defaultValue: newValue
			})
		}
  }
	getData = () => {
		var data = []
		this.props.element.checkboxs.forEach(function(checkbox, index) {
			var entry = {label: decodeHtml(checkbox.text), value: parseInt(checkbox.value, 10)}
			data.push(entry)
		})
		
		return data
	}
	render() {
		var value = this.state.defaultValue
		var targetClass = this.props.element.orientation === 'horizontal' ? 'flex-row':'flex-column'
		var blocked = false
		
		var eme = this.props.editor.state.editModeEnabled
		if (eme === false) {
			value = this.props.element.value
			if (typeof value === 'string') {
				value = []
			}
			
			blocked = (this.props.element.lock === true) || (this.props.editor.state.form.status === 2)
		}
		
		return <div style={{alignItems:'center', justifyContent:'center', display:'inline-flex', marginLeft:10, minHeight:this.props.minHeight}}>
			<CheckboxGroup disabled={blocked} className={targetClass} options={this.getData()} defaultValue={value} onChange={this.saveValue}/>
		</div>
	}
}

ElementItemsChecklist.propTypes = {
	editor: PropTypes.object.isRequired,
	element: PropTypes.object.isRequired
}