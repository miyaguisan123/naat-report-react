import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Form from 'antd/lib/form'
import Icon from 'antd/lib/icon'
import Input from 'antd/lib/input'
import InputNumber from 'antd/lib/input-number'

import {decodeHtml} from '../functions/functions'

const FormItem = Form.Item

export default class Option extends Component {
	deleteOption = (e) => {
		e.stopPropagation()
		this.props.deleteOption(this.props.index)
	}
	updateValue = (newValue) => {
		if (typeof newValue === typeof undefined) {
			return
		}
		
		this.props.updateOption(this.props.index, newValue, this.props.option.text, this.props.option.dict)
	}
	updateText = (e) => {
		e.stopPropagation()
		this.props.updateOption(this.props.index, this.props.option.value, e.target.value, this.props.option.dict)
	}
	updateDictionary = (e) => {
		e.stopPropagation()
		this.props.updateOption(this.props.index, this.props.option.value, this.props.option.text, e.target.value)
	}
	render() {
		var isInvalid = (this.props.option.text.length === 0 || this.props.option.dict.length === 0)
		
		return <FormItem style={{display:'flex', alignItems:'center', marginBottom:'5px'}} validateStatus={isInvalid ? 'error':''}>
			<InputNumber value={this.props.option.value} style={{color:'#000000', display:'inline-flex', flex:'none', fontStyle:'italic', width:'50px'}} onChange={this.updateValue}/>
			<Input value={decodeHtml(this.props.option.text)} style={{color:'#2c6ba8', display:'inline-flex', flex:1, fontStyle:'italic'}} onChange={this.updateText}/>
			&nbsp;&nbsp;
			<Input value={decodeHtml(this.props.option.dict)} style={{color:'#2c6ba8', display:'inline-flex', flex:1, fontStyle:'italic'}} onChange={this.updateDictionary}/>
			&nbsp;&nbsp;
			<Icon type='minus-circle' onClick={this.deleteOption} style={{color:'#2c6ba8', cursor:'pointer'}}/>
		</FormItem>
	}
}

Option.propTypes = {
	option: PropTypes.object.isRequired,
	deleteOption: PropTypes.func.isRequired,
	lang: PropTypes.string.isRequired,
	index: PropTypes.number.isRequired,
	updateOption: PropTypes.func.isRequired
}