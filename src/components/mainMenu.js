import React, {Component} from 'react'

import Icon from 'antd/lib/icon'
import Menu from 'antd/lib/menu'

import PropTypes from 'prop-types'
import AboutPanel from './aboutPanel'
import {cleanForm} from '../functions/functions'
import {translate} from '../functions/translations'

const SubMenu = Menu.SubMenu

export default class MainMenu extends Component {
	constructor(props) {
		super(props)
		this.state = {
			lang: this.props.editor.state.currentLang
		}
	}
	handleClick = (e) => {
		e.domEvent.stopPropagation()
		
		if (e.key === 'save') {
			this.props.editor.save()
			this.props.close()
		}
		else if (e.key === 'eng' || e.key === 'esp') {
			this.props.editor.setState({currentLang:e.key})
			this.setState({
				lang: e.key
			})
		}
		else if (e.key === 'editMode') {
			var currentEditMode = this.props.editor.state.editModeEnabled
			var form = this.props.editor.state.form
			cleanForm(form)
			
			this.props.editor.setState({
				currentOverlay: null,
				editModeEnabled:!currentEditMode
			})
			
			this.props.close()
		}
		else if (e.key === 'about') {
			var about = <AboutPanel lang={this.state.lang}/>
			this.props.editor.openPanel(about, true, true, true)
			this.props.close()
		}
		else if (e.key === 'undo') {
			this.props.editor.removeLastVersion()
		}
		else if (e.key === 'exit') {
			window.location = 'viewProject.php?idProject='+this.props.editor.props.idProject+'#projects'
		}
		else if (e.key === 'print') {
			this.props.editor.print()
			this.props.close()
		}
	}
	render() {
		var lang = this.state.lang
		var editModeEnabled = this.props.editor.state.editModeEnabled
		var text = 'Enable Edit Mode'
		
		if (editModeEnabled === true) {
			text = 'Disable Edit Mode'
		}
		
		return <Menu onClick={this.handleClick} mode='vertical' style={{backgroundColor:'#ffffff',  border:'none', display:'inline-flex',flexDirection:'column', marginLeft:35, marginTop:30}}>
			<SubMenu key='lang' title={<span><Icon type='flag'/>{translate('Language', lang)}</span>}>
				<Menu.Item key='eng'>{translate('English', lang)}</Menu.Item>
				<Menu.Item key='esp'>{translate('Spanish', lang)}</Menu.Item>
			</SubMenu>
			
			{this.props.editor.state.userIsAdmin && <Menu.Item key='save'><span><Icon type='save'/></span>{(translate('_system-save-changes', lang))}</Menu.Item>}
			{this.props.editor.state.userIsAdmin && <Menu.Item key='editMode'><span><Icon type='edit'/></span>{(translate(text, lang))}</Menu.Item>}
			
			{this.props.editor.state.editModeEnabled && <Menu.Item key='undo'><span><Icon type='double-left'/></span>{(translate('Undo last change', lang))}</Menu.Item>}
			<Menu.Item key='print'><span><Icon type='printer'/></span>{(translate('Print', lang))}</Menu.Item>
			<Menu.Item key='about'><span><Icon type='info-circle-o'/></span>{(translate('About', lang))}</Menu.Item>
			<Menu.Item key='exit'><span><Icon type='logout'/></span>{(translate('_system-exit', lang))}</Menu.Item>
		</Menu>
	}
}

MainMenu.propTypes = {
	close: PropTypes.func.isRequired,
	editor: PropTypes.object.isRequired
}