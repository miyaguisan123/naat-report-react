import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class ElementItemsNumeric extends Component {
	constructor(props) {
		super(props)
		var eme = this.props.editor.state.editModeEnabled
		var value = ((eme === false) ? parseFloat(this.props.element.value) : '')
		
		this.state = {
			defaultValue: value
		}
	}
	update = (e) => {
		e.stopPropagation()
		this.setState({
			defaultValue: parseFloat(e.target.value)
		})
  }
	saveValue = (e) => {
		e.stopPropagation()
		this.setState({
			defaultValue: parseFloat(e.target.value)
		})
		
		var eme = this.props.editor.state.editModeEnabled
		
		if (eme === false || (eme === true && this.props.element.lock === true)) {
			var value = parseFloat(e.target.value)
			this.props.element.value = value
			var updatedForm = this.props.editor.state.form
			this.props.editor.addVersion(updatedForm, true)
		}
	}
	shit = (e) => {
		console.log(e)
	}
	render() {
		var className = ''
		var value = this.state.defaultValue
		var eme = this.props.editor.state.editModeEnabled
		var step = (1.0 / Math.pow(10, parseInt(this.props.element.precision, 10)))
		var blocked = false
		
		if (isNaN(value) || (value+'').length === 0) {
			var alternateValue = parseFloat(this.props.element.value)
			if (isNaN(alternateValue) === false) {
				value = alternateValue
			}
			else {
				value = ''
			}
		}
		
		if (eme === false) {
			blocked = (this.props.element.lock === true) || (this.props.editor.state.form.status === 2)
			var min = parseFloat(this.props.element.minimum)
			var max = parseFloat(this.props.element.maximum)
			
			if (parseFloat(value) < min || parseFloat(value) > max) {
				className = 'error'
			}
		}
		else {
			value = ''
		}
		
		return <div className={className} style={{alignItems:'center', justifyContent:'center', display:'inline-flex', marginLeft:10, minHeight:this.props.minHeight}}>
			{(blocked === true && eme === false) && <input className={className} type='number' step={step} disabled value={value} onChange={this.update} onBlur={this.saveValue} style={{width:'80px',backgroundColor:'#eeeeee',paddingLeft:'2px',paddingRight:'2px',height:'25px',border:'1px solid #cccccc',borderRadius:'3px'}}/>}
			{(blocked === false || eme === true) && <input className={className} type='number' step={step} value={value} onChange={this.update} onBlur={this.saveValue} style={{width:'80px',paddingLeft:'2px',paddingRight:'2px',height:'25px',border:'1px solid #cccccc',borderRadius:'3px'}}/>}
		</div>
	}
}

ElementItemsNumeric.propTypes = {
	editor: PropTypes.object.isRequired,
	element: PropTypes.object.isRequired
}