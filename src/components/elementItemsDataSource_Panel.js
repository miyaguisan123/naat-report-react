import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Button from 'antd/lib/button'
import Input from 'antd/lib/input'
import Table from 'antd/lib/table'

import {cleanDependenciesRelatedTo} from '../functions/elementFunctions'
import {connectToDB, decodeHtml, getElementWith} from '../functions/functions'
import {translate} from '../functions/translations'

const ButtonGroup = Button.Group
	
export default class DataSourcePanel extends Component {
	constructor(props) {
		super(props)
		this.state = {
			loading: false,
			data: [],
			selectedRowKeys: []
		}
		
		this.page = 1
		this.totalPages = 1
		this.filter = ""
	}
	handleClick = (e) => {
		e.stopPropagation()
	}
	componentDidMount() {
		this.applyFilter()
	}
	applyFilter = () => {
		var config = this.props.container.props.element.dataSourceConfig
		
		if (config.sourceDB.length === 0 || config.sourceTable.length === 0 || config.sourceValue.length === 0 || config.sourceDescription.length === 0) {
			this.props.closePanel()
			return false
		}
		
		var dependency = this.props.container.props.element.dataSourceConfig.dependencies
		var scope = []
		
		if (typeof dependency !== typeof undefined) {
			if (dependency.editorID.length > 0 && dependency.tableField.length > 0) {
				var form = this.props.container.props.editor.state.form
				var element = getElementWith(form, dependency.editorID, 'idElement')
				var currentValue = element.value
				
				var entry = {
					name: dependency.tableField,
					value: currentValue
				}
				
				scope.push(entry)
			}
		}
		
		this.setState({
			loading: true,
			selectedRowKeys: []
		})
		
		config.token = this.props.container.props.editor.props.token
		config.scope = scope
		config.page = this.page
		config.filter = this.filter
		connectToDB(this, config)
	}
	errorLoading = () => {
		this.props.closePanel()
	}
	updateFilter = (e) => {
		e.stopPropagation()
		this.filter = e.target.value
	}
	updatePage = (e) => {
		e.stopPropagation()
		this.page = e.target.value
		this.applyFilter()
	}
	updateSelection = (selectedRowKeys) => {
		this.setState({
			selectedRowKeys
		})
	}
	saveValue = () => {
		if (typeof this.state.selectedRowKeys !== typeof undefined) {
			var form = this.props.container.props.editor.state.form
			var idElement = this.props.container.props.element.fieldID
			cleanDependenciesRelatedTo(form, idElement)
			
			this.props.container.props.element.key = this.state.selectedRowKeys.key
			this.props.container.props.element.value = this.state.selectedRowKeys.id
			this.props.container.props.element.dataSourceTemporal = this.state.selectedRowKeys.description
		}
		
		this.props.closePanel()
	}
	renderPages = () => {
		var elements = []
		for (var i=1;i<=this.totalPages;i++) {
			var element = <option key={'db_option_'+i} value={i}>{i}</option>
			elements.push(element)
		}
		return elements
	}
	render() {
		var locale = {filterConfirm:'Ok',filterReset:'Reset',emptyText:translate('No Results', this.props.container.props.editor.state.currentLang)}
		var config = this.props.container.props.element.dataSourceConfig
		var columns = []
		
		if (config.sourceExtra.length > 0) {
			columns = [{
				title: config.sourceValue,
				dataIndex: 'id',
				key: 'id',
				sorter: (a, b) => { return (a.id+'').localeCompare((b.id+'')) }
			},
			{
				title: config.sourceDescription,
				dataIndex: 'description',
				key: 'description',
				sorter: (a, b) => { return (a.description+'').localeCompare((b.description+'')) }
			},
			{
				title: config.sourceExtra,
				dataIndex: 'extra',
				key: 'extra',
				sorter: (a, b) => { return (a.extra+'').localeCompare((b.extra+'')) }
			}]
		}
		else {
			columns = [{
				title: config.sourceValue,
				dataIndex: 'id',
				key: 'id',
				sorter: (a, b) => { return (a.id+'').localeCompare((b.id+'')) }
			},
			{
				title: config.sourceDescription,
				dataIndex: 'description',
				key: 'description',
				sorter: (a, b) => { return (a.description+'').localeCompare((b.description+'')) }
			}]
		}
		
		return <div className='rounded dropshadow' onClick={this.handleClick} style={{backgroundColor:'#ffffff', marginLeft:10, minHeight:this.props.minHeight}}>
			<div style={{backgroundColor:'#2c6ba8', color:'#ffffff', display:'block', width:'100%', textAlign:'center', fontWeight:'bold',padding:'5px'}}>{decodeHtml(config.sourceDB)}</div>
			<div style={{display:'block', width:'100%', textAlign:'center', fontWeight:'bold',padding:'5px'}}>
				<Input size="large" style={{width:'100%'}} addonAfter={<Button onClick={this.applyFilter} size="small" type="primary" shape="circle" icon="search" />} defaultValue={this.state.filter} onChange={this.updateFilter}/>
			</div>
			<Table rowSelection={{selectedRowKeys: [this.state.selectedRowKeys.key], onSelect: this.updateSelection, type:'radio',}} pagination={false} scroll={{ y: 400 }} columns={columns} dataSource={this.state.data} loading={this.state.loading} locale={locale} style={{padding:'5px'}}/>
			<div style={{display:'flex', flexDirection:'row', width:'100%', textAlign:'center', fontWeight:'bold',padding:'5px'}}>
				<ButtonGroup>
					<Button onClick={this.saveValue} type='primary'>Ok</Button>
				</ButtonGroup>
				<select defaultValue={this.page} onChange={this.updatePage} style={{borderRadius:3, border: '1px solid #cccccc', display:'inline-flex', marginLeft:'auto', minWidth:50}}>
					{this.renderPages()}
				</select>
			</div>
		</div>
	}
}

DataSourcePanel.propTypes = {
	closePanel: PropTypes.func.isRequired,
	container: PropTypes.object.isRequired
}