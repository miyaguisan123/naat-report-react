import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {decodeHtml} from '../functions/functions'
	
export default class ElementItemsTextarea extends Component {
	constructor(props) {
		super(props)
		var eme = this.props.editor.state.editModeEnabled
		
		this.state = {
			defaultValue: ((eme === false) ? this.props.element.value : '')
		}
	}
	update = (event) => {
		event.stopPropagation()
		
		var newValue = event.target.value
		this.setState({
			defaultValue: newValue
		})
  }
	saveValue = (event) => {
		event.stopPropagation()
		var eme = this.props.editor.state.editModeEnabled
		
		if (eme === false) {
			var newValue = this.state.defaultValue
			this.props.element.value = newValue
			var updatedForm = this.props.editor.state.form
			
			this.props.editor.addVersion(updatedForm, true)
		}
	}
	render() {
		var className = ''
		var value = this.state.defaultValue
		var eme = this.props.editor.state.editModeEnabled
		var blocked = false
		
		if (eme === false) {
			blocked = (this.props.element.lock === true) || (this.props.editor.state.form.status === 2)
			var maxLength = parseInt(this.props.element.maxLength, 10)
			
			if (maxLength > 0 && value.length > maxLength) {
				className = 'error'
			}
		}
		
		var style = {
			marginLeft:10,
			resize:'both',
			minHeight:30,
			minWidth:200
		}
		
		if (this.props.isInsideTable === true) {
			style.rows = 1
		}
		
		return <textarea className={className} style={style} value={decodeHtml(this.state.defaultValue)} onChange={this.update} onBlur={this.saveValue} disabled={blocked}/>
	}
}

ElementItemsTextarea.propTypes = {
	editor: PropTypes.object.isRequired,
	element: PropTypes.object.isRequired
}