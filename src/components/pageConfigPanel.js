import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Button from 'antd/lib/button'
import FontAwesome from 'react-fontawesome'
import {decodeHtml} from '../functions/functions'
import {translate} from '../functions/translations'

const ButtonGroup = Button.Group

export default class PageConfigPanel extends Component {
	constructor(props) {
		super(props)
		this.state = {
			pageName:this.props.page.name
		}
	}
	componentDidMount(){
    this.textArea.focus();
  }
	handleClick = (e) => {
		e.preventDefault()
		e.stopPropagation()
	}
	updateName = (e) => {
		this.setState({
			pageName:e.target.value
		})
	}
	saveChanges = (e) => {
		e.stopPropagation()
		
		this.props.page.name = this.state.pageName
		this.props.close()
	}
	render() {
		return <div className="dropshadow" onClick={this.handleClick} style={{backgroundColor:'#ffffff', fontSize:'90%', borderRadius:3, display:'flex', flexDirection:'column', minWidth:340, padding:10}}>
			<div style={{alignItems:'flex-start', display:'flex'}}>
				<span style={{color:'#2c6ba8',fontWeight:'bold',marginRight:10}}>{translate("Configuration", this.props.lang)}:</span>
				<span style={{flex:2}}>{translate('Page', this.props.lang)}</span>
				<FontAwesome name='times' onClick={this.props.close} style={{alingItems:'center', color:'#2c6ba8', cursor:'pointer', display:'flex', justifyContent:'flex-end', height:25, width: 30}}/>
			</div>
			<div style={{alignItems:'flex-start', display:'flex'}}>
				<textarea ref={(textarea) => { this.textArea = textarea }} style={{display:'inline-flex', height:150,width:'100%'}} defaultValue={decodeHtml(this.state.pageName)} onChange={this.updateName}/>
			</div>
			<ButtonGroup style={{display:'flex', justifyContent:'flex-end', padding:5}}>
				<Button onClick={this.props.close}>{translate('Cancel', this.props.lang)}</Button>
				<Button type='primary' onClick={this.saveChanges}>{translate('Save', this.props.lang)}</Button>
			</ButtonGroup>
		</div>
	}
}

PageConfigPanel.PropTypes = {
	close: PropTypes.func.isRequired,
	page: PropTypes.object.isRequired
}