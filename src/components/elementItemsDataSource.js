import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {decodeHtml} from '../functions/functions'
	
export default class ElementItemsDataSource extends Component {
	render() {
		var value = ''
		var blocked = false
		
		if (this.props.editor.state.editModeEnabled === false) {
			value = this.props.element.dataSourceTemporal
			blocked = (this.props.element.lock === true) || (this.props.editor.state.form.status === 2)
		}
		
		return <div style={{alignItems:'center', justifyContent:'center', display:'inline-flex', marginLeft:10, minHeight:this.props.minHeight}}>
			{blocked === true && <label className='flex-row' style={{backgroundColor:'#eeeeee', border:'1px solid #d9d9d9', borderRadius:'4px', height:'28px', lineHeight:'28px', minWidth:'150px', paddingLeft:'3px', paddingRight:'3px'}}>{decodeHtml(value)}</label>}
			{blocked === false && <label className='flex-row' onClick={this.props.openDataBasePanel} style={{backgroundColor:'#eeeeee', border:'1px solid #d9d9d9', borderRadius:'4px', cursor:'pointer', height:'28px', lineHeight:'28px', minWidth:'150px', paddingLeft:'3px', paddingRight:'3px'}}>{decodeHtml(value)}</label>}
		</div>
	}
}

ElementItemsDataSource.propTypes = {
	editor: PropTypes.object.isRequired,
	element: PropTypes.object.isRequired,
	openDataBasePanel: PropTypes.func.isRequired
}