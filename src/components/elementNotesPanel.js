import axios from 'axios'
import React, {Component} from 'react'
import FontAwesome from 'react-fontawesome'
import PropTypes from 'prop-types'
import NoteEntry from './noteEntry'

import Button from 'antd/lib/button'
import Radio from 'antd/lib/radio'
import Spin from 'antd/lib/spin';

import {notesWSURL} from '../functions/functions'
import {translate} from '../functions/translations'

const RadioGroup = Radio.Group

export default class ElementNotesPanel extends Component {
	constructor(props) {
		super(props)
		
		this.state = {
			notes: [],
			loading: false,
			showHiddenFeatures: false,
			addingNote: false,
			newNoteType: 2,
			newNoteSummary: '',
			missingSummary: true
		}
	}
	componentWillMount() {
		var idElement = this.props.elementObject.props.element.fieldID
		var config = {
			method: 'get',
			headers: {
				'Accept':'application/json, text/javascript, */*; q=0.01'
			},
			json: true,
			params: {
				token: this.props.token,
				idElement: idElement,
				idForm: this.props.idForm
			},
			url: notesWSURL
		}
		
		axios.request(config).then(response => {
			var jsonResponse = response.request.response
			var currentNotes = JSON.parse(jsonResponse)
			
			if (typeof currentNotes !== typeof undefined && currentNotes !== null) {
				this.setState({
					notes: currentNotes
				})
			}
			else {
				//error response
			}
		}).catch(error => {
			//error loading
		})
	}
	addNote = () => {
		if (this.state.newNoteSummary.length === 0) {
			this.setState({
				missingSummary:true
			})
			
			return false
		}
		
		var idElement = this.props.elementObject.props.element.fieldID
		var config = {
			method: 'post',
			headers: {
				'Accept':'application/json, text/javascript, */*; q=0.01'
			},
			json: true,
			data: {
				action: 'addNote',
				token: this.props.token,
				idElement: idElement,
				idForm: this.props.idForm,
				summary: this.state.newNoteSummary,
				type: this.state.newNoteType,
				value: this.props.elementObject.props.element.value
			},
			url: notesWSURL
		}
		
		axios.request(config).then(response => {
			var jsonResponse = response.request.response
			var newNote = JSON.parse(jsonResponse)
			
			if (typeof newNote !== typeof undefined && newNote !== null) {
				var currentNotes = this.state.notes
				currentNotes.unshift(newNote)
				
				this.setState({
					notes: currentNotes,
					addingNote: false,
					missingSummary: true,
					newNoteSummary: '',
					newNoteType: 2
				})
				
				this.props.elementObject.setState({
					notes: currentNotes
				})
			}
			else {
				//error response
			}
		}).catch(error => {
			//error loading
		})
	}
	handleClick = (e) => {
		e.stopPropagation()
	}
	cancelNote = (e) => {
		this.setState({
			addingNote: false,
			missingSummary: true,
			newNoteSummary: '',
			newNoteType: 2
		})
	}
	requestNewNote = () => {
		this.setState({
			addingNote: true
		})
	}
	setNewNoteType = (e) => {
		this.setState({
			newNoteType: e.target.value
		})
	}
	setNewNoteSummary = (e) => {
		var newText = e.target.value
		var empty = newText.length === 0
		this.setState({
			newNoteSummary: newText,
			missingSummary: empty
		})
	}
	renderNotes = () => {
		return this.state.notes.map((note, index) => {
			return <NoteEntry editable={this.props.editable} key={'note_'+index} note={note} lang={this.props.lang} token={this.props.token}/>
		})
	}
	render() {
		const radioStyle = {
		  display: 'block',
		  height: '30px',
		  lineHeight: '30px'
		}
		
		const border = this.state.missingSummary === true ? '1px solid red' : ''
		
		return <div className='dropshadow' onClick={this.handleClick} style={{backgroundColor:'#ffffff', borderRadius:3, display:'flex', flexDirection:'column', minWidth:200, overflow:'hidden'}}>
			{this.state.loading && <div style={{display:'flex', flexDirection:'column', justifyContent:'center', width:'200px', height:'200px'}}>
				<Spin />
			</div>}
			{(this.state.loading === false && this.state.addingNote === false) && <div style={{display:'flex', flexDirection:'column', justifyContent:'flex-start', width:'500px'}}>
				<div style={{backgroundColor:'#304f59',color:'#ffffff', alignItems:'flex-start', display:'flex'}}>
					<span style={{flex:1, textAlign:'center',fontWeight:'bold',lineHeight:'40px'}}>{translate('Notes', this.props.lang)}</span>
					<FontAwesome name='times' onClick={this.props.close} style={{alingItems:'center', color:'#ffffff', cursor:'pointer', display:'flex', justifyContent:'center', alignItems:'center', height:40, width: 40}}/>
				</div>
				<div style={{backgroundColor:'#efefef',border:'1px solid #cccccc',borderRadius:'3px',color:'#ffffff',display:'flex',flexDirection:'column',width:'calc(100% - 10px)',margin:'5px',padding:'5px',overflowY:'scroll',maxHeight:'400px'}}>
					{this.renderNotes()}
				</div>
				{this.props.editable && <div style={{padding:'5px'}}>
					<Button onClick={this.requestNewNote} type='primary' style={{fontWeight:'bold', width:'100%'}}>{translate('Add', this.props.lang)}</Button>
				</div>}
			</div>}
			{(this.state.loading === false && this.state.addingNote === true) && <div style={{display:'flex', flexDirection:'column', justifyContent:'flex-start', width:'500px'}}>
				<div style={{backgroundColor:'#304f59', color:'#ffffff', display:'block', width:'100%', textAlign:'center', fontWeight:'bold',padding:'5px'}}>{translate('Notes', this.props.lang)}</div>
				<div style={{backgroundColor:'#ffffff',display:'flex',flexDirection:'column',width:'calc(100% - 10px)',margin:'5px',padding:'5px'}}>
					<RadioGroup onChange={this.setNewNoteType} value={this.state.newNoteType}>
						{this.showHiddenFeatures && <Radio style={radioStyle} value={1}>Discrepancy</Radio>}
						<Radio style={radioStyle} value={2}>Note</Radio>
						{this.showHiddenFeatures && <Radio style={radioStyle} value={5}>Bypass</Radio>}
					</RadioGroup>
					<div style={{color:'#333333',fontWeight:'bold',marginTop:'20px',marginBottom:'10px'}}>
						{translate('Description',this.props.lang)}:
					</div>
					<textarea onChange={this.setNewNoteSummary} placeholder={translate('Add a description', this.props.lang)} style={{border:border,resize:'vertical',maxHeight:'200px',width:'100%'}}></textarea>
				</div>
				<div style={{padding:'5px'}}>
					<Button onClick={this.addNote} style={{backgroundColor:'#0089cb',color:'#ffffff',fontWeight:'bold',paddingLeft:'30px',paddingRight:'30px'}}>{translate('Add', this.props.lang)}</Button>
					<Button onClick={this.cancelNote} style={{backgroundColor:'#ea533f',color:'#ffffff',fontWeight:'bold',paddingLeft:'30px',paddingRight:'30px'}}>{translate('Cancel', this.props.lang)}</Button>
				</div>
			</div>}
		</div>
	}
}

ElementNotesPanel.propTypes = {
	close: PropTypes.func.isRequired,
	editable: PropTypes.bool.isRequired,
	elementObject: PropTypes.object.isRequired,
	idForm: PropTypes.number.isRequired,
	lang: PropTypes.string.isRequired,
	token: PropTypes.string.isRequired
}