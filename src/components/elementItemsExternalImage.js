import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class ElementItemsExternalImage extends Component {
	render() {
		var height = this.props.element.height
		var width = this.props.element.width
		
		return <div style={{alignItems:'center', justifyContent:'center', display:'inline-flex', marginLeft:10, minHeight:this.props.minHeight}}>
			<img src={this.props.element.value} alt='Attachment' style={{width:width, height:height}}/>
		</div>
	}
}

ElementItemsExternalImage.propTypes = {
	element: PropTypes.object.isRequired
}