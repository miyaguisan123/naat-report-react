import React, { Component } from 'react'
import Icon from 'antd/lib/icon'
import PropTypes from 'prop-types'
import Element from './element'
import Separator from './separator'
import Overlay from './overlay'

import {CartesianGrid, BarChart , Bar, LineChart, Line, XAxis, YAxis} from 'recharts'

import Tooltip from 'antd/lib/tooltip'

import ElementItemsTableDroppable from './elementItemsTable_Droppable'
import TableHeader from './elementItemsTable_Header'

import {createNewElement, decodeHtml, generateRange, getTableElementMaxElements, removeAt} from '../functions/functions'
import {translate} from '../functions/translations'

const placeHolderWidth = '100px'
const placeHolderHeight = '23px'
const alphabetic = generateRange("A", "Z")

export default class ElementItemsTable extends Component {
	constructor(props) {
		super(props)
		
		this.state = {
			currentOverlay: null,
			currentValue: this.props.element.value
		}
	}
	fixTable = () => {
		if (this.props.editor.state.editModeEnabled && this.props.element.addedByUser > 0) {
			var fields = this.props.element.fields
			var adu = this.props.element.addedByUser
			var maxRows = getTableElementMaxElements(fields)
			var start = (maxRows - adu)
			
			fields.forEach(field => {
				var remainingItems = field.items
				remainingItems.splice(start, maxRows - start)
				field.items = remainingItems
			})
		}
		
		this.props.element.addedByUser = 0
	}
	openChart = (e) => {
		e.stopPropagation()
		
		var chartType = this.props.element.chart
		var data = []
		var chartItems = []
		
		if (chartType === 'bars') {
			this.props.element.fields.forEach((header, headerIndex) => {
				var entry = {}
				entry.name = header.header
				header.items.forEach((item, itemIndex) => {
					entry[header.header+'_'+itemIndex] = item.value
					var key = alphabetic[headerIndex]+'_'+itemIndex
					chartItems.push(<Bar key={key} dataKey={key} fill="#8884d8" />)
				})
				
				data.push(entry)
			})
		}
		else if (chartType === 'line') {
			this.props.element.fields.forEach((header, headerIndex) => {
				var entry = {}
				entry.name = header.header
				header.items.forEach((item, itemIndex) => {
					var key = alphabetic[itemIndex]
					entry[key] = item.value
					chartItems.push(<Line key={key} type='monotone' dataKey={key} stroke='#8884d8' />)
				})
				
				data.push(entry)
			})
		}
		
		var overlay = <Overlay dismissable={false} draggable={true} centered={true} close={this.closeChart}>
			<div className='dropshadow' style={{backgroundColor:'#ffffff',borderRadius:'3px', display:'flex', flexDirection:'column', overflow:'hidden'}}>
				<div style={{backgroundColor:'#2c6ba8', color:'#ffffff', display:'flex', flexDirection:'row', height:'30px'}}>
					<div style={{color:'#ffffff', display:'flex', flex:2, fontWeight:'bold', height:'30px', lineHeight:'30px', alignItems:'center', justifyContent:'center'}}>
						{this.props.element.title}
					</div>
					<Icon onClick={this.closeChart} type="close-square" style={{color:'#ffffff', cursor:'pointer', fontSize:'20px',height:'30px',width:'30px', display:'flex', flex:'none', alignItems:'center', justifyContent:'center'}}/>
				</div>
				{chartType === 'bars' && <BarChart width={600} height={400} data={data}>
					<XAxis dataKey='name'/>
					<YAxis/>
					<CartesianGrid strokeDasharray='3 5'/>
					{chartItems}
				</BarChart >}
				{chartType === 'line' && <LineChart width={600} height={400} data={data}>
					<XAxis dataKey='name'/>
					<YAxis/>
					<CartesianGrid strokeDasharray='3 5'/>
					{chartItems}
				</LineChart >}
			</div>
		</Overlay>
		
		this.setState({
			currentOverlay:overlay
		})
	}
	closeChart = (e) => {
		e.stopPropagation()
		
		this.setState({
			currentOverlay: null
		})
	}
	startTimer = (e) => {
		e.stopPropagation()
		e.preventDefault()
		
		this.timer = new Date()
	}
	killColumnTimer = (e) => {
		e.stopPropagation()
		e.preventDefault()
		
		var endDate = new Date()
		var seconds = (endDate.getTime() - this.timer.getTime()) / 1000
		
		if (seconds > 0.5) {
			this.cloneLastColumn(e)
		}
		else {
			this.addColumn(e)
		}
	}
	addColumn = (e) => {
		if (e !== null && typeof e !== typeof undefined) {
			e.stopPropagation()
		}
		
		var headers = this.props.element.fields
		var newHeader = {
			header: translate('Header', this.props.editor.state.currentLang),
			items: [createNewElement('', this.props.editor.state.currentLang)]
		}
		headers.push(newHeader)
		
		this.addedNewElement()
	}
	cloneLastColumn = (e) => {
		e.stopPropagation()
		
		if (this.props.editor.state.editModeEnabled) {
			var headers = this.props.element.fields
			var lastColumn = headers[headers.length - 1]
			var clone = JSON.parse(JSON.stringify(lastColumn))
			clone.items.forEach(function(item) {
				item.fieldID = ''
				item.value = ''
			})
			
			headers.push(clone)
			this.addedNewElement()
		}
	}
	cloneLastRow = () => {
		var headers = this.props.element.fields
		
		if (this.props.editor.state.editModeEnabled === false) {
			var adu = parseInt(this.props.element.addedByUser, 10)
			adu++
			this.props.element.addedByUser = adu
		}
		
		headers.forEach(function(header) {
			var items = header.items
			var lastItem = items[items.length - 1]
			var clone = JSON.parse(JSON.stringify(lastItem))
			
			if (clone.type === 'calc') {
				var idClone = clone.fieldID
				var idParts = idClone.split('_')
				var idIndex = (idParts[1] + '').replace(/\D/g,'')
				
				var newFormula = clone.formula
				var regex = new RegExp('\\w'+idIndex+'\\]','g')
				var matches = newFormula.match(regex)
				
				if (matches !== null && typeof matches !== typeof undefined && matches.length > 0) {
					matches.forEach(match => {
						var letter = match.substring(0,1)
						var replacement = letter + (parseInt(idIndex, 10) + 1) + ']'
						newFormula = newFormula.replace(match, replacement)
					})
					
					clone.formula = newFormula
				}
			}
			
			clone.fieldID = ''
			clone.value = ''
			items.push(clone)
		})
		
		this.addedNewElement()
	}
	killRowTimer = (e) => {
		e.stopPropagation()
		e.preventDefault()
		
		var endDate = new Date()
		var seconds = (endDate.getTime() - this.timer.getTime()) / 1000
		
		if (seconds > 0.5) {
			this.cloneLastRow()
		}
		else {
			this.addRow(e)
		}
	}
	addRow = (e) => {
		e.stopPropagation()
		
		var headers = this.props.element.fields
		for (var headerIndex=0;headerIndex<headers.length;headerIndex++) {
			var header = headers[headerIndex]
			var items = header.items
			items.push(createNewElement('', this.props.editor.state.currentLang))
		}
		
		this.addedNewElement()
	}
	removeLastColumn = () => {
		var currentOrientation = this.props.element.orientation
		
		if (this.props.editor.state.editModeEnabled === false) {
			var adu = parseInt(this.props.element.addedByUser, 10)
			if (adu > 0) {
				adu--
				this.props.element.addedByUser = adu
			}
		}
		
		var headers = this.props.element.fields
		if (currentOrientation === 'horizontal') {
			headers.pop()
			
			if (headers.length === 0) {
				var newHeader = {
					header: translate('Header', this.props.editor.state.currentLang),
					items: [createNewElement('', this.props.editor.state.currentLang)]
				}
				headers.push(newHeader)
			}
			
			this.props.element.fields = headers
		}
		else {
			headers.forEach(function(header) {
				var items = header.items
				items.pop()
				header.items = items
			})
		}
		
		this.addedNewElement()
	}
	removeLastRow = () => {
		var currentOrientation = this.props.element.orientation
		
		if (this.props.editor.state.editModeEnabled === false) {
			var adu = parseInt(this.props.element.addedByUser, 10)
			if (adu > 0) {
				adu--
				this.props.element.addedByUser = adu
			}
		}
		
		var headers = this.props.element.fields
		if (currentOrientation === 'horizontal') {
			headers.forEach(function(header) {
				var items = header.items
				items.pop()
				header.items = items
			})
		}
		else {
			headers.pop()
			
			if (headers.length === 0) {
				var newHeader = {
					header: translate('Header', this.props.editor.state.currentLang),
					items: [createNewElement('', this.props.editor.state.currentLang)]
				}
				headers.push(newHeader)
			}
			
			this.props.element.fields = headers
		}
		
		this.addedNewElement()
	}
	addedNewElement = () => {
		var updatedForm = this.props.editor.state.form
		this.props.editor.addVersion(updatedForm, false)
	}
	deleteTableColumn = (index) => {
		var headers = this.props.element.fields
		var remainingHeaders = removeAt(headers, index)
		
		if (remainingHeaders.length === 0) {
			var newHeader = {
			header: translate('Header', this.props.editor.state.currentLang),
				items: [createNewElement('', this.props.editor.state.currentLang)]
			}
			remainingHeaders.push(newHeader)
		}
		
		this.props.element.fields = remainingHeaders
		this.addedNewElement()
	}
	deleteTableItem = (row, column) => {
		if (this.props.element.orientation === 'vertical') {
			var temp = row
			row = column
			column = temp
		}
		
		var headers = this.props.element.fields
		var header = headers[column]
		var items = header.items
		items[row] = createNewElement('', this.props.editor.state.currentLang)
		
		this.addedNewElement()
	}
	populateHorizontal = (array, container, allowEmptyContainer = true, isAddedByUser = false) => {
		var maxRows = getTableElementMaxElements(array)
		
		if (this.props.editor.state.editModeEnabled) {
			maxRows -= parseInt(this.props.element.addedByUser, 10)
		}
		
		for (var cr=0;cr<maxRows;cr++) {
			var ctds = []
			
			for (var ci=0;ci<array.length;ci++) {
				var column = array[ci]
				var items = column.items
				var item = items[cr]
				
				if (typeof item !== typeof undefined && item.type.length !== 0) {
					if (item.type.length > 0 && item.type === 'separator') {
						var separatorTD = (<td key={cr+'_'+ci+'_'+isAddedByUser}>
							<Separator column={ci} row={cr} deleteElement={this.deleteTableItem} editor={this.props.editor} element={item} index={0} isInsideTable={true} padding={0}/>
						</td>)
						ctds.push(separatorTD)
					}
					else {
						var elementTD = (<td className='palette-tableTD' key={cr+'_'+ci+'_'+isAddedByUser}>
							<Element column={ci} row={cr} deleteElement={this.deleteTableItem} editor={this.props.editor} element={item} index={0} order={''} padding={'0px'} usesMinimalUI={true} isInsideTable={true}/>
						</td>)
						ctds.push(elementTD)
					}
				}
				else if (allowEmptyContainer) {
					var placeHolderTD = (<td key={cr+'_'+ci+'_'+isAddedByUser} style={{backgroundColor:'#E4F1FE',width:placeHolderWidth,height:placeHolderHeight}}>
						<ElementItemsTableDroppable addedNewElement={this.addedNewElement} column={ci} row={cr} form={this.props.editor.state.form} lang={this.props.editor.state.currentLang} tableElement={this.props.element}/>
					</td>)
					
					ctds.push(placeHolderTD)
				}
			}
			
			container.push(<tr className='palette-tableTR' key={'row_'+cr+'_'+isAddedByUser}>{ctds}</tr>)
		}
	}
	generateContent = () => {
		var rows = []
		var orientation = this.props.element.orientation
		var headers = this.props.element.fields
		var maxElements = getTableElementMaxElements(this.props.element.fields)
		var lang = this.props.editor.state.currentLang
		
		if (orientation === 'horizontal') {
			var headersTD = headers.map((header, index) => {
				return <TableHeader deleteHeader={this.deleteTableColumn} editable={this.props.editor.state.editModeEnabled} header={header.header} key={'header_'+index} lang={lang} row={index}/>
			})
			
			rows.push(<tr key='header'>{headersTD}</tr>)
			
			if (maxElements > 0) {
				this.populateHorizontal(headers, rows, true)
			}
			else {
				var emptyTDs = []
				for (var emptyColumn=0;emptyColumn<headers.length;emptyColumn++) {
					var emptyContentTD = (<td className='palette-tableEmptyTD' key={0+'_'+emptyColumn} style={{width:placeHolderWidth,height:placeHolderHeight}}>
						<ElementItemsTableDroppable addedNewElement={this.addedNewElement} column={emptyColumn} row={0} form={this.props.editor.state.form} lang={this.props.editor.state.currentLang} tableElement={this.props.element}/>
					</td>)
					
					emptyTDs.push(emptyContentTD)
				}
				
				rows.push(<tr key={'row_0'}>{emptyTDs}</tr>)
			}
		}
		else if (orientation === 'vertical') {
			if (maxElements > 0) {
				for (var rowIndex=0;rowIndex<headers.length;rowIndex++) {
					var header = headers[rowIndex]
					var headerItems = header.items
					var rowTDs = []
					
					rowTDs.push(<TableHeader deleteHeader={this.deleteTableColumn} editable={this.props.editor.state.editModeEnabled} header={header.header} key={'header_'+rowIndex} lang={lang} row={(rowIndex-1)}/>)
					
					for (var elementIndex=0;elementIndex<maxElements;elementIndex++) {
						var rowItem = headerItems[elementIndex]
						
						if (typeof rowItem !== typeof undefined && rowItem.type.length !== 0) {
							if (rowItem.type.length > 0 && rowItem.type === 'separator') {
								var rowSeparatorTD = (<td key={rowIndex+'_'+elementIndex} style={{}}>
									<Separator column={elementIndex} row={rowIndex} deleteElement={this.deleteTableItem} editor={this.props.editor} element={rowItem} index={0} isInsideTable={true} padding={0}/>
								</td>)
								rowTDs.push(rowSeparatorTD)
							}
							else {
								var rowTD = (<td className='palette-tableTD' key={rowIndex+'_'+elementIndex} style={{}}>
									<Element column={elementIndex} row={rowIndex} deleteElement={this.deleteTableItem} editor={this.props.editor} element={rowItem} index={0} order={''} padding={'0px'} usesMinimalUI={true} isInsideTable={true}/>
								</td>)
								rowTDs.push(rowTD)
							}
						}
						else {
							var rowPlaceHolderTD = (<td className='palette-tableEmptyTD' key={rowIndex+'_'+elementIndex} style={{width:placeHolderWidth,height:placeHolderHeight}}>
								<ElementItemsTableDroppable addedNewElement={this.addedNewElement} column={rowIndex} row={elementIndex} form={this.props.editor.state.form} lang={this.props.editor.state.currentLang} tableElement={this.props.element}/>
							</td>)
							
							rowTDs.push(rowPlaceHolderTD)
						}
					}
					
					rows.push(<tr className='palette-tableTR' key={'row_'+rowIndex}>{rowTDs}</tr>)
				}
			}
			else {
				var emptyHeader = headers[0]
				if (typeof emptyHeader === typeof undefined) {
					return
				}
				
				var emptyRowTD = []
				var emptyHeaderTD = (<td key={'header_'+0} style={{backgroundColor:'#E4F1FE',fontWeight:'bold',margin:0,padding:0}}>{decodeHtml(emptyHeader.header)}</td>)
				emptyRowTD.push(emptyHeaderTD)
				
				var emptyRowContent = (<td key={'0_0'} style={{backgroundColor:'#E4F1FE',width:placeHolderWidth,height:placeHolderHeight}}>
					<ElementItemsTableDroppable addedNewElement={this.addedNewElement} column={0} row={0} form={this.props.editor.state.form} lang={this.props.editor.state.currentLang} tableElement={this.props.element}/>
				</td>)
					
				emptyRowTD.push(emptyRowContent)
				rows.push(<tr key={'row_0'}>{emptyRowTD}</tr>)
			}
		}
		
		return rows
	}
	render() {
		var className = ''
		var tableStyle = {
			borderCollapse:'collapse',
			borderRadius:'3px',
			overflow:'hidden'
		}
		
		var lan = this.props.editor.state.currentLang
		var eme = this.props.editor.state.editModeEnabled
		var adu = this.props.element.addedByUser
		var ori = this.props.element.orientation
		var ane = this.props.element.allowsNewElements
		
		if (eme) {
			this.fixTable()
		}
		
		return <div style={{alignItems:'flex-end', display:'flex', flexDirection:'row'}}>
			<div className={className} style={{alignItems:'flex-end', justifyContent:'center', display:'flex', marginLeft:10, minHeight:this.props.minHeight}}>
				{eme && <div style={{display:'inline-flex'}}>
					<Tooltip title={translate('Remove Column', lan)} arrowPointAtCenter={false}>
						<Icon onClick={this.removeLastColumn} type="arrow-left" style={{color:'#ff0000', cursor:'pointer', fontWeight:'bold'}}/>
					</Tooltip>
					<Tooltip title={translate('Remove Row', lan)} arrowPointAtCenter={false}>
						<Icon onClick={this.removeLastRow} type="arrow-up" style={{color:'#ff0000', cursor:'pointer', fontWeight:'bold', marginRight:'10px', marginTop:'2px'}}/>
					</Tooltip>
				</div>}
				
				{(eme === false && ori === 'horizontal' && adu > 0) &&
					<Tooltip title={translate('Remove Row', lan)} arrowPointAtCenter={false}>
						<Icon onClick={this.removeLastRow} type="arrow-up" style={{color:'#ff0000', cursor:'pointer', fontWeight:'bold', marginRight:'10px', marginTop:'2px'}}/>
					</Tooltip>}
					
				{(eme === false && ori === 'vertical' && adu > 0) &&
					<Tooltip title={translate('Remove Column', lan)} arrowPointAtCenter={false}>
						<Icon onClick={this.removeLastColumn} type="arrow-left" style={{color:'#ff0000', cursor:'pointer', fontWeight:'bold'}}/>
					</Tooltip>}
				
				<table style={tableStyle}>
					<caption className='palette-tableCaption' style={{justifyContent:'center', alignItems:'center', padding:'3px'}}>
						<label style={{flex:1, marginLeft:'5px',marginRight:'5px'}}>{decodeHtml(this.props.element.title)}</label>
						{(this.props.element.chart.length > 0) && <Icon onClick={this.openChart} style={{color:'red', cursor: 'pointer'}} type="area-chart" />}
					</caption>
					<tbody>
						{this.generateContent()}
					</tbody>
				</table>
				{eme && <Tooltip title={translate('Add Column', lan)} arrowPointAtCenter={false}>
					{ori === 'horizontal' && <Icon onMouseDown={this.startTimer} onMouseUp={this.killColumnTimer} type="arrow-right" style={{color:'#2c6ba8', cursor:'pointer', fontWeight:'bold'}}/>}
					{ori === 'vertical' && <Icon onMouseDown={this.startTimer} onMouseUp={this.killRowTimer} type="arrow-right" style={{color:'#2c6ba8', cursor:'pointer', fontWeight:'bold'}}/>}
				</Tooltip>}
				{(eme === false && ori === 'horizontal' && ane === true) && <Tooltip title={translate('Add Row', lan)} arrowPointAtCenter={false}>
					<Icon onClick={this.cloneLastRow} type="arrow-down" style={{color:'#2c6ba8', cursor:'pointer', fontWeight:'bold', marginRight:'10px', marginTop:'2px'}}/>
				</Tooltip>}
			</div>
			{eme && <Tooltip title={translate('Add Row', lan)} arrowPointAtCenter={false}>
				{ori === 'horizontal' && <Icon onMouseDown={this.startTimer} onMouseUp={this.killRowTimer} type="arrow-down" style={{color:'#2c6ba8', cursor:'pointer', fontWeight:'bold', marginRight:'10px', marginTop:'2px'}}/>}
				{ori === 'vertical' && <Icon onMouseDown={this.startTimer} onMouseUp={this.killColumnTimer} type="arrow-down" style={{color:'#2c6ba8', cursor:'pointer', fontWeight:'bold', marginRight:'10px', marginTop:'2px'}}/>}
			</Tooltip>}
			{(eme === false && ori === 'vertical' && ane === true) && <Tooltip title={translate('Add Row', lan)} arrowPointAtCenter={false}>
				<Icon onClick={this.cloneLastRow} type="arrow-right" style={{color:'#2c6ba8', cursor:'pointer', fontWeight:'bold'}}/>
			</Tooltip>}
			{this.state.currentOverlay}
		</div>
	}
}

ElementItemsTable.propTypes = {
	editor: PropTypes.object.isRequired,
	element: PropTypes.object.isRequired
}