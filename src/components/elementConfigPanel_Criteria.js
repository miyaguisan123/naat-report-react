import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Checkbox from 'antd/lib/checkbox'
import Form from 'antd/lib/form'
import Icon from 'antd/lib/icon'
import Input from 'antd/lib/input'

const FormItem = Form.Item

export default class Criteria extends Component {
	deleteCriteria = (e) => {
		e.stopPropagation()
		this.props.deleteCriteria(this.props.index)
	}
	updateOptional = (e) => {
		e.stopPropagation()
		this.props.updateCriteria(this.props.index, e.target.checked, this.props.criteria.field, this.props.criteria.value, this.props.criteria.equality)
	}
	updateField = (e) => {
		e.stopPropagation()
		
		var newValue = e.target.value
		var replaced = newValue.replace(/[[\]']+/g,'')
		this.props.updateCriteria(this.props.index, this.props.criteria.optionals, replaced, this.props.criteria.value, this.props.criteria.equality)
	}
	updateValue = (e) => {
		e.stopPropagation()
		this.props.updateCriteria(this.props.index, this.props.criteria.optionals, this.props.criteria.field, e.target.value, this.props.criteria.equality)
	}
	updateEquality = (e) => {
		e.stopPropagation()
		this.props.updateCriteria(this.props.index, this.props.criteria.optionals, this.props.criteria.field, this.props.criteria.value, e.target.value)
	}
	render() {
		var isInvalid = (this.props.criteria.field.length === 0 || this.props.criteria.value.length === 0)
		
		return <FormItem style={{display:'flex', alignItems:'center', marginBottom:'5px'}} validateStatus={isInvalid ? 'error':''}>
			<Checkbox checked={this.props.criteria.optional} style={{display:'inline-flex'}} onChange={this.updateOptional}/>
			<Input value={'['+this.props.criteria.field+']'} style={{color:'#2c6ba8', display:'inline-flex', flex:'none', fontStyle:'italic', width:'130px'}} onChange={this.updateField}/>
			&nbsp;&nbsp;
			<select defaultValue={this.props.criteria.equality} onChange={this.updateEquality} style={{border:'1px solid #d9d9d9', borderRadius:'4px', color:'#2c6ba8', flex:'none', height:'28px', width:'40px'}}>
				<option value='==='>=</option>
				<option value='>'>&#62;</option>
				<option value='<'>&#60;</option>
				<option value='>='>&#62;=</option>
				<option value='<='>&#60;=</option>
				<option value='!=='>!=</option>
			</select>
			&nbsp;&nbsp;
			<Input value={this.props.criteria.value} style={{color:'#2c6ba8', display:'inline-flex', flex:1, fontStyle:'italic'}} onChange={this.updateValue}/>
			&nbsp;&nbsp;
			<Icon type='minus-circle' onClick={this.deleteCriteria} style={{color:'#2c6ba8', cursor:'pointer'}}/>
		</FormItem>
	}
}

Criteria.propTypes = {
	criteria: PropTypes.object.isRequired,
	deleteCriteria: PropTypes.func.isRequired,
	lang: PropTypes.string.isRequired,
	index: PropTypes.number.isRequired,
	updateCriteria: PropTypes.func.isRequired
}