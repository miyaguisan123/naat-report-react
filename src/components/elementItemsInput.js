import React, { Component } from 'react'
import Autosize from 'react-input-autosize'
import PropTypes from 'prop-types'

import {decodeHtml} from '../functions/functions'
	
export default class ElementItemsInput extends Component {
	constructor(props) {
		super(props)
		var eme = this.props.editor.state.editModeEnabled
		
		this.state = {
			defaultValue: ((eme === false) ? this.props.element.value : '')
		}
	}
	update = (event) => {
		if (this.props.element.lock)
		
		event.stopPropagation()
		
		var newValue = event.target.value
		this.setState({
			defaultValue: newValue
		})
  }
	saveValue = (event) => {
		event.stopPropagation()
		var eme = this.props.editor.state.editModeEnabled
		
		if (eme === false) {
			var newValue = this.state.defaultValue
			this.props.element.value = newValue
			var updatedForm = this.props.editor.state.form
			
			this.props.editor.addVersion(updatedForm, true)
		}
	}
	render() {
		var className = ''
		var value = this.state.defaultValue
		var eme = this.props.editor.state.editModeEnabled
		var blocked = false
		
		if (eme === false) {
			blocked = (this.props.element.lock === true) || (this.props.editor.state.form.status === 2)
			var maxLength = parseInt(this.props.element.maxLength, 10)
			
			if (maxLength > 0 && value.length > maxLength) {
				className = 'error'
			}
			
			if (this.props.element.expression.length > 0 && value.length > 0) {
				try {
					var regex = new RegExp(this.props.element.expression)
					if (regex.test(value) === false) {
						className = 'error'
					}
				}
				catch(e) {
					console.log('invalid regex')
				}
			}
		}
		
		return <div className={className} style={{alignItems:'center', justifyContent:'center', display:'inline-flex', marginLeft:10, minHeight:this.props.minHeight}}>
			<Autosize className="autosize-input flex-row" defaultValue={decodeHtml(value)} readOnly={blocked} onChange={this.update} onBlur={this.saveValue}/>
		</div>
	}
}

ElementItemsInput.propTypes = {
	editor: PropTypes.object.isRequired,
	element: PropTypes.object.isRequired
}