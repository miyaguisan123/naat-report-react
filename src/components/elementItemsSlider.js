import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Slider from 'antd/lib/slider'
	
export default class ElementItemsSlider extends Component {
	constructor(props) {
		super(props)
		var eme = this.props.editor.state.editModeEnabled
		
		this.state = {
			defaultValue: ((eme === false) ? parseFloat(this.props.element.value) : 0.0)
		}
	}
	saveValue = (newValue) => {
		var eme = this.props.editor.state.editModeEnabled
		
		if (eme === false) {
			this.props.element.value = newValue
			var updatedForm = this.props.editor.state.form
			
			this.props.editor.addVersion(updatedForm, true)
		}
	}
	render() {
		var value = this.state.defaultValue
		var eme = this.props.editor.state.editModeEnabled
		var blocked = false
		
		if (isNaN(value)) {
			value = 0.0
		}
		
		if (eme === false) {
			blocked = (this.props.element.lock === true) || (this.props.editor.state.form.status === 2)
		}
		
		var min = parseFloat(this.props.element.minimum)
		var max = parseFloat(this.props.element.maximum)
		var step = parseFloat(this.props.element.step)
		
		return <div style={{alignItems:'center', justifyContent:'center', display:'inline-flex', marginLeft:10, minHeight:this.props.minHeight}}>
			<label style={{color:'#cccccc',marginRight:5}}>{min}</label>
			<Slider disabled={blocked} min={min} max={max} defaultValue={value} step={step} onAfterChange={this.saveValue}/>
			<label style={{color:'#cccccc',marginLeft:5}}>{max}</label>
		</div>
	}
}

ElementItemsSlider.propTypes = {
	editor: PropTypes.object.isRequired,
	element: PropTypes.object.isRequired
}