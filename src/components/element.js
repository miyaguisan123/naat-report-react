import axios from 'axios'
import React,{ Component } from 'react'
import FontAwesome from 'react-fontawesome'
import PropTypes from 'prop-types'

import ElementItemsChecklist from './elementItemsChecklist'
import ElementItemsDataSource from './elementItemsDataSource'
import DataSourcePanel from './elementItemsDataSource_Panel'
import ElementItemsDate from './elementItemsDate'
import ElementItemsDropdown from './elementItemsDropdown'
import ElementItemsExternalImage from './elementItemsExternalImage'
import ElementItemsFile from './elementItemsFile'
import ElementItemsFunction from './elementItemsFunction'
import ElementItemsInput from './elementItemsInput'
import ElementItemsNumber from './elementItemsNumber'
import ElementItemsRadio from './elementItemsRadio'
import ElementItemsSlider from './elementItemsSlider'
import ElementItemsTable from './elementItemsTable'
import ElementItemsTextarea from './elementItemsTextarea'
import ElementItemsTime from './elementItemsTime'

import ElementActionsBar from './elementActionsBar'
import ElementClonePanel from './elementClonePanel'
import ElementConfigPanel from './elementConfigPanel'
import ElementNotesPanel from './elementNotesPanel'
import ElementContainer from './elementContainer'
import Overlay from './overlay'

import {decodeHtml, notesWSURL} from '../functions/functions'
import {elementFulfillsCriteria} from '../functions/criteriaValidation'
import {deleteContentRelatedTo} from '../functions/elementFunctions'

const minHeight = '35px'

export default class Element extends Component {
	constructor(props) {
		super(props)
		
		this.state = {
			clonePanelIsOpen: false,
			configPanelIsOpen: false,
			dbPanelOpen: false,
			notesPanelOpen: false,
			notes: []
		}
	}
	componentDidMount = () => {
		var idElement = this.props.element.fieldID
		var config = {
			method: 'get',
			headers: {
				'Accept':'application/json, text/javascript, */*; q=0.01'
			},
			json: true,
			params: {
				token: this.props.editor.props.token,
				idElement: idElement,
				idForm: this.props.editor.props.idForm
			},
			url: notesWSURL
		}
		
		axios.request(config).then(response => {
			var jsonResponse = response.request.response
			var currentNotes = JSON.parse(jsonResponse)
			
			if (typeof currentNotes !== typeof undefined && currentNotes !== null) {
				this.setState({
					notes: currentNotes
				})
			}
			else {
				//error response
			}
		}).catch(error => {
			//error loading
		})
	}
	closeNotesPanel = () => {
		this.setState({
			notesPanelOpen: false
		})
	}
	closeClonePanel = () => {
		this.setState({
			clonePanelIsOpen: false
		})
	}
	closeConfigPanel = () => {
		this.setState({
			configPanelIsOpen: false
		})
	}
	saveConfig = () => {
		var updatedForm = this.props.editor.state.form
		this.props.editor.addVersion(updatedForm, false)
	}
	closeDBPanel = () => {
		this.setState({
			dbPanelOpen: false
		})
	}
	deleteElement = () => {
		deleteContentRelatedTo(this.props.editor.state.form, this.props.element.fieldID)
		
		if (typeof this.props.row !== typeof undefined && typeof this.props.column !== typeof undefined) {
			this.props.deleteElement(this.props.row, this.props.column)
		}
		else {
			this.props.deleteElement(this.props.index)
		}
	}
	openNotesPanel = () => {
		var eme = this.props.editor.state.editModeEnabled
		if (eme) {
			return
		}
		
		this.setState({
			notesPanelOpen: true
		})
	}
	openClonePanel = () => {
		this.setState({
			clonePanelIsOpen: true
		})
	}
	openConfigPanel = () => {
		this.setState({
			configPanelIsOpen: true
		})
	}
	openDBPanel = () => {
		var eme = this.props.editor.state.editModeEnabled
		if (eme) {
			return
		}
		
		this.setState({
			dbPanelOpen: true
		})
	}
  render() {
		var editor = this.props.editor
		var form = editor.state.form
		var eme = editor.state.editModeEnabled
		var lang = editor.state.currentLang
		var borderStyle = 'none'
		var visible = this.props.element.visible
		var padding = (this.props.padding + 'px')
		
		if (eme === true) {
			if (this.state.configPanelIsOpen) {
				borderStyle = '2px solid #344146'
			}
		}
		else {
			var criteriaMet = elementFulfillsCriteria(this.props.editor.state.form, this.props.element)
			if (criteriaMet === true) {
				visible = !visible 
			}
			
			if (visible === false) {
				return null
			}
		}
		
		var style = {
			border: this.props.isInsideTable ? '1px solid #eeeeee' : '',
			display: 'block',
			width: '100%'
		}
		
		var flagColor = '#CCCCCC'
		var discrepancies = 0
		var notes = 0
		var bypass = 0
		
		this.state.notes.forEach(note => {
			if (note.status !== 2) {
				switch(note.type) {
					case 1:
						discrepancies++;
					break;
					case 2:
						notes++;
					break;
					case 5:
						bypass++;
					break;
					default:break;
				}
			}
		})
		
		if (discrepancies > 0 || notes > 0 || bypass > 0) {
			if (discrepancies > notes && discrepancies > bypass) {
				flagColor = '#ff4500'
			}
			else if (notes > discrepancies && notes > bypass) {
				flagColor = '#00008b'
			}
			else {
				flagColor = '#643687'
			}
		}
		
		return <div className='element_container' style={style}>
			<div style={{borderBottom:this.props.isInsideTable ? 'none':'1px solid #eeeeee',display:'flex-block',minHeight:minHeight,paddingLeft:padding}} data-id={this.props.element.fieldID}>
				<div style={{border:borderStyle,backgroundColor:'#ffffff',display:'flex',flexFlow:'column',minHeight:minHeight,opacity:visible ? '1.0':'0.25'}}>
					<div style={{width:'100%', display:'flex', flexDirection:'row'}}>
						<div style={{borderBottom:this.props.isInsideTable ? 'none':'1px solid #eeeeee', alignItems:'flex-start',backgroundColor:'#ffffff',display:'inline-flex',flex:2,justifyContent:'flex-start',minHeight:minHeight,paddingLeft:5}}>
							{(eme && this.props.usesMinimalUI === false) && <FontAwesome className='handle' name='ellipsis-v' style={{alignItems:'center',cursor:'grab',display:'inline-flex',justifyContent:'center',height:minHeight,textAlign:'center',width:20}}/>}
							{(this.props.usesMinimalUI === false) && <span className='palette-numericOrder' style={{alignItems:'center',display:'inline-flex',fontSize:this.props.isInsideTable ? '80%':'100%',fontWeight:'bold',height:minHeight,justifyContent:'left',marginLeft:'3px'}}>{this.props.order}</span>}
							{(this.props.usesMinimalUI === false) && <div style={{display:'inline-flex', alignItems:'center', justifyContent:'center', height:minHeight,marginLeft:'3px'}}>
								<img src={'./img/blue_tool_'+this.props.element.type+'.png'} style={{height:'20px'}} alt='icon'/>
							</div>}
							{this.props.element.required && <FontAwesome name='circle' style={{marginLeft:'5px',display:'inline-block',fontSize:'4pt',color:'#2c6ba8',lineHeight:'35px'}}/>}
							{this.props.element.leftText && ((this.props.usesMinimalUI === false) && <div className='palette-leftText' style={{alignItems:'center',display:'inline-flex',minHeight:minHeight,marginLeft:'5px'}}>{decodeHtml(this.props.element.leftText)}</div>)}
							
							{this.props.element.type === 'calc' && <ElementItemsFunction editor={editor} element={this.props.element} minHeight={minHeight}/>}
							{this.props.element.type === 'checkbox' && <ElementItemsChecklist editor={editor} element={this.props.element} minHeight={minHeight}/>}
							{this.props.element.type === 'dataSource' && <ElementItemsDataSource editor={this.props.editor} element={this.props.element} minHeight={minHeight} openDataBasePanel={this.openDBPanel}/>}
							{this.props.element.type === 'date' && <ElementItemsDate editor={this.props.editor} element={this.props.element} minHeight={minHeight}/>}
							{this.props.element.type === 'dropdown' && <ElementItemsDropdown editor={this.props.editor} element={this.props.element} minHeight={minHeight}/>}
							{this.props.element.type === 'external_file' && <ElementItemsExternalImage editor={this.props.editor} element={this.props.element} minHeight={minHeight}/>}
							{this.props.element.type === 'firm' && <ElementItemsFile editor={this.props.editor} element={this.props.element} minHeight={minHeight} lang={this.props.editor.state.currentLang} isDigitalFirm={true}/>}
							{this.props.element.type === 'file' && <ElementItemsFile editor={this.props.editor} element={this.props.element} minHeight={minHeight} lang={this.props.editor.state.currentLang} isDigitalFirm={false}/>}
							{this.props.element.type === 'number' && <ElementItemsNumber editor={editor} element={this.props.element} minHeight={minHeight}/>}
							{this.props.element.type === 'radio' && <ElementItemsRadio editor={this.props.editor} element={this.props.element} minHeight={minHeight}/>}
							{this.props.element.type === 'slider' && <ElementItemsSlider editor={this.props.editor} element={this.props.element} minHeight={minHeight}/>}
							{this.props.element.type === 'text' && <ElementItemsInput editor={editor} element={this.props.element} minHeight={minHeight}/>}
							{this.props.element.type === 'textarea' && <ElementItemsTextarea editor={editor} element={this.props.element} minHeight={minHeight} isInsideTable={this.props.isInsideTable}/>}
							{this.props.element.type === 'time' && <ElementItemsTime editor={this.props.editor} element={this.props.element} minHeight={minHeight}/>}
							
							{this.props.element.rightText && ((this.props.usesMinimalUI === false) && <div className='palette-rightText' style={{alignItems:'center',display:'inline-flex',fontStyle:'italic',minHeight:minHeight,marginLeft:'10px'}}>{decodeHtml(this.props.element.rightText)}</div>)}
							<FontAwesome name='flag' style={{cursor:'pointer',alignItems:'center',color:flagColor,display:'inline-flex',fontSize:'80%',height:minHeight,marginLeft:'5px'}} onClick={this.openNotesPanel}/>
							<div className='palette-idElement' style={{alignItems:'center',display:'inline-flex',fontWeight:'bold',fontSize:'80%',minHeight:minHeight,marginLeft:'10px',marginRight:'10px'}}>{this.props.element.fieldID}</div>
						</div>
						{eme && <ElementActionsBar allowsClone={!this.props.isInsideTable} isInsideTable={this.props.isInsideTable} lang={lang} requestClone={this.openClonePanel} requestConfig={this.openConfigPanel} requestDelete={this.deleteElement}/>}
					</div>
					{this.props.element.type === 'table' && <ElementItemsTable editor={editor} element={this.props.element} minHeight={minHeight}/>}
				</div>
				{(this.props.usesMinimalUI === false && this.props.isInsideTable === false) &&
					<ElementContainer editor={this.props.editor} isMainContainer={false} key={this.props.element.fieldID+'_container'} number={this.props.order} padding={8} parent={this.props.element}/>
				}
			</div>
			{this.state.clonePanelIsOpen && <Overlay draggable={true} centered={true} close={this.closeClonePanel}>
				<ElementClonePanel cloneElement={editor.cloneElement} element={this.props.element} form={form} close={this.closeClonePanel} lang={lang}/>
			</Overlay>}
			
			{this.state.configPanelIsOpen && <Overlay draggable={true} centered={true} close={this.closeConfigPanel} hackTextSelection={true}>
				<ElementConfigPanel editor={this.props.editor} saveConfig={this.saveConfig} close={this.closeConfigPanel} element={this.props.element} form={this.props.editor.state.form} lang={lang} style={{opacity:1.0}}/>
			</Overlay>}
			
			{(this.props.element.type === 'dataSource' && this.state.dbPanelOpen === true) && <Overlay draggable={true} centered={true} close={this.closeDBPanel}>
				<DataSourcePanel container={this} closePanel={this.closeDBPanel}/>
			</Overlay>}
			
			{this.state.notesPanelOpen && <Overlay draggable={false} dismissable={false} centered={true} close={this.closeNotesPanel}>
				<ElementNotesPanel editable={true} elementObject={this} idForm={editor.props.idForm} close={this.closeNotesPanel} lang={this.props.editor.state.currentLang} token={this.props.editor.props.token}/>
			</Overlay>}
		</div>
  }
}

Element.propTypes = {
	editor:PropTypes.object.isRequired,
	element:PropTypes.object.isRequired,
	isInsideTable:PropTypes.bool.isRequired,
	order:PropTypes.string.isRequired,
	usesMinimalUI:PropTypes.bool.isRequired
}