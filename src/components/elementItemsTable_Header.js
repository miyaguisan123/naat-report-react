import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Popconfirm from 'antd/lib/popconfirm'
import Tooltip from 'antd/lib/tooltip'

import {decodeHtml} from '../functions/functions'
import {translate} from '../functions/translations'

export default class TableHeader extends Component {
	deleteHeader = (e) => {
		this.props.deleteHeader(this.props.row)
	}
	render() {
		var lang = this.props.lang
		
		if (this.props.editable === true) {
			return <Popconfirm title={translate("_system-delete-column", lang)} onConfirm={this.deleteHeader} okText={translate("Yes", lang)} cancelText="No">
				<Tooltip placement="bottom" title={translate("Delete", lang)} arrowPointAtCenter={false}>
					<td className='palette-tableHeader' style={{cursor:'pointer',fontWeight:'bold',margin:0,padding:0,textAlign:'center'}}>
						{decodeHtml(this.props.header)}
					</td>
				</Tooltip>
			</Popconfirm>
		}
		
		return <td className='palette-tableHeader' style={{cursor:'pointer',fontWeight:'bold',margin:0,padding:0,textAlign:'center'}}>
			{decodeHtml(this.props.header)}
		</td>
	}
}

TableHeader.propTypes = {
	deleteHeader: PropTypes.func.isRequired,
	editable: PropTypes.bool.isRequired,
	header: PropTypes.string.isRequired,
	lang: PropTypes.string.isRequired,
}