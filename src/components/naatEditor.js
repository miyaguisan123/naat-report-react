import axios from 'axios'
import React, {Component} from 'react'
import FontAwesome from 'react-fontawesome'
import ElementsContainer from './elementContainer'
import Overlay from './overlay'
import PageContainer from './pageContainer'
import ToolPanel from './toolPanel'
import Tooltip from 'antd/lib/tooltip'
import TopBar from './topBar'

import message from 'antd/lib/message'
import {translate} from '../functions/translations'

import {addFormulaFixIndicator, createNewElement, getUncompleteElements, getVisibleElements, loadForm, pdfWSURL, removeAt, scrollToBottom, saveFormChanges, scrollToTop, xmlWSURL} from '../functions/functions'
import {reOrderFormIds} from '../functions/reordering'

export default class NaatEditor extends Component {
	constructor(props) {
		super(props)
		var formVersions = []
		
		this.state = {
			currentOverlay: null,
			currentLang: 'esp',
			currentPage: 0,
			editModeEnabled: false,
			userIsAdmin: false,
			embeded: true,
			form: null,
			path: null
		}
	}
	componentDidMount() {
		loadForm(this)
	}
	save = (complete = false) => {
		saveFormChanges(this, complete)
	}
	completeForm = () => {
		var uncompleteElements = getUncompleteElements(this.state.form)
		if (uncompleteElements.length === 0) {
			this.save(true)
			var updatedForm = this.state.form
			updatedForm.status = 2
			
			this.setState({
				form: updatedForm
			})
		}
		else {
			message.error(translate('_system-error-uncomplete-form', this.state.currentLang), 5.0)
		}
	}
	print = (format = 'pdf') => {
		var editor = this
		saveFormChanges(editor, false, function() {
			console.log('document saved')
			
			if (format === 'xml') {
				console.log('format is XML')
			}
			else {
				var config = {
					method: 'post',
					headers: {
						'Accept':'application/json, text/javascript, */*; q=0.01'
					},
					json: true,
					data: {
						path: editor.state.path,
						visible: getVisibleElements(editor.state.form)
					},
					url: pdfWSURL
				}
				
				axios.request(config).then(response => {
					
				}).catch(error => {
					
				})
			}
		})
		
		/*
		var fileURL = '?zoom=100&idForm='+idForm+'&token='+this.props.token
		
		if (format === 'xml') {
			fileURL = xmlWSURL + fileURL
		}
		else {
			fileURL = pdfWSURL + fileURL
		}
		
		this.setState({
			currentOverlay: <Overlay dismissable={true} draggable={false} centered={true} close={this.closeCurrentPanel}>
				<div className='dropshadow' style={{backgroundColor:'#ffffff', border:'1px solid #cccccc', borderRadius:3, maxHeight:700, maxWidth:800, overflow:'hidden'}}>
					<embed src={fileURL} width='800' height='700' />
				</div>
			</Overlay>
		})*/
	}
	addVersion = (formVersion, overWriteLast) => {
		if (overWriteLast === true) {
			this.formVersions = removeAt(this.formVersions, this.formVersions.length - 1)
		}
		else if (this.formVersions.length > 100) {
			this.formVersions = removeAt(this.formVersions, 0)
		}
		
		formVersion = this.updatedForm(formVersion)
		var newVersion = JSON.stringify(formVersion)
		this.formVersions.push(newVersion)
		
		this.setState({
			currentOverlay: null,
			form: formVersion
		})
	}
	updatedForm = (fv) => {
		return reOrderFormIds(fv)
	}
	removeLastVersion = () => {
		if (this.formVersions.length > 1) {
			this.formVersions = removeAt(this.formVersions, this.formVersions.length - 1)
			var lastItem = this.formVersions[this.formVersions.length - 1]
			var form = JSON.parse(lastItem)
			var totalPages = (form.pages.length - 1)
			var currentPage = this.state.currentPage
			
			if (currentPage > totalPages) {
				currentPage = totalPages
			}
			
			this.setState({
				currentPage: currentPage,
				currentOverlay: null,
				form: form
			})
		}
		else {
			this.setState({
				currentOverlay: null
			})
		}
	}
	currentPage = () => {
		return this.state.form ? this.state.form.pages[this.state.currentPage] : null
	}
	addElement = (type) => {
		if (this.state.form === null) {
			return
		}
		
		var form = this.state.form
		var page = this.currentPage()
		var elements = page.elements
		
		var newElement = createNewElement(type, this.state.currentLang)
		elements.push(newElement)
		page.elements = elements
		
		this.addVersion(form, false)
	}
	cloneElement = (elementToClone, targetPages) => {
		var updatedForm = this.state.form
		var currentPages = updatedForm.pages
		var cloneJSON = JSON.stringify(elementToClone)
		
		if (elementToClone.type === 'table') {
			cloneJSON = addFormulaFixIndicator(cloneJSON, elementToClone.fieldID)
		}
		
		var clone = JSON.parse(cloneJSON)
		clone.fieldID = ''
		
		targetPages.forEach(pageIndex => {
			var page = currentPages[pageIndex]
			page.elements.push(clone)
		})
		
		updatedForm.pages = currentPages
		
		this.addVersion(updatedForm, false)
	}
	openPanel = (panel, dismissable, draggable, centered) => {
		var overlay = <Overlay dismissable={dismissable} draggable={draggable} centered={centered} close={this.closeCurrentPanel}>{panel}</Overlay>
		this.setState({
			currentOverlay:overlay
		})
	}
	closeCurrentPanel = (e) => {
		this.setState({
			currentOverlay: null
		})
	}
	goToTop = (e) => {
		if (typeof e !== typeof undefined) {
			e.stopPropagation()
			e.preventDefault()
		}
		
		scrollToTop('editor-container')
	}
	goToBottom = (e) => {
		if (typeof e !== typeof undefined) {
			e.stopPropagation()
			e.preventDefault()
		}
		
		scrollToBottom('editor-container')
	}
	render() {
		if (this.state.form === null) {
			return null;
		}
		
		var canvasStyle = {
			backgroundColor:'#ffffff',
			display:'flex',
			top:50,
			width:'100%',
			minWidth: '1366px',
			minHeight:'768px',
		}
		
		return <div>
			{(this.state.embeded === false) && <TopBar editor={this}/>}
			<div onKeyDown={this.reviewShortcut} style={canvasStyle}>
				{this.state.editModeEnabled && <ToolPanel addElement={this.addElement} page={this.currentPage()} editModeEnabled={this.state.editModeEnabled} lang={this.state.currentLang}/>}
				<div id='editor-container' style={{display:'flex', flex:2, flexDirection:'column', backgroundColor:'#ffffff', height:'100%', overflow:'auto'}}>
					{this.state.form && <PageContainer editor={this}/>}
					{this.state.form && <ElementsContainer editor={this} isMainContainer={true} key='main-item-container' number='' padding={0} parent={this.currentPage()}/>}
				</div>
			</div>
			{this.state.editModeEnabled && <div>
				<div className='dropshadow'  style={{backgroundColor:'#2c6ba8', borderRadius:'10px', cursor:'pointer', position:'fixed', top:5, right: 5, width:20, height:20}} onClick={this.goToTop}>
					<FontAwesome name='arrow-up' style={{alignItems:'center', backgroundColor:'#22A7F0', borderRadius:'20px',color:'#ffffff', display:'flex', justifyContent:'center', width:'100%', height:'100%', fontSize:'120%'}}/>
				</div>
				<div className='dropshadow'  style={{backgroundColor:'#2c6ba8', borderRadius:'10px', cursor:'pointer', position:'fixed', top:25, right: 5, width:20, height:20}} onClick={this.goToBottom}>
					<FontAwesome name='arrow-down' style={{alignItems:'center', backgroundColor:'#22A7F0', borderRadius:'20px',color:'#ffffff', display:'flex', justifyContent:'center', width:'100%', height:'100%', fontSize:'120%'}}/>
				</div>
			</div>}
			{(this.state.editModeEnabled === false) && <div>
				{this.state.form.status !== 2 && <div className='dropshadow'  style={{backgroundColor:'#8CBD00', borderRadius:'3px', cursor:'pointer', position:'fixed', top:270, right: 110, width:35, height:35}} onClick={this.save}>
					<Tooltip title={translate("Save", this.props.lang)} arrowPointAtCenter={false}>
						<FontAwesome name='save' className='palette-actionButton' style={{alignItems:'center', borderRadius:'3px',color:'#ffffff', display:'flex', justifyContent:'center', width:'100%', height:'100%', fontSize:'180%'}}/>
					</Tooltip>
				</div>}
				<div className='dropshadow'  style={{backgroundColor:'#8CBD00', borderRadius:'3px', cursor:'pointer', position:'fixed', top:270, right:55, width:35, height:35}} onClick={this.print}>
					<Tooltip title={translate("Print", this.props.lang)} arrowPointAtCenter={false}>
						<FontAwesome name='print' className='palette-actionButton' style={{alignItems:'center', borderRadius:'3px',color:'#ffffff', display:'flex', justifyContent:'center', width:'100%', height:'100%', fontSize:'180%'}}/>
					</Tooltip>
				</div>
				{this.state.form.status !== 2 && <div className='dropshadow'  style={{backgroundColor:'#8CBD00', borderRadius:'3px', cursor:'pointer', position:'fixed', top:270, right: 5, width:35, height:35}} onClick={this.completeForm}>
					<Tooltip title={translate("Complete", this.props.lang)} arrowPointAtCenter={false}>
						<FontAwesome name='check-square-o' className='palette-actionButton' style={{alignItems:'center', borderRadius:'3px',color:'#ffffff', display:'flex', justifyContent:'center', width:'100%', height:'100%', fontSize:'180%'}}/>
					</Tooltip>
				</div>}
			</div>}
			{this.state.currentOverlay}
		</div>
	}
}