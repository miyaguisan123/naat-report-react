import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Button from 'antd/lib/button'
import Table from 'antd/lib/table'
import {decodeHtml} from '../functions/functions'
import {translate} from '../functions/translations'

const ButtonGroup = Button.Group
var pages = []

export default class ElementClonePanel extends Component {
	constructor(props) {
		super(props)
		pages = []
		this.props.form.pages.forEach((page, index) => {
			pages.push({key:index, name: (index+1)+".- " + decodeHtml(page.name)})
		})
		
		this.state = {
			selected: [0]
		}
	}
	clone = (e) => {
		this.props.cloneElement(this.props.element, this.state.selected)
		this.props.close()
	}
	
	handleClick = (e) => {
		e.stopPropagation()
	}
	getDisabledItems = (record) => {
		return {disabled: false}
	}
	updateSelectedPages = (selectedRowKeys, selectedRows) => {
		this.setState({
			selected: selectedRowKeys
		})
	}
	render() {
		return <div className='dropshadow' onClick={this.handleClick} style={{backgroundColor:'#ffffff', borderRadius:3, display:'flex', flexDirection:'column', minWidth:400}}>
			<Table style={{padding:5}} rowSelection={{onChange: this.updateSelectedPages, getCheckboxProps: this.getDisabledItems}} columns={[{title: translate('Page', this.props.lang),dataIndex: 'name'}]} dataSource={pages} />
			<ButtonGroup style={{display:'flex', justifyContent:'flex-end', padding:5}}>
				<Button onClick={this.props.close}>Cancel</Button>
				<Button onClick={this.clone} type='primary'>OK</Button>
			</ButtonGroup>
		</div>
	}
}

ElementClonePanel.propTypes = {
	cloneElement: PropTypes.func.isRequired,
	close: PropTypes.func.isRequired,
	element: PropTypes.object.isRequired,
	form: PropTypes.object.isRequired,
	lang: PropTypes.string.isRequired
}