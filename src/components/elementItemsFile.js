import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Overlay from './overlay'

import Upload from 'antd/lib/upload'
import Button from 'antd/lib/button'
import Icon from 'antd/lib/icon'
import message from 'antd/lib/message'

import {base, filesWSURL} from '../functions/functions'
import {translate} from '../functions/translations'

export default class ElementItemsFile extends Component {
	constructor(props) {
		super(props)
		this.state = {
			currentValue: this.props.element.value,
			fileViewerOpen: false
		}
	}
	closeFileViewer = () => {
		this.setState({
			fileViewerOpen: false
		})
	}
	openFileViewer = (e) => {
		e.preventDefault()
		e.stopPropagation()
		
		this.setState({
			fileViewerOpen: true
		})
	}
	handleFileUpload = (info) => {
		if (info.file.status === 'done') {
			this.props.element.value = info.file.response.name
			this.setState({
				currentValue: info.file.response.name
			})
		}
		else if (info.file.status === 'error') {
			message.error(info.file.name);
		}
		else {
			//...uploading
		}
	}
	ignore = (e) => {
		e.preventDefault()
		e.stopPropagation()
	}
	render() {
		var action = filesWSURL
		var accepts = 'image/*, application/pdf'
		var file = this.state.currentValue
		var blocked = false
		
		var eme = this.props.editor.state.editModeEnabled
		if (eme === true) {
			action = ''
			file = ''
		}
		else {
			blocked = (this.props.element.lock === true) || (this.props.editor.state.form.status === 2)
		}
		
		if (this.props.isDigitalFirm === true) {
			accepts = '.nek'
		}
		
		if (typeof file === typeof undefined) {
			file = ''
		}
		
		var isImage = true;
		if (file.length > 0) {
			var parts = file.split('.')
			var extension = parts[parts.length - 1]
			
			if (extension === 'pdf') {
				isImage = false
			}
		}
		
		return <div style={{alignItems:'left', justifyContent:'center', display:'inline-flex', flexDirection:'column', marginLeft:10, minHeight:this.props.minHeight, marginTop:'5px'}}>
			{blocked === false && <Upload accept={accepts} multiple={false} name='form_file' showUploadList={false} action={action} onChange={this.handleFileUpload}>
				<Button>
					<Icon type="upload" />{translate('Upload file', this.props.lang)}
				</Button>
			</Upload>}
			{file.length > 0 && <a href='#viewFile' onClick={this.openFileViewer}>{file}</a>}
			{this.state.fileViewerOpen && <Overlay dismissable={true} draggable={isImage} centered={true} close={this.closeFileViewer}>
				<div onClick={this.ignore} className='dropshadow' style={{backgroundColor:'#ffffff', border:'1px solid #cccccc', borderRadius:3, maxHeight:800, maxWidth:800, overflow:'hidden', padding:2}}>
					{(isImage === true) && <img src={base+'uploads/'+file} alt={'Attachment: '+file} style={{maxWidth:400, maxHeight:400, display:'flex', alignItems:'center', justifyContent:'center', textAlign:'center'}}/>}
					{(isImage === false) && <embed src={base+'uploads/'+file} width='600px' height='800px' />}
				</div>
			</Overlay>}
		</div>
	}
}

ElementItemsFile.propTypes = {
	editor: PropTypes.object.isRequired,
	element: PropTypes.object.isRequired,
	isDigitalFirm: PropTypes.bool.isRequired
}