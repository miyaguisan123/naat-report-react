import React, {Component} from 'react'
import FontAwesome from 'react-fontawesome'
import PropTypes from 'prop-types'

import Popconfirm from 'antd/lib/popconfirm'
import Tooltip from 'antd/lib/tooltip'

import {translate} from '../functions/translations'

class ActionButton extends Component {
	render() {
		return <div className={this.props.highlightClass} style={{display: 'inline-block',textAlign: 'center',width: '30px'}}>
			<FontAwesome name={this.props.type}/>
		</div>
	}
}

export default class ElementActionsBar extends React.Component {
	render() {
		var style = {
			alignItems:'top',
			display:'inline-flex',
			flex:'none',
			listStyleType:'none',
			margin:'0px',
			padding:'0px',
			paddingTop:'10px'
		}
		
		if (this.props.isInsideTable === true) {
			style.transform = 'scale(0.85, 0.85)'
		}
		
		return <ul style={style}>
			<div style={{display:'flex'}}>
				<Tooltip placement="bottom" title={translate("Settings", this.props.lang)} arrowPointAtCenter={false}>
					<li onClick={this.props.requestConfig}>
						<ActionButton highlightClass='item-config action-button' type='cog'/>
					</li>
				</Tooltip>
				
				{this.props.allowsClone && <Tooltip placement="bottom" title={translate("Clone", this.props.lang)} arrowPointAtCenter={false}>
					<li onClick={this.props.requestClone}>
						<ActionButton highlightClass='item-clone action-button' type='clone'/>
					</li>
				</Tooltip>}
				
				<Popconfirm title={translate("_system-delete-element", this.props.lang)} onConfirm={this.props.requestDelete} okText={translate("Yes", this.props.lang)} cancelText="No">
					<Tooltip placement="bottom" title={translate("Delete", this.props.lang)} arrowPointAtCenter={false}>
						<li>
							<ActionButton highlightClass='item-delete action-button' type='times'/>
						</li>
					</Tooltip>
				</Popconfirm>
			</div>
		</ul>
	}
}

ElementActionsBar.propTypes = {
	lang: PropTypes.string.isRequired,
	requestClone: PropTypes.func.isRequired,
	requestConfig: PropTypes.func.isRequired,
	requestDelete: PropTypes.func.isRequired
}

ElementActionsBar.defaultProps = {
	allowsClone: true
}