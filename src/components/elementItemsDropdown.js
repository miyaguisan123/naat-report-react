import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {decodeHtml} from '../functions/functions'
	
export default class ElementItemsDropdown extends Component {
	saveValue = (event) => {
		event.stopPropagation()
		var eme = this.props.editor.state.editModeEnabled
		
		if (eme === false) {
			var newValue = event.target.value
			this.props.element.value = newValue
			var updatedForm = this.props.editor.state.form
			
			this.props.editor.addVersion(updatedForm, true)
		}
	}
	renderElements = () => {
		return this.props.element.options.map((option, index) => {
			return <option key={index} value={parseInt(option.value, 10)}>{decodeHtml(option.text)}</option>
		})
	}
	render() {
		var eme = this.props.editor.state.editModeEnabled
		var value = ''
		var blocked = false
		
		if (eme === false) {
			blocked = (this.props.element.lock === true) || (this.props.editor.state.form.status === 2)
			value = this.props.element.value
			if (value.length === 0) {
				value = this.props.element.default
			}
		}
		
		return <div style={{alignItems:'center', justifyContent:'center', display:'inline-flex', marginLeft:10, minHeight:this.props.minHeight}}>
			<select defaultValue={value} disabled={blocked} style={{borderRadius:3, border: '1px solid #cccccc', display:'inline-flex', minWidth:180, height:21}} onChange={this.saveValue}>
				{this.renderElements()}
			</select>
		</div>
	}
}

ElementItemsDropdown.propTypes = {
	editor: PropTypes.object.isRequired,
	element: PropTypes.object.isRequired
}