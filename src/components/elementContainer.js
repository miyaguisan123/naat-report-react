import uniqueId from 'lodash/uniqueId'
import React, { Component } from 'react'
import Sortable from 'react-sortablejs'
import PropTypes from 'prop-types'

import Element from './element'
import Separator from './separator'

import {createNewElement, getElementWith, moveArrayElement, removeAt} from '../functions/functions'

const toolPrefix = 'tool_'

export default class ElementContainer extends Component {
	updateForm = (overWrite) => {
		var updatedForm = this.props.editor.state.form
		this.props.editor.addVersion(updatedForm, overWrite)
	}
	deleteElement = (index) => {
		var parent = this.props.parent
		var updatedElements = removeAt(parent.elements, index)
		parent.elements = updatedElements
		
		this.updateForm(false)
	}
	itemAdded = (event) => {
		event.stopPropagation()
		var item = event.item
		var itemClass = event.item.getAttribute('class')
		if (itemClass === 'element_container') {
			item = item.children[0]
		}
		
		var fieldID = item.getAttribute('data-id')
		var element = null
		
		if (fieldID.startsWith(toolPrefix)) {
			var type = fieldID.substr(toolPrefix.length)
			
			element = createNewElement(type, this.props.editor.state.currentLang)
			element.fieldID = ''
		}
		else {
			var form = this.props.editor.state.form
			element = getElementWith(form, fieldID, 'idElement')
		}
		
		if (element != null) {
			var parent = this.props.parent
			parent.elements.splice(event.newIndex, 0, element)
			
			this.updateForm(true)
		}
	}
	itemRemoved = (event) => {
		event.stopPropagation()
		
		var indexToDelete = event.oldIndex
		this.deleteElement(indexToDelete)
	}
	itemsSorted = (event) => {
		event.stopPropagation()
		
		var parent = this.props.parent
		let oldIndex = event.oldIndex
		let newIndex = event.newIndex
		
		var newElements = moveArrayElement(parent.elements, oldIndex, newIndex)
		parent.elements = newElements
		
		this.updateForm(false)
	}
	renderElements = () => {
		var i = 0
		
		return this.props.parent.elements.map((element, index) => {
			if (element.type === 'separator') {
				return <Separator deleteElement={this.deleteElement} editor={this.props.editor} element={element} key={uniqueId()} index={index} isInsideTable={false} padding={this.props.padding}/>
			}
			
			i += 1
			return <Element deleteElement={this.deleteElement} editor={this.props.editor} element={element} key={uniqueId()} index={index} order={this.props.number + '' + i + '.'} padding={this.props.padding} usesMinimalUI={false} isInsideTable={false}/>
		})
	}
	onOrderUpdate = (order, sortableContainer, event) => {
		event.preventDefault()
		event.stopPropagation()
	}
	render() {
		var options = {
			animation:0,
			ghostClass:'ghost',
			group:{
				name:'element-container'
			},
			handle:'.handle',
			onAdd:this.itemAdded,
			onRemove:this.itemRemoved,
			onUpdate:this.itemsSorted
		}
		
		var containerStyle = {
			backgroundColor: 'rgba(52, 73, 94,0.05)',
			display: 'block',
			flex: this.props.isMainContainer ? 1:0,
			minHeight: 15,
			minWidth: 800
		}
		
		return <Sortable onChange={this.onOrderUpdate} options={options} style={containerStyle}>
			{this.renderElements()}
		</Sortable>
	}
}

ElementContainer.propTypes = {
	editor: PropTypes.object.isRequired,
	isMainContainer: PropTypes.bool.isRequired,
	padding: PropTypes.number.isRequired,
	parent: PropTypes.object.isRequired
}