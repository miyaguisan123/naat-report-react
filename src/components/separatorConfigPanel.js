import React, {Component} from 'react'
import Button from 'antd/lib/button'
import FontAwesome from 'react-fontawesome'
import {CompactPicker} from 'react-color'

import {decodeHtml} from '../functions/functions'
import {translate} from '../functions/translations'

const ButtonGroup = Button.Group

export default class SeparatorConfigPanel extends Component {
	constructor(props) {
		super(props)
		this.state = {
			separatorText:this.props.element.title,
			background:this.props.element.background
		}
	}
	componentDidMount(){
    this.textArea.focus();
  }
	handleClick = (e) => {
		e.stopPropagation()
	}
	updateBackground = (newColor) => {
		this.setState({
			background:newColor.hex
		})
	}
	updateText = (e) => {
		this.setState({
			separatorText:e.target.value
		})
	}
	saveChanges = (e) => {
		e.preventDefault()
		e.stopPropagation()
		
		this.props.element.title = this.state.separatorText
		this.props.element.background = this.state.background
		
		this.props.saveConfig()
	}
	render() {
		return <div className="dropshadow" onClick={this.handleClick} style={{backgroundColor:'#ffffff', fontSize:'90%', borderRadius:3, display:'flex', flexDirection:'column', minWidth:340, padding:10}}>
			<div style={{alignItems:'flex-start', display:'flex'}}>
				<span style={{color:'#2c6ba8',fontWeight:'bold',marginRight:10}}>{translate("Configuration", this.props.lang)}:</span>
				<span style={{flex:2}}>{translate('Separator', this.props.lang)}</span>
				<FontAwesome name='times' onClick={this.props.close} style={{alingItems:'center', color:'#2c6ba8', cursor:'pointer', display:'flex', justifyContent:'flex-end', height:25, width: 30}}/>
			</div>
			<div style={{alignItems:'center', display:'flex', flexDirection:'column'}}>
				<textarea ref={(textarea) => { this.textArea = textarea }} style={{display:'inline-flex', height:70,width:'100%'}} defaultValue={decodeHtml(this.state.separatorText)} onChange={this.updateText}/>
				<div style={{height:'10px',width:'100%'}}/>
				<CompactPicker color={this.state.background} onChangeComplete={this.updateBackground}/>
			</div>
			<ButtonGroup style={{display:'flex', justifyContent:'flex-end', padding:5}}>
				<Button onClick={this.props.close}>{translate('Cancel', this.props.lang)}</Button>
				<Button type='primary' onClick={this.saveChanges}>{translate('Save', this.props.lang)}</Button>
			</ButtonGroup>
		</div>
	}
}