import axios from 'axios'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Spinner from 'react-spinkit';

import FontAwesome from 'react-fontawesome'
import Tooltip from 'antd/lib/tooltip'

import {decodeHtml, notesWSURL} from '../functions/functions'
import {translate} from '../functions/translations'

const NOTE_STRING_TYPES = {1:'Discrepancy',2:'Note',5:'Bypass'}
const NOTE_STRING_STATUS = {0:'Open',1:'In Progress',2:'Closed'}
const NOTE_STRING_STATUS_COLORS = {0:'#CF000F',1:'#F89406',2:'#26A65B'}

export default class NoteEntry extends Component {
	constructor(props) {
		super(props)
		this.state = {
			loading: false,
			status: this.props.note.status
		}
	}
	setOpen = () => {
		this.setNewStatus(0)
	}
	setInProgress = () => {
		this.setNewStatus(1)
	}
	setClosed = () => {
		this.setNewStatus(2)
	}
	setNewStatus = (newStatus) => {
		var config = {
			method: 'post',
			headers: {
				'Accept':'application/json, text/javascript, */*; q=0.01'
			},
			json: true,
			data: {
				action: 'setNoteStatus',
				idNote: this.props.note.idNotes,
				idForm: this.props.note.idForm,
				status: newStatus,
				token: this.props.token
			},
			url: notesWSURL
		}
		
		axios.request(config).then(response => {
			var jsonResponse = response.request.response
			var newStatus = JSON.parse(jsonResponse)
			
			if (typeof newStatus !== typeof undefined && newStatus !== null && typeof newStatus.status !== typeof undefined) {
				this.props.note.status = newStatus.status
				this.setState({
					loading: false,
					status: newStatus.status
				})
			}
			else {
				this.setState({
					loading: false
				})
			}
		}).catch(error => {
			this.setState({
				loading: false
			})
		})
		
		this.setState({
			loading: true
		})
	}
	render() {
		var color = NOTE_STRING_STATUS_COLORS[this.props.note.status]
		var type = NOTE_STRING_TYPES[this.props.note.type]
		var status = NOTE_STRING_STATUS[this.state.status]
		var username = this.props.note.username
		var date = this.props.note.creationDate
		var summary = decodeHtml(this.props.note.summary)
		
		return <div style={{display:'flex',flexDirection:'column',maxWidth:'470px',marginBottom:'20px',alignItems:'left',justifyContent:'center'}}>
			<div style={{display:'flex',flexDirection:'row',fontSize:'10pt',marginBottom:'5px'}}>
				{this.state.loading === true && <Spinner name='circle' fadeIn='none' color='#aaaaaa' style={{height:'15px',marginRight:'5px',width:'15px'}}/>}
				<div style={{color:'#333333',fontWeight:'bold',marginRight:'3px'}}>
					{username},
				</div>
				<div style={{color:'#333333',marginRight:'10px'}}>
					{date}
				</div>
				<div style={{color:color,fontWeight:'bold'}}>
					({translate(type,this.props.lang)} - {translate(status,this.props.lang)})
				</div>
				{this.props.note.status === 2 && this.props.editable === true && <Tooltip placement='bottom' title={translate('Re-open', this.props.lang)} arrowPointAtCenter={false}>
					<FontAwesome onClick={this.setOpen} name='close' style={{cursor:'pointer',fontSize:'10pt',paddingLeft:'5px',paddingRight:'5px',color:'#cf000f',lineHeight:'20px'}}/>
				</Tooltip>}
			</div>
			<div className='dropshadow' style={{backgroundColor:'#ffffff',borderRadius:'3px',color:'#666666',fontSize:'11pt',padding:'5px',marginBottom:'5px'}}>
				{summary}
			</div>
			<div style={{display:'flex',flexDirection:'row'}}>
				{this.props.note.status !== 2 && this.props.editable === true && <div>
					<Tooltip placement='bottom' title={translate('Add Progress', this.props.lang)} arrowPointAtCenter={false}>
						<FontAwesome onClick={this.setInProgress} name='mail-reply' style={{cursor:'pointer',fontSize:'12pt',padding:'5px',color:'#0089cb'}}/>
					</Tooltip>
					<Tooltip placement='bottom' title={translate('Close', this.props.lang)} arrowPointAtCenter={false}>
						<FontAwesome onClick={this.setClosed} name='check-square-o' style={{cursor:'pointer',fontSize:'12pt',padding:'5px',color:'#26a65b'}}/>
					</Tooltip>
				</div>}
			</div>
		</div>
	}
}

NoteEntry.propTypes = {
	editable: PropTypes.bool.isRequired,
	note: PropTypes.object.isRequired,
	lang: PropTypes.string.isRequired,
}