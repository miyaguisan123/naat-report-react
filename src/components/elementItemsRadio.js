import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Radio from 'antd/lib/radio'
import {decodeHtml} from '../functions/functions'

const RadioGroup = Radio.Group

export default class ElementItemsRadio extends Component {
	constructor(props) {
		super(props)
		
		this.state = {
			defaultValue: ''
		}
	}
	saveValue = (event) => {
		event.stopPropagation()
		var eme = this.props.editor.state.editModeEnabled
		
		if (eme === false) {
			var newValue = event.target.value
			this.props.element.value = newValue
			var updatedForm = this.props.editor.state.form
			
			this.props.editor.addVersion(updatedForm, true)
		}
		else {
			this.setState({
				defaultValue: event.target.value
			})
		}
  }
	renderRadios = () => {
		return this.props.element.radios.map((radio,i) => {
			var value = radio.value + ''
			
			return <Radio key={i} value={value}>{decodeHtml(radio.text)}</Radio>
		})
	}
	render() {
		var targetClass = this.props.element.orientation === 'horizontal' ? 'row':'column'
		var value = this.state.defaultValue
		var eme = this.props.editor.state.editModeEnabled
		var blocked = false
		
		if (eme === false) {
			value = this.props.element.value
			blocked = (this.props.element.lock === true) || (this.props.editor.state.form.status === 2)
		}
		
		return <div style={{alignItems:'center', justifyContent:'center', display:'inline-flex', marginLeft:10, minHeight:this.props.minHeight}}>
			<RadioGroup disabled={blocked} onChange={this.saveValue} value={value} style={{display:'flex', flexDirection:targetClass}}>
				{this.renderRadios()}
			</RadioGroup>
		</div>
	}
}

ElementItemsRadio.propTypes = {
	editor: PropTypes.object.isRequired,
	element: PropTypes.object.isRequired
}