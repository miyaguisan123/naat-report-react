import React, {Component} from 'react'
import {translate} from '../functions/translations'

const aboutStyle = {
	backgroundColor: '#ffffff',
	alignItems: 'center',
	backgroundImage: 'url("./img/pattern.png")',
	backgroundRepeat: 'repeat',
	backgroundSize: '25%',
	borderRadius: 3,
	display:'flex',
	flexDirection: 'column',
	padding: 10,
	position:'static',
	width: 450
}

export default class AboutPanel extends Component {
	handleClick = (e) => {
		e.stopPropagation()
	}
	render() {
		return <div className='menu dropshadow' style={aboutStyle} onClick={this.handleClick}>
			<img src={'./img/logo_naateditor.png'} style={{display:'inline-block', verticalAlign:'middle', maxWidth:'250px', textAlign:'center'}} alt='Naat Editor Logo'/>
			<div style={{color:'#2c6ba8', display:'block', fontWeight:'bold', textAlign:'right', width:'190px', position:'absolute', top:125}}>v2.0.0</div>
			<a target='_blank' rel='noopener noreferrer' href='http://www.digitalizatxt.com'>
				<img src={'./img/logo_digitaliza.png'} style={{display:'inline-block', verticalAlign:'middle', maxWidth:'300px', padding:10, textAlign:'center'}} alt='Digitaliza Logo'/>
			</a>
			<div style={{color:'#2c6ba8', display:'block', padding:10, textAlign:'center', width:'350px'}}>{translate('_system-trademark', this.props.lang)}</div>
		</div>
	}
}

AboutPanel.defaultProps = {
	lang: 'esp'
}