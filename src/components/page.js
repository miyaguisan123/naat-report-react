import React, { Component } from 'react'
import FontAwesome from 'react-fontawesome'
import Overlay from './overlay'
import PageConfigPanel from './pageConfigPanel'
import PropTypes from 'prop-types'
import Popconfirm from 'antd/lib/popconfirm'
import Tooltip from 'antd/lib/tooltip'

import {decodeHtml} from '../functions/functions'
import {translate} from '../functions/translations'

export default class Page extends Component {
	clonePage = (e) => {
		e.stopPropagation()
		this.props.clonePage(this.props.index)
	}
	closePanel = () => {
		this.props.setConfigPanelIndex(-1)
	}
	deletePage = (e) => {
		e.stopPropagation()
		this.props.deletePage(this.props.index)
	}
	openConfigPanel = (e) => {
		e.stopPropagation()
		this.props.setConfigPanelIndex(this.props.index)
	}
	selectPage = (e) => {
		e.stopPropagation()
		this.props.selectPage(this.props.index)
	}
	render() {
		return <li data-id={this.props.index} className={this.props.className} style={{flex:'none'}}>
			{this.props.editModeEnabled && <FontAwesome className="handle" name='ellipsis-v' style={{color:'#aaa',cursor:'grab', width:20, textAlign:'center'}}/>}
			
			<button href='#' className='palette-pageTitle' onClick={this.selectPage}>{this.props.index + 1}.- {decodeHtml(this.props.page.name)}</button>
			
			{this.props.editModeEnabled && <div style={{display:'inline-block'}}>
				<Tooltip title={translate("Settings", this.props.lang)} arrowPointAtCenter={false}>
					<button onClick={this.openConfigPanel} style={{fontSize:'75%'}}><FontAwesome name='cog' style={{display:'inline-block'}}/></button>
				</Tooltip>
				
				<Popconfirm title={translate("_system-clone-page", this.props.lang)} index={this.props.index} onConfirm={this.clonePage} okText={translate("Yes", this.props.lang)} cancelText="No">
					<Tooltip title={translate("Clone", this.props.lang)} arrowPointAtCenter={false}>
						<button style={{fontSize:'75%'}}>
							<FontAwesome name='clone' style={{display:'inline-block'}}/>
						</button>
					</Tooltip>
				</Popconfirm>
				
				<Popconfirm title={translate("_system-delete-page", this.props.lang)} onConfirm={this.deletePage} okText={translate("Yes", this.props.lang)} cancelText="No">
					<Tooltip title={translate("Delete", this.props.lang)} arrowPointAtCenter={false}>
						<button style={{fontSize:'75%'}}>
							<FontAwesome name='times' style={{display:'inline-block'}}/>
						</button>
					</Tooltip>
				</Popconfirm>
			</div>}
			{(this.props.configIndex === this.props.index) && <Overlay draggable={true} centered={true} close={this.closePanel}><PageConfigPanel close={this.closePanel} lang={this.props.lang} page={this.props.page}/></Overlay>}
		</li>
	}
}

Page.propTypes = {
	className: PropTypes.string.isRequired,
	clonePage: PropTypes.func.isRequired,
	deletePage: PropTypes.func.isRequired,
	editModeEnabled: PropTypes.bool.isRequired,
	configIndex: PropTypes.number.isRequired,
	index: PropTypes.number.isRequired,
	lang: PropTypes.string.isRequired,
	page: PropTypes.object.isRequired,
	selectPage: PropTypes.func.isRequired,
	setConfigPanelIndex: PropTypes.func.isRequired,
}