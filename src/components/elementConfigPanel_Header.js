import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Form from 'antd/lib/form'
import Input from 'antd/lib/input'

const FormItem = Form.Item

export default class Header extends Component {
	updateHeader = (e) => {
		e.stopPropagation()
		this.props.updateHeader(this.props.index, e.target.value)
	}
	render() {
		var isInvalid = (this.props.field.header.length === 0)
		
		return <FormItem style={{display:'flex', alignItems:'center', marginBottom:'5px', justifyContent:'center'}} validateStatus={isInvalid ? 'error':''}>
			<Input value={this.props.field.header} style={{color:'#2c6ba8', display:'flex', flex:1, fontStyle:'italic'}} onChange={this.updateHeader}/>
		</FormItem>
	}
}

Header.propTypes = {
	field: PropTypes.object.isRequired,
	lang: PropTypes.string.isRequired,
	index: PropTypes.number.isRequired,
	updateHeader: PropTypes.func.isRequired
}