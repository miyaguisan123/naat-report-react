import React,{ Component } from 'react'
import Markdown  from 'react-remarkable'
import FontAwesome from 'react-fontawesome'
import PropTypes from 'prop-types'
import Overlay from './overlay'

import SeparatorConfigPanel from './separatorConfigPanel'
import ElementClonePanel from './elementClonePanel'
import ElementActionsBar from './elementActionsBar'

export default class Separator extends Component {
	constructor(props) {
		super(props)
		
		this.state = {
			clonePanelIsOpen: false,
			configPanelIsOpen: false,
		}
	}
	closeClonePanel = () => {
		this.setState({
			clonePanelIsOpen: false
		})
	}
	closeConfigPanel = () => {
		this.setState({
			configPanelIsOpen: false
		})
	}
	saveConfig = () => {
		var updatedForm = this.props.editor.state.form
		this.props.editor.addVersion(updatedForm, false)
	}
	deleteElement = () => {
		if (typeof this.props.row !== typeof undefined && typeof this.props.column !== typeof undefined) {
			this.props.deleteElement(this.props.row, this.props.column)
		}
		else {
			this.props.deleteElement(this.props.index)
		}
	}
	openClonePanel = () => {
		this.setState({
			clonePanelIsOpen: true
		})
	}
	openConfigPanel = () => {
		this.setState({
			configPanelIsOpen: true
		})
	}
	render() {
		var title = this.props.element.title
		var editor = this.props.editor
		var eme = editor.state.editModeEnabled
		var lang = editor.state.currentLang
		
		var padding = (this.props.padding + 'px')
		return <div style={{alignItems:'baseline',backgroundColor:this.props.element.background,display:'flex',marginLeft:padding}} data-id={this.props.element.fieldID}>
			{(eme && this.props.isInsideTable === false) && <FontAwesome className="handle palette-separatorText" name='ellipsis-v' style={{cursor:'grab',width:20,lineHeight:'40px',textAlign:'center'}}/>}
			<div className='palette-separatorText' style={{flex:1,display:'inline-flex',alignItems:'center',paddingLeft: 10,minHeight:40}}>
				<Markdown source={title}/>
			</div>
			{eme && <ElementActionsBar allowsClone={!this.props.isInsideTable} lang={lang} requestClone={this.openClonePanel} requestConfig={this.openConfigPanel} requestDelete={this.deleteElement}/>}
			{this.state.clonePanelIsOpen && <Overlay draggable={true} centered={true} close={this.closeClonePanel}>
				<ElementClonePanel cloneElement={editor.cloneElement} element={this.props.element} form={editor.state.form} close={this.closeClonePanel} lang={lang}/>
			</Overlay>}
			{this.state.configPanelIsOpen && <Overlay draggable={true} centered={true} close={this.closeConfigPanel}>
				<SeparatorConfigPanel saveConfig={this.saveConfig} close={this.closeConfigPanel} element={this.props.element} lang={lang}/>
			</Overlay>}
		</div>
	}
}

Separator.propTypes = {
	editor: PropTypes.object.isRequired,
	element: PropTypes.object.isRequired,
	isInsideTable:PropTypes.bool.isRequired,
}