import React, { Component } from 'react'
import PropTypes from 'prop-types'

import LocaleProvider from 'antd/lib/locale-provider'
import DatePicker from 'antd/lib/date-picker'
import esES from 'antd/lib/locale-provider/es_ES'
import moment from 'moment-timezone'

import {translate} from '../functions/translations'
import {minMaxAreValid} from '../functions/criteriaValidation'

moment.locale('es')

const DefaultDateFormat = 'DD/MMM/YYYY'

export default class ElementItemsDate extends Component {
	setCurrentDate = () => {
		var eme = this.props.editor.state.editModeEnabled
		
		if (eme === false) {
			var newDate = new Date()
			this.props.element.value = newDate
			var updatedForm = this.props.editor.state.form
			this.props.editor.addVersion(updatedForm, true)	
		}
	}
	saveValue = (newMoment) => {
		var eme = this.props.editor.state.editModeEnabled
		
		if (eme === false) {
			var newValue = ''
			if (newMoment !== null) {
				var date = newMoment._d
				newValue = date.toString()
			}
			
			this.props.element.value = newValue
			var updatedForm = this.props.editor.state.form
			
			this.props.editor.addVersion(updatedForm, true)
		}
	}
	render() {
		var value = null
		var className = ''
		var eme = this.props.editor.state.editModeEnabled
		var blocked = false
		
		if (eme === false) {
			blocked = (this.props.element.lock === true) || (this.props.editor.state.form.status === 2)
			var date = new Date(this.props.element.value)
			
			if (isNaN(date.getTime()) === false) {
				value = moment(date.getTime())
				
				var minDate = this.props.element.minimum
				var maxDate = this.props.element.maximum
				var minString = minDate + ''
				var maxString = maxDate + ''
				
				if (minString.length > 0 && minMaxAreValid(minDate, this.props.element.value, 'date') === false) {
					className = 'error'
				}
				else if (maxString.length > 0 && minMaxAreValid(this.props.element.value, maxDate, 'date') === false)  {
					className = 'error'
				}
			}
		}
		
		return <div className={className} style={{alignItems:'center', justifyContent:'center', display:'inline-flex', marginLeft:10, minWidth:'180px', minHeight:this.props.minHeight}}>
			{(blocked === true) && <LocaleProvider locale={esES}>
				<DatePicker defaultValue={value} onChange={this.saveValue} placeholder={translate('Date', this.props.editor.state.currentLang)} format={DefaultDateFormat} disabled/>
			</LocaleProvider>}
			{(blocked === false) && <LocaleProvider locale={esES}>
				<DatePicker defaultValue={value} onChange={this.saveValue} placeholder={translate('Date', this.props.editor.state.currentLang)} format={DefaultDateFormat}/>
			</LocaleProvider>}
			<div onClick={this.setCurrentDate} style={{padding:'5px', border:'1px solid #eeeeee', cursor:'pointer',borderRadius:'3px', marginLeft:'10px'}}>{translate('Now', this.props.editor.state.currentLang)}</div>
		</div>
	}
}

ElementItemsDate.propTypes = {
	editor: PropTypes.object.isRequired,
	element: PropTypes.object.isRequired
}