import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Sortable from 'react-sortablejs'

import {createNewElement, getElementWith} from '../functions/functions'

const toolPrefix = 'tool_'

export default class ElementItemsTableDroppable extends Component {
	itemAdded = (event) => {
		event.stopPropagation()
		
		var item = event.item
		var itemClass = event.item.getAttribute('class')
		if (itemClass === 'element_container') {
			item = item.children[0]
		}
		
		var itemType = item.getAttribute('data-id')
		var element = null
		
		if (itemType.startsWith(toolPrefix)) {
			var type = itemType.substr(toolPrefix.length)
			if (type === 'table') {
				return
			}
			
			element = createNewElement(type, this.props.lang)
		}
		else {
			var form = this.props.form
			element = getElementWith(form, itemType, 'idElement')
			
			if (element.type === 'table') {
				return
			}
		}
		
		var row = this.props.row
		var column = this.props.column
		var headers = this.props.tableElement.fields
		var header = headers[column]
		var items = header.items
		
		items[row] = element
		
		this.props.addedNewElement()
	}
	onOrderUpdate = (order, sortableContainer, event) => {
		event.preventDefault()
		event.stopPropagation()
	}
	render() {
		var options = {
			animation:0,
			ghostClass:'ghost',
			group:{
				name:'element-container'
			},
			onAdd:this.itemAdded
		}
		
		var containerStyle = {
			display: 'block',
			flex: 0,
			minHeight: 40,
			minWidth: 150
		}
		
		return <Sortable onChange={this.onOrderUpdate} options={options} style={containerStyle}/>
	}
}

ElementItemsTableDroppable.propTypes = {
	addedNewElement: PropTypes.func.isRequired,
	column: PropTypes.number.isRequired,
	row: PropTypes.number.isRequired,
	form: PropTypes.object.isRequired,
	lang: PropTypes.string.isRequired,
	tableElement: PropTypes.object.isRequired
}