import React, {Component} from 'react'
import FontAwesome from 'react-fontawesome'
import PropTypes from 'prop-types'
import MainMenu from './mainMenu'
import Overlay from './overlay'

export default class TopBar extends Component {
	constructor(props) {
		super(props)
		this.state = {
			currentOverlay: null
		}
	}
	closeMenu = () => {
		this.setState({
			currentOverlay: null
		})
	}
	showMenu = (e) => {
		e.stopPropagation()
		
		var overlay = <Overlay dismissable={true} draggable={false} centered={false} close={this.closeMenu}>
			<MainMenu close={this.closeMenu} editor={this.props.editor}/>
		</Overlay>
		
		this.setState({
			currentOverlay:overlay
		})
	}
	render() {
		var name = this.props.editor.state.form ? this.props.editor.state.form.name : ''
		var version = this.props.editor.state.form ? this.props.editor.state.form.version : ''
		
		return <div><nav className='palette-theme' style={{display:'flex',height:50,flex:'none',left: 0,top: 0,position:'fixed',width:'100%'}}>
			<div style={{display:'inline-flex', flexDirection:'row', flex:'none'}} id='NaatEditorMenu' onClick={this.showMenu}>
				<FontAwesome name='navicon' style={{alignItems:'center', color:'#ffffff', cursor:'pointer', display:'flex', fontSize:'110%', height:50, justifyContent:'center', width:30}}/>
				<FontAwesome name='caret-down' style={{alignItems:'center', color:'#ffffff', cursor:'pointer', display:'flex', fontSize:'80%', height:50, justifyContent:'center'}}/>
			</div>
			<label style={{alignItems:'center', color:'#ffffff', display:'flex', fontWeight:'bold', justifyContent:'center', marginLeft:20}}>{name} - v{version}</label>
		</nav>
		{this.state.currentOverlay}
		</div>
	}
}

TopBar.propTypes = {
	editor: PropTypes.object.isRequired
}