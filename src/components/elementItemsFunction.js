import React, { Component } from 'react'
import Autosize from 'react-input-autosize'
import PropTypes from 'prop-types'
import moment from 'moment-timezone'

import {getFormulaType, resultFromFormula} from '../functions/formulas'

moment.locale('es')
	
export default class ElementItemsFunction extends Component {
	getFormulaValue = () => {
		var formula = this.props.element.formula
		var precision = this.props.element.precision
		var result = ''
		
		if (formula.length > 0) {
			var form = this.props.editor.state.form
			result = resultFromFormula(form, formula, precision)
		}
		
		return result
	}
	render() {
		var currentValue = ''
		
		if (this.props.editor.state.editModeEnabled === false) {
			currentValue = this.getFormulaValue()
			this.props.element.value = currentValue
			var type = getFormulaType(this.props.editor.state.form, this.props.element.formula)
			
			if ((currentValue + '').length > 0) {
				if (type === 'number') {
					var precision = this.props.element.precision
					var numericResult = parseFloat(currentValue)
					
					if (isNaN(numericResult) === false) {
						currentValue = numericResult.toFixed(parseInt(precision, 10))
					}
				}
				else if (type === 'days') {
					currentValue = parseInt((currentValue / (86400000)), 10)
				}
				else if (type === 'milliseconds') {
					var date = moment(parseInt(currentValue, 10)).format('h:mm:ss a')
					currentValue = date.toUpperCase()
				}
			}
		}
		
		return <div style={{alignItems:'center', justifyContent:'center', display:'inline-flex', marginLeft:10, minHeight:this.props.minHeight}}>
			<Autosize className="autosize-input flex-row" value={currentValue} readOnly={true}/>
		</div>
	}
}

ElementItemsFunction.propTypes = {
	editor: PropTypes.object.isRequired,
	element: PropTypes.object.isRequired
}