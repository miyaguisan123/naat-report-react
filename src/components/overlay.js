import React, { Component } from 'react'
import Draggable from 'react-draggable'

export default class Overlay extends Component {
	close = (e) => {
		e.stopPropagation()
		
		if (this.props.dismissable === true) {
			this.props.close()
		}
	}
	render() {
		var overlayStyle = {
			alignItems: this.props.centered && 'center',
			backgroundColor: this.props.color,
			display: this.props.centered ? 'flex':'block',
			justifyContent: this.props.centered && 'center',
			position: 'fixed',
			top: 0,
			left: 0,
			margin: 0,
			padding: 0,
			width: '100%',
			height: '100vh',
			zIndex: 20
		}
		
		if (this.props.hackTextSelection === true) {
			overlayStyle.height = '0px'
			overlayStyle.marginTop = '450px'
		}
		
		return <div style={overlayStyle} onClick={this.close}>
			{(this.props.draggable === true && <Draggable><div>{this.props.children}</div></Draggable>)}
			{(this.props.draggable === false && <div>{this.props.children}</div>)}
		</div>
	}
}

Overlay.defaultProps = {
	dismissable: true,
	draggable: true,
	centered: true,
	hackTextSelection: false,
	color: 'rgba(0,0,0,0.0)'
}