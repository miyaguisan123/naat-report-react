import React, { Component } from 'react'
import Draggable from 'react-draggable'

export default class Draggable extends Component {
	render() {
		return <Draggable>{this.props.children}</Draggable>
	}
}