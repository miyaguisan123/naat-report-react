import React, {Component} from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'

import Button from 'antd/lib/button'
import Checkbox from 'antd/lib/checkbox'
import DatePicker from 'antd/lib/date-picker'
import Form from 'antd/lib/form'
import Icon from 'antd/lib/icon'
import Input from 'antd/lib/input'
import InputNumber from 'antd/lib/input-number'
import LocaleProvider from 'antd/lib/locale-provider'
import message from 'antd/lib/message'
import Radio from 'antd/lib/radio'
import TimePicker from 'antd/lib/time-picker'

import enUS from 'antd/lib/locale-provider/en_US'
import moment from 'moment'

import FontAwesome from 'react-fontawesome'
import {decodeHtml, getElementWith, removeAt, dataSourceHelperWSURL} from '../functions/functions'
import {updateElementId} from '../functions/reordering'

import {minMaxAreValid} from '../functions/criteriaValidation'
import {translate} from '../functions/translations'

import Criteria from './elementConfigPanel_Criteria'
import Header from './elementConfigPanel_Header'
import Option from './elementConfigPanel_Option'

moment.locale('es')

const ButtonGroup = Button.Group
const RadioGroup = Radio.Group
const DefaultDateFormat = 'DD/MMM/YYYY'
const DefaultTimeFormat = 'h:mm:ss A'
const FormItem = Form.Item

const spanColor = '#2c6ba8'

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 15 },
  },
}

export default class ElementConfigPanel extends Component {
	constructor(props) {
		super(props)
		
		var stateClone = JSON.parse(JSON.stringify(this.props.element))
		
		this.state = {
			configGeneralOpen: true,
			configTextOpen: true,
			configDataSourceOpen: true,
			configFormulaOpen: true,
			configRangeOpen: true,
			configPropertiesOpen: true,
			configOptionsOpen: true,
			configTableOpen: true,
			errors: [],
			connections: [],
			tables: [],
			fields: [],
			...stateClone
		}
	}
	componentDidMount() {
		if (this.props.element.type === 'dataSource') {
			this.loadConnections()
		}
	}
	loadConnections = () => {
		var config = {
			method: 'post',
			headers: {
				'Accept':'application/json, text/javascript, */*; q=0.01'
			},
			json: true,
			data: {
				action: 'list_dbs',
				token: this.props.editor.props.token
			},
			url: dataSourceHelperWSURL
		}
		
		axios.request(config).then(response => {
			var jsonResponse = response.request.response
			var jsonObject = JSON.parse(jsonResponse)
			
			if (jsonObject.length > 0) {
				this.setState({
					connections: jsonObject,
					tables: [],
					fields: []
				})
			}
			else {
				this.setState({
					connections: [],
					tables: [],
					fields: []
				})
			}
		}).catch(error => {
			
		})
	}
	loadTables = () => {
		var connections = this.state.connections
		var dbConfig = this.state.dataSourceConfig
		
		var config = {
			method: 'post',
			headers: {
				'Accept':'application/json, text/javascript, */*; q=0.01'
			},
			json: true,
			data: {
				action: 'list_tables',
				token: this.props.editor.props.token,
				connection: connections[this.currentConnection].connection,
				dataBase: dbConfig.sourceDB
			},
			url: dataSourceHelperWSURL
		}
		
		axios.request(config).then(response => {
			var jsonResponse = response.request.response
			var jsonObject = JSON.parse(jsonResponse)
			
			if (jsonObject.length > 0) {
				this.setState({
					tables: jsonObject,
					fields: []
				})
			}
			else {
				this.setState({
					tables: [],
					fields: []
				})
			}
		}).catch(error => {
			
		})
	}
	loadFields = () => {
		var connections = this.state.connections
		var dbConfig = this.state.dataSourceConfig
		
		var config = {
			method: 'post',
			headers: {
				'Accept':'application/json, text/javascript, */*; q=0.01'
			},
			json: true,
			data: {
				action: 'list_fields',
				token: this.props.editor.props.token,
				connection: connections[this.currentConnection].connection,
				dataBase: dbConfig.sourceDB,
				table: dbConfig.sourceTable
			},
			url: dataSourceHelperWSURL
		}
		
		axios.request(config).then(response => {
			var jsonResponse = response.request.response
			var jsonObject = JSON.parse(jsonResponse)
			
			if (jsonObject.length > 0) {
				this.setState({
					fields: jsonObject
				})
			}
			else {
				this.setState({
					fields: []
				})
			}
		}).catch(error => {
			
		})
	}
	handleClick = (e) => {
		e.stopPropagation()
	}
	validateChanges = (e) => {
		e.stopPropagation()
		
		var element = this.props.element
		var form = this.props.form
		
		var errors = []
		
		if (this.state.fieldID.length === 0) {
			message.error(translate('_system-error-empty-id', this.props.lang), 3.0)
			errors.push('fieldID')
		}
		else if (this.state.fieldID !== element.fieldID && getElementWith(form, this.state.fieldID, 'idElement') !== null) {
			message.error(translate('_system-error-duplicated-id', this.props.lang), 5.0)
			errors.push('fieldID')
		}
		
		if (this.state.xmlID.length > 0 && this.state.xmlID !== element.xmlID && getElementWith(form, this.state.xmlID, 'idXML') !== null) {
			message.error(translate('_system-error-duplicated-idXML', this.props.lang), 5.0)
			errors.push('xmlID')
		}
		
		if (typeof this.state.minimum !== typeof undefined) {
			if (minMaxAreValid(this.state.minimum, this.state.maximum, this.props.element.type) === false) {
				message.error(translate('_system-error-min-bigger-than-max', this.props.lang), 5.0)
				errors.push('maximum')
				errors.push('minimum')
			}
		}
		
		var criterias = this.state.criteria
		var totalCriterias = criterias.length
		for (var i=0;i<totalCriterias;i++) {
			var criteria = criterias[i]
			var idElement = criteria.field
			var value = criteria.value + ''
			
			if (idElement.length === 0 || value.length === 0) {
				message.error(translate('_system-error-no-empty-criteria', this.props.lang), 5.0)
				errors.push('criteria')
				break
			}
			else {
				if (getElementWith(form, idElement, 'idElement') === null) {
					message.error(translate('_system-error-unkown-element', this.props.lang) + ': ' + idElement, 5.0)
					errors.push('criteria')
					break
				}
			}
		}
		
		if (typeof this.props.element.options !== typeof undefined) {
			var options = this.state.options
			var totalOptions = options.length
			
			for (var currentOption=0;currentOption<totalOptions;currentOption++) {
				var option = options[currentOption]
				var optionText = option.text
				var optionValue = option.value + ''
				var optionDict = option.dict
				
				if (optionValue !== '-1' && (optionText.length === 0 || optionDict.length === 0)) {
					message.error(translate('_system-error-no-empty-options', this.props.lang), 5.0)
					errors.push('options')
					break
				}
			}
		}
		
		if (typeof this.props.element.radios !== typeof undefined) {
			var radios = this.state.radios
			var totalRadios = radios.length
			
			for (var currentRadio=0;currentRadio<totalRadios;currentRadio++) {
				var radio = radios[currentRadio]
				var text = radio.text
				var dict = radio.dict
				
				if (text.length === 0 || dict.length === 0) {
					message.error(translate('_system-error-no-empty-options', this.props.lang), 5.0)
					errors.push('radios')
					break
				}
			}
		}
		
		if (typeof this.props.element.checkboxs !== typeof undefined) {
			var checkboxes = this.state.checkboxs
			var totalCheckboxes = checkboxes.length
			
			for (var currentCheckbox=0;currentCheckbox<totalCheckboxes;currentCheckbox++) {
				var checkbox = checkboxes[currentCheckbox]
				var checkboxText = checkbox.text
				var checkboxDict = checkbox.dict
				
				if (checkboxText.length === 0 || checkboxDict.length === 0) {
					message.error(translate('_system-error-no-empty-options', this.props.lang), 5.0)
					errors.push('checkboxes')
					break
				}
			}
		}
		
		if (typeof this.props.element.fields !== typeof undefined) {
			var fields = this.state.fields
			var totalFields = fields.length
			
			for (var currentField=0;currentField<totalFields;currentField++) {
				var field = fields[currentField]
				if (field.header.length === 0) {
					message.error(translate('_system-error-no-empty-header', this.props.lang), 5.0)
					errors.push('headers')
					break
				}
			}
		}
		
		if (errors.length > 0) {
			message.error(translate('_system-error-bad-configuration', this.props.lang), 5.0)
			
			this.setState({
				errors: errors
			})
		}
		else {
			if (this.props.element.field !== this.state.fieldID) {
				var idNew = this.state.fieldID
				updateElementId(this.props.form, this.props.element, idNew)
				this.props.element.fieldID = this.state.fieldID
			}
			
			this.props.element.xmlID = this.state.xmlID
			this.props.element.leftText = this.state.leftText
			this.props.element.rightText = this.state.rightText
			this.props.element.lock = this.state.lock
			this.props.element.required = this.state.required
			this.props.element.visible = this.state.visible
			
			if (typeof this.props.element.maxLength !== typeof undefined) {
				this.props.element.maxLength = this.state.maxLength
			}
			
			if (typeof this.props.element.expression !== typeof undefined) {
				this.props.element.expression = this.state.expression
			}
			
			if (typeof this.props.element.formula !== typeof undefined && this.props.element.formula !== this.state.formula) {
				this.props.element.formula = this.state.formula
			}
			
			if (typeof this.props.element.minimum !== typeof undefined) {
				this.props.element.minimum = this.state.minimum
				this.props.element.maximum = this.state.maximum
			}
			
			if (typeof this.props.element.precision !== typeof undefined) {
				this.props.element.precision = this.state.precision
			}
			
			if (typeof this.props.element.step !== typeof undefined) {
				this.props.element.step = this.state.step
			}
			
			this.props.element.criteria = this.state.criteria
			
			if (typeof this.props.element.options !== typeof undefined) {
				this.props.element.options = this.state.options
				this.props.element.default = this.state.default
			}
			
			if (typeof this.props.element.radios !== typeof undefined) {
				this.props.element.radios = this.state.radios
			}
			
			if (typeof this.props.element.checkboxs !== typeof undefined) {
				this.props.element.checkboxs = this.state.checkboxs
			}
			
			if (typeof this.props.element.orientation !== typeof undefined) {
				this.props.element.orientation = this.state.orientation
			}
			
			if (typeof this.props.element.fields !== typeof undefined) {
				this.props.element.fields = this.state.fields
			}
			
			if (typeof this.props.element.title !== typeof undefined) {
				this.props.element.title = this.state.title
			}
			
			if (typeof this.props.element.chart !== typeof undefined) {
				this.props.element.chart = this.state.chart
			}
			
			if (this.props.element.type === 'external_file') {
				this.props.element.value = this.state.value
				this.props.element.width = this.state.width
				this.props.element.height = this.state.height
			}
			
			if (typeof this.props.element.dataSourceConfig !== typeof undefined) {
				this.props.element.dataSourceConfig = this.state.dataSourceConfig
			}
			
			this.props.saveConfig()
		}
	}
	switchGeneralConfig = (e) => {
		e.stopPropagation()
		
		this.setState({
			configGeneralOpen: !this.state.configGeneralOpen
		})
	}
	switchTextConfig = (e) => {
		e.stopPropagation()
		
		this.setState({
			configTextOpen: !this.state.configTextOpen
		})
	}
	switchDataSourceConfig = (e) => {
		e.stopPropagation()
		
		this.setState({
			configDataSourceOpen: !this.state.configDataSourceOpen
		})
	}
	switchFormulaConfig = (e) => {
		e.stopPropagation()
		
		this.setState({
			configFormulaOpen: !this.state.configFormulaOpen
		})
	}
	switchRangeConfig = (e) => {
		e.stopPropagation()
		
		this.setState({
			configRangeOpen: !this.state.configRangeOpen
		})
	}
	switchPropertiesConfig = (e) => {
		e.stopPropagation()
		
		this.setState({
			configPropertiesOpen: !this.state.configPropertiesOpen
		})
	}
	switchOptionsConfig = (e) => {
		e.stopPropagation()
		
		this.setState({
			configOptionsOpen: !this.state.configOptionsOpen
		})
	}
	switchTableConfig = (e) => {
		e.stopPropagation()
		
		this.setState({
			configTableOpen: !this.state.configTableOpen
		})
	}
	setFieldID = (e) => {
		e.stopPropagation()
		
		this.setState({
			fieldID: e.target.value
		})
	}
	setXMLID = (e) => {
		e.stopPropagation()
		
		this.setState({
			xmlID: e.target.value
		})
	}
	setLeftText = (e) => {
		e.stopPropagation()
		
		this.setState({
			leftText: e.target.value
		})
	}
	setRightText = (e) => {
		e.stopPropagation()
		
		this.setState({
			rightText: e.target.value
		})
	}
	setLocked = (e) => {
		e.stopPropagation()
		
		this.setState({
			lock: !this.state.lock
		})
	}
	setRequired = (e) => {
		e.stopPropagation()
		
		this.setState({
			required: !this.state.required
		})
	}
	setVisible = (e) => {
		e.stopPropagation()
		
		this.setState({
			visible: !this.state.visible
		})
	}
	setMaxChars = (newValue) => {
		if (typeof newValue === typeof undefined) {
			newValue = ''
		}
		
		this.setState({
			maxLength: newValue
		})
	}
	setREGEX = (e) => {
		e.stopPropagation()
		
		this.setState({
			expression: e.target.value
		})
	}
	setFormula = (e) => {
		e.stopPropagation()
		
		this.setState({
			formula: e.target.value
		})
	}
	setDefaultOption = (e) => {
		e.stopPropagation()
		
		var newValue = e.target.value
		this.setState({
			default: newValue
		})
	}
	renderConnectionsDataBases = () => {
		var options = []
		var connections = this.state.connections
		options.push(<option key='base_empty' value=''></option>)
		
		var currentDB = this.state.dataSourceConfig.sourceDB
		if (connections.length === 0 && (typeof currentDB !== typeof undefined && currentDB.length > 0)) {
			options.push(<option key={'base_0_0'} value={currentDB}>{currentDB}</option>)
		}
		
		connections.forEach(function(connection, connection_index) {
			connection.bases.forEach(function(base, base_index) {
				options.push(<option key={'base_'+connection_index+'_'+base_index} value={base}>{base}</option>)
			})
		})
		
		return options
	}
	setDataBase = (e) => {
		e.stopPropagation()
		this.setDataBaseNamed(e.target.value)
	}
	setDataBaseNamed = (data) => {
		if (typeof data === typeof undefined || data.length === 0) {
			return
		}
		
		var connections = this.state.connections
		var found = false
		
		for (var connection_index=0;connection_index < connections.length;connection_index++) {
			var connection = connections[connection_index]
			for (var base_index=0;base_index < connection.bases.length;base_index++) {
				var base = connection.bases[base_index]
				found = true
				
				if (base === data) {
					this.currentConnection = connection_index
					var dbConfig = this.state.dataSourceConfig
					dbConfig.sourceDB = base
					dbConfig.connection = connection_index
					dbConfig.sourceTable = ''
					dbConfig.sourceValue = ''
					dbConfig.sourceDescription = ''
					
					this.setState({
						dataSourceConfig: dbConfig
					})
					
					this.loadTables()
					break
				}
			}
			
			if (found) {
				break
			}
		}
	}
	renderTables = () => {
		var options = []
		var tables = this.state.tables
		options.push(<option key='table_empty' value=''></option>)
		
		var currentTable = this.state.dataSourceConfig.sourceTable
		if (tables.length === 0 && (typeof currentTable !== typeof undefined && currentTable.length > 0)) {
			options.push(<option key={'table_0'} value={currentTable}>{currentTable}</option>)
		}
		else if (tables.length > 0) {
			tables.forEach(function(table, table_index) {
				options.push(<option key={'table_'+table_index} value={table}>{table}</option>)
			})
		}
		
		return options
	}
	setTable = (e) => {
		e.stopPropagation()
		
		var dbConfig = this.state.dataSourceConfig
		dbConfig.sourceTable = e.target.value
		dbConfig.sourceValue = ''
		dbConfig.sourceDescription = ''
		
		this.setState({
			dataSourceConfig: dbConfig
		})
		
		this.loadFields()
	}
	renderFields = (type) => {
		var options = []
		var fields = this.state.fields
		options.push(<option key={'field_empty_'+type} value=''></option>)
		
		var currentField = ''
		if (type === 'id') {
			currentField = this.state.dataSourceConfig.sourceValue
		}
		else if (type === 'description') {
			currentField = this.state.dataSourceConfig.sourceDescription
		}
		else if (type === 'extra') {
			currentField = this.state.dataSourceConfig.sourceExtra
		}
		
		if (fields.length === 0 && (typeof currentField !== typeof undefined && currentField.length > 0)) {
			options.push(<option key={'field_0'} value={currentField}>{currentField}</option>)
		}
		
		fields.forEach(function(field, field_index) {
			options.push(<option key={'field_'+field_index+'_'+type} value={field}>{field}</option>)
		})
		
		return options
	}
	setTableID = (e) => {
		e.stopPropagation()
		
		var dbConfig = this.state.dataSourceConfig
		dbConfig.sourceValue = e.target.value
		this.setState({
			dataSourceConfig: dbConfig
		})
	}
	setTableDescription = (e) => {
		e.stopPropagation()
		
		var dbConfig = this.state.dataSourceConfig
		dbConfig.sourceDescription = e.target.value
		this.setState({
			dataSourceConfig: dbConfig
		})
	}
	setTableExtra = (e) => {
		e.stopPropagation()
		var dbConfig = this.state.dataSourceConfig
		dbConfig.sourceExtra = e.target.value
		
		this.setState({
			dataSourceConfig: dbConfig
		})
	}
	setMinimum = (newValue) => {
		if (typeof newValue === typeof undefined) {
			newValue = ''
		}
		
		this.setState({
			minimum: newValue
		})
	}
	setMinimumDate = (newMoment) => {
		var newValue = ''
		if (typeof newMoment !== typeof undefined) {
			if (newMoment !== null) {
				var date = newMoment._d
				newValue = date.toISOString()
			}
		}
		
		this.setState({
			minimum: newValue
		})
	}
	setMinimumTime = (newMoment) => {
		var newValue = ''
		if (typeof newMoment !== typeof undefined) {
			if (newMoment !== null) {
				var date = newMoment._d
				newValue = date.toISOString()
			}
		}
		
		this.setState({
			minimum: newValue
		})
	}
	setMaximum = (newValue) => {
		if (typeof newValue === typeof undefined) {
			newValue = ''
		}
		
		this.setState({
			maximum: newValue
		})
	}
	setMaximumDate = (newMoment) => {
		var newValue = ''
		if (typeof newMoment !== typeof undefined) {
			if (newMoment !== null) {
				var date = newMoment._d
				newValue = date.toISOString()
			}
		}
		
		this.setState({
			maximum: newValue
		})
	}
	setMaximumTime = (newMoment) => {
		var newValue = ''
		if (typeof newMoment !== typeof undefined) {
			if (newMoment !== null) {
				var date = newMoment._d
				newValue = date.toISOString()
			}
		}
		
		this.setState({
			maximum: newValue
		})
	}
	setStep = (newValue) => {
		if (typeof newValue === typeof undefined || newValue < 0) {
			newValue = 0
		}
		
		if (typeof this.state.precision !== typeof undefined) {
			this.setState({
				precision: newValue
			})
		}
		else if (typeof this.state.step !== typeof undefined) {
			this.setState({
				step: newValue
			})
		}
	}
	setOrientation = (e) => {
		e.stopPropagation()
		
		this.setState({
			orientation: e.target.value
		})
	}
	setLink = (e) => {
		e.stopPropagation()
		
		this.setState({
			value: e.target.value
		})
	}
	setWidth = (newValue) => {
		if (typeof newValue === typeof undefined) {
			newValue = 100
		}
		
		this.setState({
			width: newValue
		})
	}
	setHeight = (newValue) => {
		if (typeof newValue === typeof undefined) {
			newValue = 100
		}
		
		this.setState({
			height: newValue
		})
	}
	setDependencyIdElement = (e) => {
		e.stopPropagation()
		
		var config = this.state.dataSourceConfig
		var dependency = config.dependencies
		dependency.editorID = e.target.value
		
		this.setState({
			dataSourceConfig: config
		})
	}
	setDependencyTableField = (e) => {
		e.stopPropagation()
		
		var config = this.state.dataSourceConfig
		var dependency = config.dependencies
		dependency.tableField = e.target.value
		
		this.setState({
			dataSourceConfig: config
		})
	}
	setTableTitle = (e) => {
		e.stopPropagation()
		
		this.setState({
			title: e.target.value
		})
	}
	setTableChart = (e) => {
		e.stopPropagation()
		
		this.setState({
			chart: e.target.value
		})
	}
	addNewCriteria = (e) => {
		e.stopPropagation()
		
		var newCriteria = {}
		newCriteria.optional = false
		newCriteria.field = ''
		newCriteria.value = ''
		newCriteria.optional = false
		newCriteria.equality = '==='
		
		var criterias = this.state.criteria
		criterias.push(newCriteria)
		this.setState({
			criterias: criterias
		})
	}
	updateCriteria = (index, optional, field, value, equality) => {
		var updatedCriterias = this.state.criteria
		var criteria = updatedCriterias[index]
		
		criteria.optional = optional
		criteria.field = field
		criteria.value = value
		criteria.equality = equality
		
		this.setState({
			criterias: updatedCriterias
		})
	}
	deleteCriteria = (index) => {
		var remainingCriterias = removeAt(this.state.criteria, index)
		
		this.setState({
			criteria: remainingCriterias
		})
	}
	addNewOption = (e) => {
		e.stopPropagation()
		
		var updatedOptions = this.state.options
		var newIndex = updatedOptions.length
		
		if (updatedOptions.length > 0) {
			var last = updatedOptions[newIndex - 1]
			var lastIndex = last.value
			
			if (newIndex <= lastIndex) {
				newIndex = lastIndex + 1
			}
		}
		
		var newOption = {}
		newOption.value = newIndex
		newOption.text = ''
		newOption.dict = ''
		updatedOptions.push(newOption)
		
		this.setState({
			options: updatedOptions
		})
	}
	addNewRadio = (e) => {
		e.stopPropagation()
		
		var updatedRadios = this.state.radios
		var newIndex = updatedRadios.length
		
		if (updatedRadios.length > 0) {
			var last = updatedRadios[newIndex - 1]
			var lastIndex = last.value
			
			if (newIndex <= lastIndex) {
				newIndex = lastIndex + 1
			}
		}
		
		var newRadio = {}
		newRadio.value = newIndex
		newRadio.text = ''
		newRadio.dict = ''
		updatedRadios.push(newRadio)
		
		this.setState({
			radios: updatedRadios
		})
	}
	addNewCheckbox = (e) => {
		e.stopPropagation()
		
		var updatedCheckboxes = this.state.checkboxs
		var newIndex = updatedCheckboxes.length
		
		if (updatedCheckboxes.length > 0) {
			var last = updatedCheckboxes[newIndex - 1]
			var lastIndex = last.value
			
			if (newIndex <= lastIndex) {
				newIndex = lastIndex + 1
			}
		}
		
		var newCheckbox = {}
		newCheckbox.value = newIndex
		newCheckbox.text = ''
		newCheckbox.dict = ''
		updatedCheckboxes.push(newCheckbox)
		
		this.setState({
			checkboxs: updatedCheckboxes
		})
	}
	updateOption = (index, value, text, dictionary) => {
		var updatedOptions = this.state.options
		
		var option = updatedOptions[index]
		option.value = value
		option.text = text
		option.dict = dictionary
		
		this.setState({
			options: updatedOptions
		})
	}
	updateRadio = (index, value, text, dictionary) => {
		var updatedRadios = this.state.radios
		
		var radio = updatedRadios[index]
		radio.value = value
		radio.text = text
		radio.dict = dictionary
		
		this.setState({
			radios: updatedRadios
		})
	}
	updateCheckbox = (index, value, text, dictionary) => {
		var updatedCheckboxes = this.state.checkboxs
		
		var checkbox = updatedCheckboxes[index]
		checkbox.value = value
		checkbox.text = text
		checkbox.dict = dictionary
		
		this.setState({
			checkboxs: updatedCheckboxes
		})
	}
	updateHeader = (index, newTitle) => {
		var updatedFields = this.state.fields
		var field = updatedFields[index]
		field.header = newTitle
		
		this.setState({
			fields: updatedFields
		})
	}
	deleteOption = (index) => {
		var remainingOptions = removeAt(this.state.options, index)
		
		this.setState({
			options: remainingOptions
		})
	}
	deleteRadio = (index) => {
		var remainingRadios = removeAt(this.state.radios, index)
		
		this.setState({
			radios: remainingRadios
		})
	}
	deleteCheckbox = (index) => {
		var remainingCheckboxes = removeAt(this.state.checkboxs, index)
		
		this.setState({
			checkboxs: remainingCheckboxes
		})
	}
	dateRange = () => {
		var minDate = new Date(this.state.minimum)
		var maxDate = new Date(this.state.maximum)
		var minMoment = null
		var maxMoment = null
		
		if (isNaN(minDate.getTime()) === false) {
			minMoment = moment(minDate.getTime())
		}
		
		if (isNaN(maxDate.getTime()) === false) {
			maxMoment = moment(maxDate.getTime())
		}
		
		var elements = []
		var min = <FormItem {...formItemLayout} key='date_min' label={translate('Minimum', this.props.lang)} validateStatus={this.state.errors.includes('minimum') ? 'error':''}>
			<LocaleProvider locale={enUS}><DatePicker defaultValue={minMoment} onChange={this.setMinimumDate} placeholder={translate('Date', this.props.lang)}  format={DefaultDateFormat}/></LocaleProvider>
		</FormItem>
		elements.push(min)
		
		var max = <FormItem {...formItemLayout} key='date_max' label={translate('Maximum', this.props.lang)} validateStatus={this.state.errors.includes('maximum') ? 'error':''}>
			<LocaleProvider locale={enUS}><DatePicker defaultValue={maxMoment} onChange={this.setMaximumDate} placeholder={translate('Date', this.props.lang)}  format={DefaultDateFormat}/></LocaleProvider>
		</FormItem>
		elements.push(max)
		
		return elements
	}
	timeRange = () =>	{
		var minDate = new Date(this.state.minimum)
		var maxDate = new Date(this.state.maximum)
		var minMoment = null
		var maxMoment = null
		
		if (isNaN(minDate.getTime()) === false) {
			minMoment = moment(minDate.getTime())
		}
		
		if (isNaN(maxDate.getTime()) === false) {
			maxMoment = moment(maxDate.getTime())
		}
		
		var elements = []
		var min = <FormItem {...formItemLayout} key='date_min' label={translate('Minimum', this.props.lang)} validateStatus={this.state.errors.includes('minimum') ? 'error':''}>
			<LocaleProvider locale={enUS}><TimePicker defaultValue={minMoment} format={DefaultTimeFormat} onChange={this.setMinimumTime}/></LocaleProvider>
		</FormItem>
		elements.push(min)
		
		var max = <FormItem {...formItemLayout} key='date_max' label={translate('Maximum', this.props.lang)} validateStatus={this.state.errors.includes('maximum') ? 'error':''}>
			<LocaleProvider locale={enUS}><TimePicker defaultValue={maxMoment} format={DefaultTimeFormat} onChange={this.setMaximumTime}/></LocaleProvider>
		</FormItem>
		elements.push(max)
		
		return elements
	}
	numericRange = () => {
		var minimum = this.state.minimum
		var maximum = this.state.maximum
		var configStep = parseInt(this.state.precision, 10)
		
		if (isNaN(configStep)) {
			configStep = 0
		}
		var elementStep = (1.0 / Math.pow(10, parseInt(configStep, 10)))
		
		var elements = []
		var min = <FormItem {...formItemLayout} key='numeric_min' label={translate('Minimum', this.props.lang)} validateStatus={this.state.errors.includes('minimum') ? 'error':''}>
			<InputNumber step={elementStep} onChange={this.setMinimum} value={minimum} placeholder={translate('Minimum', this.props.lang)} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
		</FormItem>
		elements.push(min)
		
		var max = <FormItem {...formItemLayout} key='numeric_max' label={translate('Maximum', this.props.lang)} validateStatus={this.state.errors.includes('maximum') ? 'error':''}>
			<InputNumber step={elementStep} onChange={this.setMaximum} value={maximum} placeholder={translate('Maximum', this.props.lang)} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
		</FormItem>
		elements.push(max)
		
		if (typeof this.state.precision !== typeof undefined) {
			var precision = <FormItem {...formItemLayout} key='numeric_step' label={translate('Step', this.props.lang)} validateStatus=''>
				<InputNumber onChange={this.setStep} value={configStep} placeholder={translate('Step', this.props.lang)} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
			</FormItem>
			elements.push(precision)
		}
		
		return elements
	}
	sliderRange = () => {
		var minimum = this.state.minimum
		var maximum = this.state.maximum
		var configStep = parseFloat(this.state.step)
		if (isNaN(configStep)) {
			configStep = 1
		}
		
		var elements = []
		var min = <FormItem {...formItemLayout} key='numeric_min' label={translate('Minimum', this.props.lang)} validateStatus={this.state.errors.includes('minimum') ? 'error':''}>
			<InputNumber onChange={this.setMinimum} value={minimum} placeholder={translate('Minimum', this.props.lang)} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
		</FormItem>
		elements.push(min)
		
		var max = <FormItem {...formItemLayout} key='numeric_max' label={translate('Maximum', this.props.lang)} validateStatus={this.state.errors.includes('maximum') ? 'error':''}>
			<InputNumber onChange={this.setMaximum} value={maximum} placeholder={translate('Maximum', this.props.lang)} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
		</FormItem>
		elements.push(max)
		
		if (typeof this.state.step !== typeof undefined) {
			var precision = <FormItem {...formItemLayout} key='slider_step' label={translate('Units', this.props.lang)} validateStatus=''>
				<InputNumber onChange={this.setStep} value={configStep} placeholder={translate('Units', this.props.lang)} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
			</FormItem>
			elements.push(precision)
		}
		
		return elements
	}
	renderOptions = () => {
		var currentOptions = this.state.options
		var totalOptions = currentOptions.length
		var optionElements = []
		
		for (var i=1;i<totalOptions;i++) {
			var option = currentOptions[i]
			optionElements.push(<Option key={'option_'+i} option={option} lang={this.props.lang} index={i} updateOption={this.updateOption} deleteOption={this.deleteOption}/>)
		}
		
		return optionElements
	}
	renderDefaultOptions = () => {
		var options = []
		var currentOptions = this.state.options
		var totalOptions = currentOptions.length
		
		for (var i=0;i<totalOptions;i++) {
			var option = currentOptions[i]
			var optionElement = <option key={"default_"+i} value={option.value}>{decodeHtml(option.text)}</option>
			options.push(optionElement)
		}
		
		return options
	}
	renderRadios = () => {
		var elements = []
		var radios = this.state.radios
		var totalRadios = radios.length
		
		for (var i=0;i<totalRadios;i++) {
			var radio = radios[i]
			elements.push(<Option key={'radio_'+i} option={radio} lang={this.props.lang} index={i} updateOption={this.updateRadio} deleteOption={this.deleteRadio}/>)
		}
		
		return elements
	}
	renderCheckboxes = () => {
		var elements = []
		var checkboxes = this.state.checkboxs
		var totalCheckboxes = checkboxes.length
		
		for (var i=0;i<totalCheckboxes;i++) {
			var checkbox = checkboxes[i]
			elements.push(<Option key={'checkbox_'+i} option={checkbox} lang={this.props.lang} index={i} updateOption={this.updateCheckbox} deleteOption={this.deleteCheckbox}/>)
		}
		
		return elements
	}
	renderCriterias = () => {
		var currentCriterias = this.state.criteria
		var totalCriterias = currentCriterias.length
		var criteriaElements = []
		
		for (var i=0;i<totalCriterias;i++) {
			criteriaElements.push(<Criteria key={'criteria_'+i} criteria={currentCriterias[i]} lang={this.props.lang} index={i} updateCriteria={this.updateCriteria} deleteCriteria={this.deleteCriteria}/>)
		}
		
		return criteriaElements
	}
	renderHeaders = () => {
		var fields = this.state.fields
		var totalFields = fields.length
		var headers = []
		
		for (var i=0;i<totalFields;i++) {
			var field = fields[i]
			var header = <Header key={'header_'+i} field={field} index={i} lang={this.props.lang} updateHeader={this.updateHeader}/>
			headers.push(header)
		}
		
		return headers
	}
	render() {
		var maxChars = parseInt(this.state.maxLength, 10)
		if (isNaN(maxChars) || maxChars === 0) {maxChars = ''}
		
		var visibilitySectionTitle = 'To hide'
		if (this.state.visible === false) { visibilitySectionTitle = 'To show' }
		
		var editorID = ''
		var tableField = ''
		
		if (this.props.element.type === 'dataSource') {
			if (typeof this.state.dataSourceConfig.dependencies !== typeof undefined) {
				editorID = this.state.dataSourceConfig.dependencies.editorID
				tableField = this.state.dataSourceConfig.dependencies.tableField
			}
		}
		
		return <Form className='dropshadow' onClick={this.handleClick} style={{backgroundColor:'#ffffff', fontSize:'90%', borderRadius:3, display:'flex', flexDirection:'column', minWidth:'340px', maxWidth:'380px', padding:10}}>
			<div style={{alignItems:'flex-start', display:'flex'}}>
				<span style={{color:'#2c6ba8',fontWeight:'bold',marginRight:10}}>{translate('Configuration', this.props.lang)}:</span>
				<span style={{flex:1}}>[{this.state.fieldID}]</span>
				<FontAwesome name='times' onClick={this.props.close} style={{alingItems:'center', color:'#2c6ba8', cursor:'pointer', display:'flex', justifyContent:'flex-end', height:25, width: 30}}/>
			</div>
			
			<div className='config-general' style={{borderBottom:'1px solid #eeeeee', marginBottom:'5px', marginTop:'5px'}}>
				<div onClick={this.switchGeneralConfig} style={{color:'#2c6ba8',cursor:'pointer',display:'block',padding:'5px'}}>
					<span style={{fontWeight:'bold'}}>{translate('General Configuration', this.props.lang)}</span>  <Icon style={{fontSize:'80%'}} type={this.state.configGeneralOpen ? 'minus-circle':'plus-circle'}/>
				</div>
				{this.state.configGeneralOpen && <div>
					<FormItem {...formItemLayout} label={translate('Element ID', this.props.lang)} validateStatus={this.state.errors.includes('fieldID') ? 'error':''}>
						<Input onChange={this.setFieldID} value={this.state.fieldID} placeholder={translate('Element ID', this.props.lang)} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
					</FormItem>
					<FormItem {...formItemLayout} label={translate('XML ID', this.props.lang)} validateStatus={this.state.errors.includes('xmlID') ? 'error':''}>
						<Input onChange={this.setXMLID} value={this.state.xmlID} placeholder={translate('XML ID', this.props.lang)} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
					</FormItem>
					<FormItem {...formItemLayout} label={translate('Left Text', this.props.lang)} validateStatus=''>
						<Input onChange={this.setLeftText} value={decodeHtml(this.state.leftText)} placeholder={translate('Left Text', this.props.lang)} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
					</FormItem>
					<FormItem {...formItemLayout} label={translate('Right Text', this.props.lang)} validateStatus=''>
						<Input onChange={this.setRightText} value={decodeHtml(this.state.rightText)} placeholder={translate('Right Text', this.props.lang)} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
					</FormItem>
					<FormItem {...formItemLayout} label={translate('Required', this.props.lang)} validateStatus=''>
						<Checkbox checked={this.state.required} onChange={this.setRequired}/>
					</FormItem>
					{this.props.element.type !== 'calc' && <FormItem {...formItemLayout} label={translate('Locked', this.props.lang)} validateStatus=''>
						<Checkbox checked={this.state.lock} onChange={this.setLocked}/>
					</FormItem>}
					<FormItem {...formItemLayout} label={translate('Visible', this.props.lang)} validateStatus=''>
						<Checkbox checked={this.state.visible} onChange={this.setVisible}/>
					</FormItem>
					{(typeof this.state.orientation !== typeof undefined) && <FormItem {...formItemLayout} label={translate('Orientation', this.props.lang)}>
						<RadioGroup onChange={this.setOrientation} value={this.state.orientation}>
							<Radio value='horizontal'>Horizontal</Radio>
							<Radio value='vertical'>Vertical</Radio>
						</RadioGroup>
					</FormItem>}
					{(this.props.element.type === 'external_file') && <div>
						<FormItem {...formItemLayout} label='Link'>
							<Input onChange={this.setLink} value={this.state.value} placeholder='Link' style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
						</FormItem>
						<FormItem {...formItemLayout} label={translate('Width', this.props.lang)}>
							<InputNumber step={1} onChange={this.setWidth} value={this.state.width} placeholder={translate('Width', this.props.lang) + ' (px)'} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
						</FormItem>
						<FormItem {...formItemLayout} label={translate('Height', this.props.lang)}>
							<InputNumber step={1} onChange={this.setHeight} value={this.state.height} placeholder={translate('Height', this.props.lang) + ' (px)'} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
						</FormItem>
					</div>}
				</div>}
			</div>
			
			{(this.props.element.type === 'text' || this.props.element.type === 'textarea') && <div className='config-general' style={{borderBottom:'1px solid #eeeeee', marginBottom:'5px', marginTop:'5px'}}>
				<div onClick={this.switchTextConfig} style={{color:'#2c6ba8',cursor:'pointer',display:'block',padding:'5px'}}>
					<span style={{fontWeight:'bold'}}>{translate('Text Configuration', this.props.lang)}</span>  <Icon style={{fontSize:'80%'}} type={this.state.configTextOpen ? 'minus-circle':'plus-circle'}/>
				</div>
				{this.state.configTextOpen && <div>
					{(typeof this.state.maxLength !== typeof undefined) &&
						<FormItem {...formItemLayout} label={translate('Max Characters', this.props.lang)}>
							<InputNumber onChange={this.setMaxChars} value={maxChars} placeholder={translate('Max Characters', this.props.lang)} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
						</FormItem>}
					{(typeof this.state.expression !== typeof undefined) &&
						<FormItem {...formItemLayout} label='REGEX:' validateStatus={this.state.errors.includes('regex') ? 'error':''}>
							<Input onChange={this.setREGEX} value={decodeHtml(this.state.expression)} placeholder={translate('Regular Expression', this.props.lang)} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
						</FormItem>}
				</div>}
			</div>}
			
			{this.props.element.type === 'dataSource' && <div className='config-general' style={{borderBottom:'1px solid #eeeeee', marginBottom:'5px', marginTop:'5px'}}>
				<div onClick={this.switchDataSourceConfig} style={{color:'#2c6ba8',cursor:'pointer',display:'block',padding:'5px'}}>
					<span style={{fontWeight:'bold'}}>{translate('Data Base Configuration', this.props.lang)}</span>  <Icon style={{fontSize:'80%'}} type={this.state.configDataSourceOpen ? 'minus-circle':'plus-circle'}/>
				</div>
				{this.state.configDataSourceOpen && <div>
					<FormItem {...formItemLayout} label={translate('Data Base', this.props.lang)}>
						<select onChange={this.setDataBase} defaultValue={this.state.dataSourceConfig.sourceDB} style={{color:spanColor, flex:1, fontStyle:'italic', height:'30px', border:'1px solid #d9d9d9', borderRadius:'3px'}}>
							{this.renderConnectionsDataBases()}
						</select>
					</FormItem>
					<FormItem {...formItemLayout} label={translate('Table', this.props.lang)}>
						<select onChange={this.setTable} defaultValue={this.state.dataSourceConfig.sourceTable} style={{color:spanColor, flex:1, fontStyle:'italic', height:'30px', border:'1px solid #d9d9d9', borderRadius:'3px'}}>
							{this.renderTables()}
						</select>
					</FormItem>
					<FormItem {...formItemLayout} label={translate('ID', this.props.lang)}>
						<select onChange={this.setTableID} defaultValue={this.state.dataSourceConfig.sourceValue} style={{color:spanColor, flex:1, fontStyle:'italic', height:'30px', border:'1px solid #d9d9d9', borderRadius:'3px'}}>
							{this.renderFields('id')}
						</select>
					</FormItem>
					<FormItem {...formItemLayout} label={translate('Description', this.props.lang)}>
						<select onChange={this.setTableDescription} defaultValue={this.state.dataSourceConfig.sourceDescription} style={{color:spanColor, flex:1, fontStyle:'italic', height:'30px', border:'1px solid #d9d9d9', borderRadius:'3px'}}>
							{this.renderFields('description')}
						</select>
					</FormItem>
					<FormItem {...formItemLayout} label={translate('Extra', this.props.lang)}>
						<select onChange={this.setTableExtra} defaultValue={this.state.dataSourceConfig.sourceExtra} style={{color:spanColor, flex:1, fontStyle:'italic', height:'30px', border:'1px solid #d9d9d9', borderRadius:'3px'}}>
							{this.renderFields('extra')}
						</select>
					</FormItem>
					<FormItem {...formItemLayout} label={translate('Dependency', this.props.lang)}>
						<Input onChange={this.setDependencyIdElement} value={editorID} placeholder={translate('Element ID', this.props.lang)} style={{width:'80px', color:spanColor, fontStyle:'italic'}}/>
						<span style={{color:spanColor}}>=</span>
						<Input onChange={this.setDependencyTableField} value={tableField} placeholder={translate('Table ID', this.props.lang)} style={{width:'80px', color:spanColor, fontStyle:'italic'}}/>
					</FormItem>
				</div>}
			</div>}
			
			{this.props.element.type === 'calc' && <div className='config-general' style={{borderBottom:'1px solid #eeeeee', marginBottom:'5px', marginTop:'5px'}}>
				<div onClick={this.switchFormulaConfig} style={{color:'#2c6ba8',cursor:'pointer',display:'block',padding:'5px'}}>
					<span style={{fontWeight:'bold'}}>{translate('Formula Configuration', this.props.lang)}</span>  <Icon style={{fontSize:'80%'}} type={this.state.configFormulaOpen ? 'minus-circle':'plus-circle'}/>
				</div>
				{this.state.configFormulaOpen && <div>
					<FormItem {...formItemLayout} label='Formula' validateStatus={this.state.errors.includes('formula') ? 'error':''}>
						<Input onChange={this.setFormula} value={this.state.formula} placeholder='Formula' style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
					</FormItem>
					<FormItem {...formItemLayout} label={translate('Step', this.props.lang)} validateStatus=''>
						<InputNumber onChange={this.setStep} value={this.state.precision} placeholder={translate('Step', this.props.lang)} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
					</FormItem>
				</div>}
			</div>}
			
			{(typeof this.props.element.minimum !== typeof undefined && this.props.element.type !== 'calc') && <div className='config-general' style={{borderBottom:'1px solid #eeeeee', marginBottom:'5px', marginTop:'5px'}}>
				<div onClick={this.switchRangeConfig} style={{color:'#2c6ba8',cursor:'pointer',display:'block',padding:'5px'}}>
					<span style={{fontWeight:'bold'}}>{translate('Range Configuration', this.props.lang)}</span>  <Icon style={{fontSize:'80%'}} type={this.state.configRangeOpen ? 'minus-circle':'plus-circle'}/>
				</div>
				{this.state.configRangeOpen && <div>
					{this.props.element.type === 'date' && this.dateRange()}
					{this.props.element.type === 'time' && this.timeRange()}
					{this.props.element.type === 'slider' && this.sliderRange()}
					{(this.props.element.type !== 'slider' && this.props.element.type !== 'date' && this.props.element.type !== 'time') && this.numericRange()}
				</div>}
			</div>}
			
			{(this.props.element.type === 'dropdown') && <div className='config-general' style={{borderBottom:'1px solid #eeeeee', marginBottom:'5px', marginTop:'5px'}}>
				<div onClick={this.switchOptionsConfig} style={{color:'#2c6ba8',cursor:'pointer',display:'block',padding:'5px'}}>
					<span style={{fontWeight:'bold'}}>{translate('Options', this.props.lang)}</span>  <Icon style={{fontSize:'80%'}} type={this.state.configOptionsOpen ? 'minus-circle':'plus-circle'}/>
				</div>
				{this.state.configOptionsOpen && <div>
					{this.renderOptions()}
					<div style={{color:'#2c6ba8',justifyContent:'right',padding:'5px', textAlign:'right'}}>
						<Icon onClick={this.addNewOption} style={{cursor:'pointer', fontSize:'110%'}} type='plus-circle'/>
					</div>
				</div>}
				<FormItem {...formItemLayout} label='Default'>
					<select defaultValue={this.state.default} style={{borderRadius:3, border: '1px solid #cccccc', display:'inline-flex', minWidth:180, height:21}} onChange={this.setDefaultOption}>
						{this.renderDefaultOptions()}
					</select>
				</FormItem>
			</div>}
			
			{(this.props.element.type === 'radio') && <div className='config-general' style={{borderBottom:'1px solid #eeeeee', marginBottom:'5px', marginTop:'5px'}}>
				<div onClick={this.switchOptionsConfig} style={{color:'#2c6ba8',cursor:'pointer',display:'block',padding:'5px'}}>
					<span style={{fontWeight:'bold'}}>{translate('Options', this.props.lang)}</span>  <Icon style={{fontSize:'80%'}} type={this.state.configOptionsOpen ? 'minus-circle':'plus-circle'}/>
				</div>
				{this.state.configOptionsOpen && <div>
					{this.renderRadios()}
					<div style={{color:'#2c6ba8',justifyContent:'right',padding:'5px', textAlign:'right'}}>
						<Icon onClick={this.addNewRadio} style={{cursor:'pointer', fontSize:'110%'}} type='plus-circle'/>
					</div>
				</div>}
			</div>}
			
			{(this.props.element.type === 'checkbox') && <div className='config-general' style={{borderBottom:'1px solid #eeeeee', marginBottom:'5px', marginTop:'5px'}}>
				<div onClick={this.switchOptionsConfig} style={{color:'#2c6ba8',cursor:'pointer',display:'block',padding:'5px'}}>
					<span style={{fontWeight:'bold'}}>{translate('Options', this.props.lang)}</span>  <Icon style={{fontSize:'80%'}} type={this.state.configOptionsOpen ? 'minus-circle':'plus-circle'}/>
				</div>
				{this.state.configOptionsOpen && <div>
					{this.renderCheckboxes()}
					<div style={{color:'#2c6ba8',justifyContent:'right',padding:'5px', textAlign:'right'}}>
						<Icon onClick={this.addNewCheckbox} style={{cursor:'pointer', fontSize:'110%'}} type='plus-circle'/>
					</div>
				</div>}
			</div>}
			
			{(this.props.element.type === 'table') && <div className='config-general' style={{borderBottom:'1px solid #eeeeee', marginBottom:'5px', marginTop:'5px'}}>
				<div onClick={this.switchTableConfig} style={{color:'#2c6ba8',cursor:'pointer',display:'block',padding:'5px'}}>
					<span style={{fontWeight:'bold'}}>{translate('Table', this.props.lang)}</span>  <Icon style={{fontSize:'80%'}} type={this.state.configTableOpen ? 'minus-circle':'plus-circle'}/>
				</div>
				{this.state.configTableOpen && <div>
					<FormItem {...formItemLayout} label={translate('Title', this.props.lang)}>
						<Input onChange={this.setTableTitle} value={this.state.title} placeholder={translate('Title', this.props.lang)} style={{color:spanColor, flex:1, fontStyle:'italic'}}/>
					</FormItem>
					<FormItem {...formItemLayout} label={translate('Chart', this.props.lang)}>
						<select defaultValue={this.state.chart} style={{borderRadius:3, border: '1px solid #cccccc', display:'inline-flex', minWidth:180, height:21}} onChange={this.setTableChart}>
							<option value=''></option>
							<option value='line'>{translate('Line', this.props.lang)}</option>
							<option value='bars'>{translate('Bars', this.props.lang)}</option>
						</select>
					</FormItem>
					<span style={{color:'#2c6ba8',display:'block',fontWeight:'bold',textAlign:'center',width:'100%'}}>{translate('Header', this.props.lang)}s</span>
					{this.renderHeaders()}
				</div>}
			</div>}
			
			<div className='config-general' style={{borderBottom:'1px solid #eeeeee', marginBottom:'5px', marginTop:'5px'}}>
				<div onClick={this.switchPropertiesConfig} style={{color:'#2c6ba8',cursor:'pointer',display:'block',padding:'5px'}}>
					<span style={{fontWeight:'bold'}}>{translate(visibilitySectionTitle, this.props.lang)}</span>  <Icon style={{fontSize:'80%'}} type={this.state.configPropertiesOpen ? 'minus-circle':'plus-circle'}/>
				</div>
				{this.state.configPropertiesOpen && <div>
					{this.renderCriterias()}
					<div style={{color:'#2c6ba8',justifyContent:'right',padding:'5px', textAlign:'right'}}>
						<Icon onClick={this.addNewCriteria} style={{cursor:'pointer', fontSize:'110%'}} type='plus-circle'/>
					</div>
				</div>}
			</div>
			
			<ButtonGroup style={{display:'flex', justifyContent:'flex-end', padding:5}}>
				<Button onClick={this.props.close}>{translate('Cancel', this.props.lang)}</Button>
				<Button onClick={this.validateChanges} type='primary'>{translate('Save', this.props.lang)}</Button>
			</ButtonGroup>
		</Form>
	}
}

ElementConfigPanel.propTypes = {
	form: PropTypes.object.isRequired,
	element: PropTypes.object.isRequired,
	lang: PropTypes.string.isRequired
}